
package repositories;

import java.util.Collection;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import domain.Curricula;

@Repository
public interface CurriculaRepository extends JpaRepository<Curricula, Integer> {

	@Query("select avg(a.curriculas.size) from Candidate a")
	Double averageNumberCurriculaPerCandidate();

	@Query("select c from Curricula c where c.candidate.id = ?1 and c.isCopy!=true")
	Collection<Curricula> findCurriculasWithoutCopy(Integer candidateId);

	@Query("select c from Curricula c where c.ticker= ?1 and c.isCopy=true")
	Curricula findCurriculaCopied(String ticker);

}
