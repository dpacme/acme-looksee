
package repositories;

import java.util.Collection;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import domain.Apply;
import domain.Offer;

@Repository
public interface OfferRepository extends JpaRepository<Offer, Integer> {

	@Query("select avg(a.offers.size) from Company a")
	Double averageNumberOffersPerCompany();

	@Query("select o from Offer o where o.draft = 0 and o.company.banned = 0")
	Collection<Offer> findFilteredOffers();

	@Query("select a from Apply a where a.offer.id = ?1")
	Collection<Apply> getAppliesFromOffer(int offerId);
}
