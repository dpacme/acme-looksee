
package repositories;

import java.util.Collection;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import domain.Apply;

public interface ApplyRepository extends JpaRepository<Apply, Integer> {

	@Query("select a from Apply a join a.curricula c join c.candidate cn where cn.id=?1")
	Collection<Apply> findApplyFromCandidate(Integer candidateId);

	@Query("select count(cu)/(select count(c2) from Candidate c2)*1.0 from Candidate c join c.curriculas cu where cu.isCopy = 1")
	Double avgNumberApplicationsPerCandidate();

	@Query("select count(cu4) from Candidate c4 join c4.curriculas cu4 where cu4.isCopy = 1 and c4.id = (select min(cu1.candidate.id) from Curricula cu1 where cu1.isCopy = 1 and (select count(cu2) from Curricula cu2 where cu2.isCopy = 1 ) <=all(select count(cu3) from Curricula cu3 where cu3.isCopy = 1 group by cu3.candidate.id))")
	Double minNumberApplicationsPerCandidate();

	@Query("select count(cu4) from Candidate c4 join c4.curriculas cu4 where cu4.isCopy = 1 and c4.id = (select min(cu1.candidate.id) from Curricula cu1 where cu1.isCopy = 1 and (select count(cu2) from Curricula cu2 where cu2.isCopy = 1 ) >=all(select count(cu3) from Curricula cu3 where cu3.isCopy = 1 group by cu3.candidate.id))")
	Double maxNumberApplicationsPerCandidate();

	@Query("select avg(c.offers.size) from Company c")
	Double avgNumberApplicationsPerOffer();

	@Query("select min(c.offers.size) from Company c")
	Double minNumberApplicationsPerOffer();

	@Query("select max(c.offers.size) from Company c")
	Double maxNumberApplicationsPerOffer();

	@Query("select avg(c.size) from Company a join a.offers b join b.applies c where c.status = 'accepted'")
	Double avgNumberApplicationsAcceptedPerCompany();

	@Query("select min(c.size) from Company a join a.offers b join b.applies c where c.status = 'accepted'")
	Double minNumberApplicationsAcceptedPerCompany();

	@Query("select max(c.size) from Company a join a.offers b join b.applies c where c.status = 'accepted'")
	Double maxNumberApplicationsAcceptedPerCompany();

	@Query("select avg(c.size) from Company a join a.offers b join b.applies c where c.status = 'rejected'")
	Double avgNumberApplicationsRejectedPerCompany();

	@Query("select min(c.size) from Company a join a.offers b join b.applies c where c.status = 'rejected'")
	Double minNumberApplicationsRejectedPerCompany();

	@Query("select max(c.size) from Company a join a.offers b join b.applies c where c.status = 'rejected'")
	Double maxNumberApplicationsRejectedPerCompany();

	@Query("select avg(c.size) from Company a join a.offers b join b.applies c where c.status = 'pending'")
	Double avgNumberApplicationsPendingPerCompany();

	@Query("select min(c.size) from Company a join a.offers b join b.applies c where c.status = 'pending'")
	Double minNumberApplicationsPendingPerCompany();

	@Query("select max(c.size) from Company a join a.offers b join b.applies c where c.status = 'pending'")
	Double maxNumberApplicationsPendingPerCompany();

	@Query("select a from Apply a where a.status = 'pending'")
	List<Apply> appliesPending();

}
