
package repositories;

import java.util.Collection;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import domain.Candidate;
import security.UserAccount;

@Repository
public interface CandidateRepository extends JpaRepository<Candidate, Integer> {

	@Query("select a from Candidate a where a.userAccount = ?1")
	Candidate findByUserAccount(UserAccount userAccount);

	@Query("select c from Candidate c order by c.curriculas.size desc")
	Collection<Candidate> candidatesInDescendingOrderByNumberOfCurricula();

	@Query("select c from Candidate c where c.curriculas.size >= (select max(c2.curriculas.size) from Candidate c2) group by c")
	Collection<Candidate> candidatesWithMoreCurriculas();
}
