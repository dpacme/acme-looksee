
package repositories;

import java.util.Collection;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import domain.Company;
import security.UserAccount;

@Repository
public interface CompanyRepository extends JpaRepository<Company, Integer> {

	@Query("select a from Company a where a.userAccount = ?1")
	Company findByUserAccount(UserAccount userAccount);

	@Query("select c from Company c where c.banned = 0")
	Collection<Company> findValidCompanies();

	@Query("select c from Company c order by c.offers.size desc")
	Collection<Company> companiesInDescendingOrderByNumberOfOffers();

	@Query("select c from Company c where c.offers.size >= (select max(c2.offers.size) from Company c2) group by c")
	Collection<Company> companiesWithMoreOffers();
}
