
package services;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import repositories.OfferRepository;
import domain.Apply;
import domain.Offer;

@Service
@Transactional
public class OfferService {

	// Managed Repository --------------------------------------------------------------
	@Autowired
	private OfferRepository	offerRepository;


	// Constructor methods --------------------------------------------------------------
	public OfferService() {
		super();
	}


	// Supporting services --------------------------------------------------------------

	@Autowired
	private CompanyService	companyService;


	// Simple CRUD methods --------------------------------------------------------------

	public Offer findOne(final int offerId) {
		Assert.isTrue(offerId != 0);
		Offer result;

		result = offerRepository.findOne(offerId);
		Assert.notNull(result);

		return result;
	}

	public Collection<Offer> findAll() {
		Collection<Offer> result;

		result = offerRepository.findAll();
		Assert.notNull(result);

		return result;
	}

	public void save(final Offer offer) {
		Assert.notNull(offer);
		Assert.isTrue(offer.getMinSalary() <= offer.getMaxSalary(), "The minimum wage must be less than the maximum");
		checkOffer(offer);
		offerRepository.save(offer);
	}

	public Offer create() {
		final Offer offer = new Offer();
		final Collection<Apply> applies = new ArrayList<Apply>();
		offer.setApplies(applies);
		offer.setDraft(true);
		offer.setCompany(companyService.findByPrincipal());
		return offer;
	}

	public void delete(final Offer offer) {
		Assert.notNull(offer);
		offerRepository.delete(offer);
	}

	public Offer saveAndFlush(final Offer offer) {
		Assert.notNull(offer);

		return offerRepository.saveAndFlush(offer);
	}

	// Other bussines methods -----------------------------------------------------

	/**
	 * M�todo que devuelve todas las ofertas que no sean borradores y cuya compa��a asociada no est� baneada.
	 * 
	 * @param offers
	 * @return Lista ofertas filtradas
	 */
	public Collection<Offer> filter(final Collection<Offer> offers) {
		Collection<Offer> res = new ArrayList<Offer>();

		/*
		 * Si se le ha pasado una lista base, se filtra la lista.
		 * Sino, se buscan todas las ofertas de BBDD que no sean borrador ni cuya compa��a est� baneada
		 */
		if (offers != null) {
			for (final Offer o : offers) {
				if (!(o.getDraft()) && !o.getCompany().getBanned()) {
					res.add(o);
				}
			}
		} else {
			res = offerRepository.findFilteredOffers();
		}
		return res;
	}

	public Collection<Offer> searchOffer(final String keyword, final Double minSalary, final Double maxSalary) {
		final Collection<Offer> aux = findAll();
		final Collection<Offer> res;

		if (keyword != "") {
			final Collection<Offer> conKeyword = buscadorPorKeyword(keyword);
			aux.retainAll(conKeyword);
		}
		if (maxSalary != null) {
			final Collection<Offer> conMax = buscadorPorMaxSalary(maxSalary);
			aux.retainAll(conMax);
		}
		if (minSalary != null) {
			final Collection<Offer> conMin = buscadorPorMinSalary(minSalary);
			aux.retainAll(conMin);
		}

		res = findOpenOffers(aux);

		return res;
	}

	public Collection<Offer> buscadorPorKeyword(final String keyword) {

		final Collection<Offer> offers = findAll();
		final Collection<Offer> res = new ArrayList<Offer>();

		for (final Offer o : offers) {
			if (keyword != "" && (o.getTitle().toLowerCase().contains(keyword.toLowerCase()) || o.getDescription().toLowerCase().contains(keyword.toLowerCase()))) {
				res.add(o);
			}
		}
		return res;
	}

	public Collection<Offer> buscadorPorMinSalary(final Double minSalary) {

		final Collection<Offer> offers = findAll();
		final Collection<Offer> res = new ArrayList<Offer>();

		for (final Offer o : offers) {
			if (minSalary != null && minSalary <= o.getMinSalary()) {
				res.add(o);
			}
		}
		return res;
	}

	public Collection<Offer> buscadorPorMaxSalary(final Double maxSalary) {

		final Collection<Offer> offers = findAll();
		final Collection<Offer> res = new ArrayList<Offer>();

		for (final Offer o : offers) {
			if (maxSalary != null && maxSalary >= o.getMaxSalary()) {
				res.add(o);
			}
		}
		return res;
	}

	public Collection<Offer> findOpenOffers(final Collection<Offer> offers) {
		final Collection<Offer> res = new ArrayList<Offer>();
		final Date date = new Date();

		for (final Offer o : offers) {
			if (date.before(o.getDeadLine())) {
				res.add(o);
			}
		}
		return res;
	}

	public Double averageNumberOffersPerCompany() {
		Double result;

		result = offerRepository.averageNumberOffersPerCompany();

		if (result == null) {
			result = 0.0;
		}

		return result;
	}

	public void publish(final Offer offer) {
		Assert.isTrue(offer.getDraft());
		Assert.isTrue(offer.getCompany().getId() == companyService.findByPrincipal().getId());
		final Date date = new Date();
		date.setDate(date.getDate() + 7);
		Assert.isTrue(date.before(offer.getDeadLine()), "The deadline must be at least one week after the current date");
		offer.setDraft(false);
		offer.setMomentCreate(new Date());
		save(offer);
	}

	public Collection<Offer> filterDrafts(final Collection<Offer> offers) {
		final Collection<Offer> res = new ArrayList<Offer>();

		for (final Offer o : offers) {
			if ((o.getDraft())) {
				res.add(o);
			}
		}

		return res;
	}

	public Collection<Offer> filterNoDrafts(final Collection<Offer> offers) {
		final Collection<Offer> res = new ArrayList<Offer>();

		for (final Offer o : offers) {
			if (!(o.getDraft())) {
				res.add(o);
			}
		}

		return res;
	}

	public void checkOffer(final Offer offer) {
		Assert.isTrue(offer.getTitle() != null && offer.getTitle() != "" && offer.getDescription() != null && offer.getDescription() != "" && offer.getMinSalary() != null && offer.getMaxSalary() != null && offer.getCurrency() != null
			&& offer.getCurrency() != "" && offer.getDeadLine() != null && offer.getDraft() != null);
	}

	public Collection<Apply> getAppliesFromOffer(final int offerId) {
		try {
			return offerRepository.getAppliesFromOffer(offerId);
		} catch (final Exception e) {
			return new ArrayList<Apply>();
		}
	}
}
