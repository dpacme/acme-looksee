
package services;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;

import javax.transaction.Transactional;

import org.apache.commons.lang.RandomStringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import domain.Candidate;
import domain.Curricula;
import domain.EducationRecord;
import domain.EndorserRecord;
import domain.MiscellaneousRecord;
import domain.PersonalRecord;
import domain.ProfessionalRecord;
import repositories.CurriculaRepository;

@Service
@Transactional
public class CurriculaService {

	// Managed Repository --------------------------------------------------------------
	@Autowired
	private CurriculaRepository curriculaRepository;


	// Constructor methods --------------------------------------------------------------
	public CurriculaService() {
		super();
	}


	// Supporting services --------------------------------------------------------------

	@Autowired
	private CandidateService			candidateService;

	@Autowired
	private EducationRecordService		educationRecordService;

	@Autowired
	private MiscellaneousRecordService	miscellaneousRecordService;

	@Autowired
	private ProfessionalRecordService	professionalRecordService;

	@Autowired
	private EndorserRecordService		endorserRecordService;

	@Autowired
	private PersonalRecordService		personalRecordService;
	// Simple CRUD methods --------------------------------------------------------------


	public Curricula createCurricula() {

		final Curricula curricula = new Curricula();
		return curricula;

	}

	public Curricula findOne(final int curriculaId) {
		Assert.isTrue(curriculaId != 0);
		Curricula result;

		result = this.curriculaRepository.findOne(curriculaId);
		Assert.notNull(result);

		return result;
	}

	public Collection<Curricula> findAll() {
		Collection<Curricula> result;

		result = this.curriculaRepository.findAll();
		Assert.notNull(result);

		return result;
	}

	public void save(final Curricula curricula) {
		Assert.notNull(curricula);

		this.curriculaRepository.save(curricula);
	}

	public Curricula saveAndFlush(final Curricula curricula) {
		Assert.notNull(curricula);

		return this.curriculaRepository.saveAndFlush(curricula);
	}

	public Curricula create() {
		final Curricula curricula = new Curricula();
		curricula.setIsCopy(false);
		curricula.setTicker(this.createTicker());
		curricula.setCandidate(this.candidateService.findByPrincipal());
		curricula.setEducationRecords(new ArrayList<EducationRecord>());
		curricula.setProfessionalRecords(new ArrayList<ProfessionalRecord>());
		curricula.setEndorserRecords(new ArrayList<EndorserRecord>());
		curricula.setMiscellaneousRecords(new ArrayList<MiscellaneousRecord>());
		if (this.candidateService.findByPrincipal().getCurriculas() != null || !(this.candidateService.findByPrincipal().getCurriculas().isEmpty()))
			for (final Curricula c : this.candidateService.findByPrincipal().getCurriculas())
				if (!c.getIsCopy()) {
					curricula.setPersonalRecord(c.getPersonalRecord());
					break;
				}
		return curricula;
	}

	public void delete(final Curricula curricula) {
		for (final EducationRecord e : curricula.getEducationRecords())
			this.educationRecordService.delete(e);
		for (final ProfessionalRecord p : curricula.getProfessionalRecords())
			this.professionalRecordService.delete(p);
		for (final EndorserRecord en : curricula.getEndorserRecords())
			this.endorserRecordService.delete(en);
		for (final MiscellaneousRecord m : curricula.getMiscellaneousRecords())
			this.miscellaneousRecordService.delete(m);

		final Collection<Curricula> curriculas = this.candidateService.findByPrincipal().getCurriculas();
		final Collection<Curricula> aux = this.filterCurriculas(curriculas);
		if (aux.size() == 1) {
			final PersonalRecord p = curricula.getPersonalRecord();
			this.curriculaRepository.delete(curricula);
			this.personalRecordService.delete(p);

		} else
			this.curriculaRepository.delete(curricula);

	}

	// Other bussines methods -----------------------------------------------------

	public Double averageNumberCurriculaPerCandidate() {
		Double result;

		result = this.curriculaRepository.averageNumberCurriculaPerCandidate();

		if (result == null)
			result = 0.0;

		return result;
	}

	public Collection<Curricula> filterCurriculas(final Collection<Curricula> curriculas) {
		final Collection<Curricula> res = new ArrayList<Curricula>();

		for (final Curricula c : curriculas)
			if (!(c.getIsCopy()))
				res.add(c);

		return res;
	}

	public Collection<Curricula> searchCurricula(final String keyword) {
		Collection<Curricula> res = new ArrayList<Curricula>();
		if (keyword != "") {
			final Collection<Curricula> porEducationRecord = this.buscadorPorEducationRecord(keyword);
			for (final Curricula c : porEducationRecord)
				if (!(res.contains(c)))
					res.add(c);

			final Collection<Curricula> porProfessionalRecord = this.buscadorPorProfessionalRecord(keyword);
			for (final Curricula c : porProfessionalRecord)
				if (!(res.contains(c)))
					res.add(c);

			final Collection<Curricula> porEndorserRecord = this.buscadorPorEndorserRecord(keyword);
			for (final Curricula c : porEndorserRecord)
				if (!(res.contains(c)))
					res.add(c);

			final Collection<Curricula> porMiscellaneousRecord = this.buscadorPorMiscellaneousRecord(keyword);
			for (final Curricula c : porMiscellaneousRecord)
				if (!(res.contains(c)))
					res.add(c);
		} else
			res = this.candidateService.findByPrincipal().getCurriculas();
		return res;
	}

	public Collection<Curricula> buscadorPorEducationRecord(final String keyword) {

		final Collection<Curricula> curriculas = this.candidateService.findByPrincipal().getCurriculas();
		final Collection<Curricula> res = new ArrayList<Curricula>();

		for (final Curricula c : curriculas)
			for (final EducationRecord e : c.getEducationRecords())
				if (keyword != "" && (e.getTitle().toLowerCase().contains(keyword.toLowerCase()) || e.getInstitution().toLowerCase().contains(keyword.toLowerCase()) || e.getComments().toLowerCase().contains(keyword.toLowerCase())))
					if (!(res.contains(c)))
						res.add(c);

		return res;
	}

	public Collection<Curricula> buscadorPorProfessionalRecord(final String keyword) {

		final Collection<Curricula> curriculas = this.candidateService.findByPrincipal().getCurriculas();
		final Collection<Curricula> res = new ArrayList<Curricula>();

		for (final Curricula c : curriculas)
			for (final ProfessionalRecord p : c.getProfessionalRecords())
				if (keyword != "" && (p.getCompany().toLowerCase().contains(keyword.toLowerCase()) || p.getRole().toLowerCase().contains(keyword.toLowerCase()) || p.getComments().toLowerCase().contains(keyword.toLowerCase())))
					if (!(res.contains(c)))
						res.add(c);

		return res;
	}

	public Collection<Curricula> buscadorPorEndorserRecord(final String keyword) {

		final Collection<Curricula> curriculas = this.candidateService.findByPrincipal().getCurriculas();
		final Collection<Curricula> res = new ArrayList<Curricula>();

		for (final Curricula c : curriculas)
			for (final EndorserRecord e : c.getEndorserRecords())
				if (keyword != "" && (e.getFullName().toLowerCase().contains(keyword.toLowerCase()) || e.getEmail().toLowerCase().contains(keyword.toLowerCase()) || e.getComments().toLowerCase().contains(keyword.toLowerCase())))
					if (!(res.contains(c)))
						res.add(c);

		return res;
	}

	public Collection<Curricula> buscadorPorMiscellaneousRecord(final String keyword) {

		final Collection<Curricula> curriculas = this.candidateService.findByPrincipal().getCurriculas();
		final Collection<Curricula> res = new ArrayList<Curricula>();

		for (final Curricula c : curriculas)
			for (final MiscellaneousRecord m : c.getMiscellaneousRecords())
				if (keyword != "" && (m.getTitle().toLowerCase().contains(keyword.toLowerCase()) || m.getComments().toLowerCase().contains(keyword.toLowerCase())))
					if (!(res.contains(c)))
						res.add(c);

		return res;
	}

	public Collection<Curricula> findCurriculasWithoutCopy(final Integer candidateId) {
		try {
			return this.curriculaRepository.findCurriculasWithoutCopy(candidateId);
		} catch (final Exception e) {
			return new ArrayList<Curricula>();
		}
	}

	public Curricula findCurriculaCopied(final String ticker) {
		try {
			final Curricula curricula = this.curriculaRepository.findCurriculaCopied(ticker);
			return curricula;
		} catch (final Exception e) {
			return null;
		}
	}

	public Curricula clone(final Integer curriculaId, final Integer offerId) {
		Assert.isTrue(curriculaId != 0);
		Curricula toClone;

		toClone = this.curriculaRepository.findOne(curriculaId);

		Assert.notNull(toClone);
		final Candidate candidate = this.candidateService.findByPrincipal();

		Curricula cloned = this.create();
		cloned.setTicker(toClone.getTicker());
		cloned.setIsCopy(true);
		cloned.setCandidate(candidate);
		cloned.setPersonalRecord(toClone.getPersonalRecord());

		cloned = this.saveAndFlush(cloned);
		if (toClone.getEducationRecords() != null && !toClone.getEducationRecords().isEmpty())
			for (final EducationRecord e : toClone.getEducationRecords()) {
				final EducationRecord educationRecord = this.educationRecordService.createEndorser();
				educationRecord.setAttachment(e.getAttachment());
				educationRecord.setComments(e.getComments());
				educationRecord.setStartDate(e.getStartDate());
				educationRecord.setEndDate(e.getEndDate());
				educationRecord.setInstitution(e.getInstitution());
				educationRecord.setTitle(e.getTitle());
				educationRecord.setCurricula(cloned);
				this.educationRecordService.save(educationRecord);
			}

		if (toClone.getProfessionalRecords() != null && !toClone.getProfessionalRecords().isEmpty())
			for (final ProfessionalRecord e : toClone.getProfessionalRecords()) {
				final ProfessionalRecord professionalRecord = this.professionalRecordService.createProfessional();
				professionalRecord.setAttachment(e.getAttachment());
				professionalRecord.setComments(e.getComments());
				professionalRecord.setStartDate(e.getStartDate());
				professionalRecord.setEndDate(e.getEndDate());
				professionalRecord.setCompany(e.getCompany());
				professionalRecord.setRole(e.getRole());
				professionalRecord.setCurricula(cloned);
				this.professionalRecordService.save(professionalRecord);
			}

		if (toClone.getEndorserRecords() != null && !toClone.getEndorserRecords().isEmpty())
			for (final EndorserRecord e : toClone.getEndorserRecords()) {
				final EndorserRecord endorserRecord = this.endorserRecordService.createEndorser();
				endorserRecord.setComments(e.getComments());
				endorserRecord.setEmail(e.getEmail());
				endorserRecord.setFullName(e.getFullName());
				endorserRecord.setLinkedInUrl(e.getLinkedInUrl());
				endorserRecord.setPhone(e.getPhone());
				endorserRecord.setCurricula(cloned);
				this.endorserRecordService.save(endorserRecord);
			}

		if (toClone.getMiscellaneousRecords() != null && !toClone.getMiscellaneousRecords().isEmpty())
			for (final MiscellaneousRecord e : toClone.getMiscellaneousRecords()) {
				final MiscellaneousRecord miscellaneousRecord = this.miscellaneousRecordService.createMiscellaneous();
				miscellaneousRecord.setAttachment(e.getAttachment());
				miscellaneousRecord.setComments(e.getComments());
				miscellaneousRecord.setTitle(e.getTitle());
				miscellaneousRecord.setCurricula(cloned);
				this.miscellaneousRecordService.save(miscellaneousRecord);
			}

		return this.saveAndFlush(cloned);
	}

	public String createTicker() {
		final String abecedario = "TRWAGMYFPDXBNJZSQVHLCKE";
		final SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		final Date date = new Date();
		final String res = sdf.format(date) + "-" + RandomStringUtils.random(4, abecedario);
		return res;
	}

}
