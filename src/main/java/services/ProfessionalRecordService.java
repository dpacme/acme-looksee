
package services;

import java.util.Collection;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import domain.Curricula;
import domain.EducationRecord;
import domain.ProfessionalRecord;
import repositories.ProfessionalRecordRepository;

@Service
@Transactional
public class ProfessionalRecordService {

	// Managed Repository --------------------------------------------------------------
	@Autowired
	private ProfessionalRecordRepository professionalRecordRepository;


	// Constructor methods --------------------------------------------------------------
	public ProfessionalRecordService() {
		super();
	}

	// Supporting services --------------------------------------------------------------

	// Simple CRUD methods --------------------------------------------------------------

	public ProfessionalRecord findOne(final int professionalRecordId) {
		Assert.isTrue(professionalRecordId != 0);
		ProfessionalRecord result;

		result = this.professionalRecordRepository.findOne(professionalRecordId);
		Assert.notNull(result);

		return result;
	}

	public Collection<ProfessionalRecord> findAll() {
		Collection<ProfessionalRecord> result;

		result = this.professionalRecordRepository.findAll();
		Assert.notNull(result);

		return result;
	}

	public void save(final ProfessionalRecord professionalRecord) {
		Assert.notNull(professionalRecord);

		if (professionalRecord.getEndDate() != null)
			Assert.isTrue(professionalRecord.getStartDate().before(professionalRecord.getEndDate()), "The end date must be after the start date");
		this.professionalRecordRepository.save(professionalRecord);

	}
	
	public ProfessionalRecord saveAndFlush(ProfessionalRecord professionalRecord) {
		Assert.notNull(professionalRecord);

		return this.professionalRecordRepository.saveAndFlush(professionalRecord);
	}

	public ProfessionalRecord create(final Curricula curricula) {
		final ProfessionalRecord professionalRecord = new ProfessionalRecord();
		professionalRecord.setCurricula(curricula);
		curricula.getProfessionalRecords().add(professionalRecord);
		return professionalRecord;
	}

	public void delete(final ProfessionalRecord professionalRecord) {
		Assert.notNull(professionalRecord);
		this.professionalRecordRepository.delete(professionalRecord);
	}

	public ProfessionalRecord createProfessional() {
		final ProfessionalRecord professionalRecord = new ProfessionalRecord();

		return professionalRecord;
	}

	// Other bussines methods -----------------------------------------------------

	public void comprobacion(final ProfessionalRecord professionalRecord) {
		Assert.isTrue(!(professionalRecord.getCompany().isEmpty()));
		Assert.isTrue(!(professionalRecord.getStartDate() == null));
		Assert.isTrue(!(professionalRecord.getRole().isEmpty()));
	}

}
