
package services;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import domain.Apply;
import domain.Candidate;
import domain.Company;
import domain.Curricula;
import domain.Offer;
import repositories.ApplyRepository;

@Service
@Transactional
public class ApplyService {

	// Managed Repository --------------------------------------------------------------
	@Autowired
	private ApplyRepository		applyRepository;

	@Autowired
	private CompanyService		companyService;

	@Autowired
	private CandidateService	candidateService;

	@Autowired
	private OfferService		offerService;

	@Autowired
	private CurriculaService	curriculaService;


	// Constructor methods --------------------------------------------------------------
	public ApplyService() {
		super();
	}

	// Supporting services --------------------------------------------------------------

	// Simple CRUD methods --------------------------------------------------------------

	public Apply create() {

		Apply apply = new Apply();
		return apply;

	}

	public Apply findOne(final int applyId) {
		Assert.isTrue(applyId != 0);
		Apply result;

		result = applyRepository.findOne(applyId);
		Assert.notNull(result);

		return result;
	}

	public Collection<Apply> findAll() {
		Collection<Apply> result;

		result = applyRepository.findAll();
		Assert.notNull(result);

		return result;
	}

	public void save(final Apply apply) {
		Assert.notNull(apply);

		applyRepository.save(apply);
	}

	public Apply saveAndFlush(Apply apply) {
		Assert.notNull(apply);

		return applyRepository.saveAndFlush(apply);
	}

	// Other business methods -------------------------------------------------

	public Apply setApplyAsPending(int applyID) {
		Company company = companyService.findByPrincipal();
		Assert.notNull(company);

		Apply apply = findOne(applyID);
		Assert.notNull(apply);

		//Se comprueba que el apply pertenece a una oferta de la compa��a logada
		Assert.isTrue(apply.getOffer().getCompany().getId() == company.getId());

		apply.setStatus("pending");
		return saveAndFlush(apply);
	}

	public Apply setApplyAsAccepted(int applyID) {
		Company company = companyService.findByPrincipal();
		Assert.notNull(company);

		Apply apply = findOne(applyID);
		Assert.notNull(apply);

		//Se comprueba que el apply pertenece a una oferta de la compa��a logada
		Assert.isTrue(apply.getOffer().getCompany().getId() == company.getId());

		apply.setStatus("accepted");
		return saveAndFlush(apply);
	}

	public Apply setApplyAsRejected(int applyID) {
		Company company = companyService.findByPrincipal();
		Assert.notNull(company);

		Apply apply = findOne(applyID);
		Assert.notNull(apply);

		//Se comprueba que el apply pertenece a una oferta de la compa��a logada
		Assert.isTrue(apply.getOffer().getCompany().getId() == company.getId());

		apply.setStatus("rejected");
		return saveAndFlush(apply);
	}

	public Collection<Apply> findApplyFromCandidate() {
		try {
			Candidate candidate = candidateService.findByPrincipal();
			return applyRepository.findApplyFromCandidate(candidate.getId());

		} catch (Exception e) {
			return new ArrayList<Apply>();
		}
	}

	public double avgNumberApplicationsPerCandidate() {
		double result = 0.0;
		try {
			result = applyRepository.avgNumberApplicationsPerCandidate();
		} catch (Exception e) {
			result = 0.0;
			return result;
		}
		return result;
	}

	public double minNumberApplicationsPerCandidate() {
		double result = 0.0;
		try {
			result = applyRepository.minNumberApplicationsPerCandidate();
		} catch (Exception e) {
			result = 0.0;
			return result;
		}
		return result;
	}

	public double maxNumberApplicationsPerCandidate() {
		double result = 0.0;
		try {
			result = applyRepository.maxNumberApplicationsPerCandidate();
		} catch (Exception e) {
			result = 0.0;
			return result;
		}
		return result;
	}

	public double avgNumberApplicationsPerOffer() {
		double result = 0.0;
		try {
			result = applyRepository.avgNumberApplicationsPerOffer();
		} catch (Exception e) {
			result = 0.0;
			return result;
		}
		return result;
	}

	public double minNumberApplicationsPerOffer() {
		double result = 0.0;
		try {
			result = applyRepository.minNumberApplicationsPerOffer();
		} catch (Exception e) {
			result = 0.0;
			return result;
		}
		return result;
	}

	public double maxNumberApplicationsPerOffer() {
		double result = 0.0;
		try {
			result = applyRepository.maxNumberApplicationsPerOffer();
		} catch (Exception e) {
			result = 0.0;
			return result;
		}
		return result;
	}

	public double avgNumberApplicationsAcceptedPerCompany() {
		double result = 0.0;
		try {
			result = applyRepository.avgNumberApplicationsAcceptedPerCompany();
		} catch (Exception e) {
			result = 0.0;
			return result;
		}
		return result;
	}

	public double minNumberApplicationsAcceptedPerCompany() {
		double result = 0.0;
		try {
			result = applyRepository.minNumberApplicationsAcceptedPerCompany();
		} catch (Exception e) {
			result = 0.0;
			return result;
		}
		return result;
	}

	public double maxNumberApplicationsAcceptedPerCompany() {
		double result = 0.0;
		try {
			result = applyRepository.maxNumberApplicationsAcceptedPerCompany();
		} catch (Exception e) {
			result = 0.0;
			return result;
		}
		return result;
	}

	public double avgNumberApplicationsRejectedPerCompany() {
		double result = 0.0;
		try {
			result = applyRepository.avgNumberApplicationsRejectedPerCompany();
		} catch (Exception e) {
			result = 0.0;
			return result;
		}
		return result;
	}

	public double minNumberApplicationsRejectedPerCompany() {
		double result = 0.0;
		try {
			result = applyRepository.minNumberApplicationsRejectedPerCompany();
		} catch (Exception e) {
			result = 0.0;
			return result;
		}
		return result;
	}

	public double maxNumberApplicationsRejectedPerCompany() {
		double result = 0.0;
		try {
			result = applyRepository.maxNumberApplicationsRejectedPerCompany();
		} catch (Exception e) {
			result = 0.0;
			return result;
		}
		return result;
	}

	public double avgNumberApplicationsPendingPerCompany() {
		double result = 0.0;
		try {
			result = applyRepository.avgNumberApplicationsPendingPerCompany();
		} catch (Exception e) {
			result = 0.0;
			return result;
		}
		return result;
	}

	public double minNumberApplicationsPendingPerCompany() {
		double result = 0.0;
		try {
			result = applyRepository.minNumberApplicationsPendingPerCompany();
		} catch (Exception e) {
			result = 0.0;
			return result;
		}
		return result;
	}

	public double maxNumberApplicationsPendingPerCompany() {
		double result = 0.0;
		try {
			result = applyRepository.maxNumberApplicationsPendingPerCompany();
		} catch (Exception e) {
			result = 0.0;
			return result;
		}
		return result;
	}

	public Offer registerApplicationOffer(Integer offerId, Integer curriculaId) {

		Apply apply = create();

		try {
			Candidate candidate = candidateService.findByPrincipal();

			Assert.notNull(candidate);

			Curricula curriculaCopied = curriculaService.clone(curriculaId, offerId);

			Offer offer = offerService.findOne(offerId);

			apply.setCurricula(curriculaCopied);
			apply.setOffer(offer);
			apply.setStatus("pending");
			apply.setMomment(new Date());
			Apply applySaved = saveAndFlush(apply);

			apply = applySaved;
			curriculaCopied.setApply(applySaved);
			curriculaService.save(curriculaCopied);
			Collection<Apply> applies = offer.getApplies();
			applies.add(apply);
			offer.setApplies(applies);
			Offer offerSaved = offerService.saveAndFlush(offer);
			return offerSaved;
		} catch (Exception e) {
			return null;

		}

	}

	public void rejectedAppliesPendingWeekAfter() {
		List<Apply> applies = applyRepository.appliesPending();
		Calendar calendar = Calendar.getInstance();
		calendar.add(Calendar.DAY_OF_MONTH, -7);
		if (applies != null && !applies.isEmpty())
			for (Apply a : applies)
				if (calendar.getTime().after(a.getOffer().getDeadLine())) {
					a.setStatus("rejected");
					save(a);
				}
	}
}
