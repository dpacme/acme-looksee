
package services;

import java.util.Collection;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import domain.Candidate;
import domain.Curricula;
import domain.EducationRecord;
import repositories.EducationRecordRepository;

@Service
@Transactional
public class EducationRecordService {

	// Managed Repository --------------------------------------------------------------
	@Autowired
	private EducationRecordRepository educationRecordRepository;


	// Constructor methods --------------------------------------------------------------
	public EducationRecordService() {
		super();
	}

	// Supporting services --------------------------------------------------------------

	// Simple CRUD methods --------------------------------------------------------------

	public EducationRecord findOne(final int educationRecordId) {
		Assert.isTrue(educationRecordId != 0);
		EducationRecord result;

		result = this.educationRecordRepository.findOne(educationRecordId);
		Assert.notNull(result);

		return result;
	}

	public Collection<EducationRecord> findAll() {
		Collection<EducationRecord> result;

		result = this.educationRecordRepository.findAll();
		Assert.notNull(result);

		return result;
	}

	public void save(final EducationRecord educationRecord) {
		Assert.notNull(educationRecord);
		if (educationRecord.getEndDate() != null)
			Assert.isTrue(educationRecord.getStartDate().before(educationRecord.getEndDate()), "The end date must be after the start date");
		this.educationRecordRepository.save(educationRecord);

	}
	
	public EducationRecord saveAndFlush(EducationRecord educationRecord) {
		Assert.notNull(educationRecord);

		return this.educationRecordRepository.saveAndFlush(educationRecord);
	}

	public EducationRecord create(final Curricula curricula) {
		final EducationRecord educationRecord = new EducationRecord();
		educationRecord.setCurricula(curricula);
		curricula.getEducationRecords().add(educationRecord);
		return educationRecord;
	}

	public void delete(final EducationRecord educationRecord) {
		Assert.notNull(educationRecord);
		this.educationRecordRepository.delete(educationRecord);
	}

	public EducationRecord createEndorser() {
		final EducationRecord educationRecord = new EducationRecord();

		return educationRecord;
	}
	// Other bussines methods -----------------------------------------------------

	public void comprobacion(final EducationRecord educationRecord) {
		Assert.isTrue(!(educationRecord.getTitle().isEmpty()));
		Assert.isTrue(!(educationRecord.getStartDate() == null));
		Assert.isTrue(!(educationRecord.getInstitution().isEmpty()));
	}


}
