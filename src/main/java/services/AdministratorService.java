
package services;

import java.util.Collection;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import domain.Administrator;
import repositories.AdministratorRepository;
import security.Authority;
import security.LoginService;
import security.UserAccount;

@Service
@Transactional
public class AdministratorService {

	// Managed Repository --------------------------------------------------------------
	@Autowired
	private AdministratorRepository administratorRepository;


	// Constructor methods --------------------------------------------------------------
	public AdministratorService() {
		super();
	}

	// Supporting services --------------------------------------------------------------

	// Simple CRUD methods --------------------------------------------------------------

	public Administrator create() {
		final Administrator administrator = new Administrator();
		return administrator;
	}

	public Administrator findOne(final int actorId) {
		Assert.isTrue(actorId != 0);
		Administrator result;

		result = administratorRepository.findOne(actorId);
		Assert.notNull(result);

		return result;
	}

	public Collection<Administrator> findAll() {
		Collection<Administrator> result;

		result = administratorRepository.findAll();
		Assert.notNull(result);

		return result;
	}

	public void save(final Administrator administrator) {
		Assert.notNull(administrator);

		administratorRepository.save(administrator);
	}

	// Other bussines methods -----------------------------------------------------

	public Administrator findByPrincipal() {
		Administrator result;
		UserAccount userAccount;

		userAccount = LoginService.getPrincipal();
		Assert.notNull(userAccount);

		result = administratorRepository.findByUserAccount(userAccount);
		Assert.notNull(result);

		return result;
	}

	/**
	 * M�todo que comprueba si existe un administrador logado
	 */
	public boolean isAdminLogged() {
		Administrator result;
		boolean logado = false;
		UserAccount userAccount;
		try {
			userAccount = LoginService.getPrincipal();
			Assert.notNull(userAccount);
			result = administratorRepository.findByUserAccount(userAccount);
			logado = result != null;
		} catch (final Exception e) {
			logado = false;
		}
		return logado;
	}

	public void comprobacion(final Administrator administrator) {
		if (administrator.getPostalAddress() != "") {
			Assert.isTrue(administrator.getPostalAddress().matches("^(\\d{5}$)"), "The format of the postaladdress is incorrect");
		}

	}

	public void checkIfAdministrator() {
		boolean res = false;

		Collection<Authority> authority;
		authority = LoginService.getPrincipal().getAuthorities();
		for (final Authority a : authority) {
			if (a.getAuthority().equals(Authority.ADMINISTRATOR)) {
				res = true;
			}
		}
		Assert.isTrue(res);
	}
}
