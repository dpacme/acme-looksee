
package services;

import java.util.ArrayList;
import java.util.Collection;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import domain.Candidate;
import domain.Curricula;
import domain.EducationRecord;
import domain.PersonalRecord;
import repositories.PersonalRecordRepository;

@Service
@Transactional
public class PersonalRecordService {

	// Managed Repository --------------------------------------------------------------
	@Autowired
	private PersonalRecordRepository personalRecordRepository;


	// Constructor methods --------------------------------------------------------------
	public PersonalRecordService() {
		super();
	}


	// Supporting services --------------------------------------------------------------

	@Autowired
	private CandidateService candidateService;


	// Simple CRUD methods --------------------------------------------------------------

	public PersonalRecord findOne(final int candidateId) {
		Assert.isTrue(candidateId != 0);
		final Candidate candidate = this.candidateService.findByPrincipal();
		PersonalRecord result = new PersonalRecord();

		for (final Curricula c : candidate.getCurriculas()) {
			result = c.getPersonalRecord();
			break;
		}
		Assert.notNull(result);

		return result;
	}

	public Collection<PersonalRecord> findAll() {
		Collection<PersonalRecord> result;

		result = this.personalRecordRepository.findAll();
		Assert.notNull(result);

		return result;
	}

	public PersonalRecord save(final PersonalRecord personalRecord) {
		Assert.notNull(personalRecord);
		return this.personalRecordRepository.save(personalRecord);
	}
	
	public PersonalRecord saveAndFlush(PersonalRecord personalRecord) {
		Assert.notNull(personalRecord);

		return this.personalRecordRepository.saveAndFlush(personalRecord);
	}

	public PersonalRecord create() {
		final PersonalRecord personalRecord = new PersonalRecord();
		final Collection<Curricula> curriculas = new ArrayList<Curricula>();
		personalRecord.setCurriculas(curriculas);
		return personalRecord;
	}

	public void delete(final PersonalRecord personalRecord) {
		Assert.notNull(personalRecord);
		this.personalRecordRepository.delete(personalRecord);
	}

	// Other bussines methods -----------------------------------------------------

	public Curricula obtenerUltimaCurricula(final PersonalRecord p) {
		Curricula curricula = null;
		for (final Curricula c : p.getCurriculas())
			curricula = c;
		return curricula;
	}

	public void comprobacion(final PersonalRecord personalRecord) {
		Assert.isTrue(!(personalRecord.getFullName().isEmpty()));
		Assert.isTrue(!(personalRecord.getEmail().isEmpty()));
		Assert.isTrue(!(personalRecord.getPicture().isEmpty()));
	}

}
