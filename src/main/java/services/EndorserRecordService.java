
package services;

import java.util.Collection;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import domain.Curricula;
import domain.EducationRecord;
import domain.EndorserRecord;
import repositories.EndorserRecordRepository;

@Service
@Transactional
public class EndorserRecordService {

	// Managed Repository --------------------------------------------------------------
	@Autowired
	private EndorserRecordRepository endorserRecordRepository;


	// Constructor methods --------------------------------------------------------------
	public EndorserRecordService() {
		super();
	}

	// Supporting services --------------------------------------------------------------

	// Simple CRUD methods --------------------------------------------------------------

	public EndorserRecord findOne(final int endorserRecordId) {
		Assert.isTrue(endorserRecordId != 0);
		EndorserRecord result;

		result = this.endorserRecordRepository.findOne(endorserRecordId);
		Assert.notNull(result);

		return result;
	}

	public Collection<EndorserRecord> findAll() {
		Collection<EndorserRecord> result;

		result = this.endorserRecordRepository.findAll();
		Assert.notNull(result);

		return result;
	}

	public void save(final EndorserRecord endorserRecord) {
		Assert.notNull(endorserRecord);
		this.endorserRecordRepository.save(endorserRecord);
	}
	

	public EndorserRecord create(final Curricula curricula) {
		final EndorserRecord endorserRecord = new EndorserRecord();
		endorserRecord.setCurricula(curricula);
		curricula.getEndorserRecords().add(endorserRecord);
		return endorserRecord;
	}

	public void delete(final EndorserRecord endorserRecord) {
		Assert.notNull(endorserRecord);
		this.endorserRecordRepository.delete(endorserRecord);
	}

	public EndorserRecord saveAndFlush(final EndorserRecord endorserRecord) {
		Assert.notNull(endorserRecord);

		return this.endorserRecordRepository.saveAndFlush(endorserRecord);
	}
	public EndorserRecord createEndorser() {
		final EndorserRecord endorserRecord = new EndorserRecord();
		return endorserRecord;
	}
	// Other bussines methods -----------------------------------------------------

	public void comprobacion(final EndorserRecord endorserRecord) {
		Assert.isTrue(!(endorserRecord.getFullName().isEmpty()));
		Assert.isTrue(!(endorserRecord.getEmail().isEmpty()));
		Assert.isTrue(!(endorserRecord.getLinkedInUrl().isEmpty()));
	}

}
