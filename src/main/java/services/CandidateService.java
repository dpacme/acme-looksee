
package services;

import java.util.ArrayList;
import java.util.Collection;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.encoding.Md5PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import domain.Actor;
import domain.Candidate;
import domain.Curricula;
import forms.ActorForm;
import repositories.CandidateRepository;
import security.Authority;
import security.LoginService;
import security.UserAccount;

@Service
@Transactional
public class CandidateService {

	// Managed Repository --------------------------------------------------------------
	@Autowired
	private CandidateRepository candidateRepository;


	// Constructor methods --------------------------------------------------------------
	public CandidateService() {
		super();
	}


	// Supporting services --------------------------------------------------------------

	@Autowired
	private ActorService actorService;


	// Simple CRUD methods --------------------------------------------------------------

	public Candidate findOne(final int actorId) {
		Assert.isTrue(actorId != 0);
		Candidate result;

		result = this.candidateRepository.findOne(actorId);
		Assert.notNull(result);

		return result;
	}

	public Collection<Candidate> findAll() {
		Collection<Candidate> result;

		result = this.candidateRepository.findAll();
		Assert.notNull(result);

		return result;
	}

	public void save(final Candidate candidate) {
		Assert.notNull(candidate);

		this.candidateRepository.save(candidate);
	}
	
	public Candidate saveAndFlush(Candidate candidate) {
		Assert.notNull(candidate);

		return this.candidateRepository.saveAndFlush(candidate);
	}

	// Other bussines methods -----------------------------------------------------

	public Candidate findByPrincipal() {
		Candidate result;
		UserAccount userAccount;

		userAccount = LoginService.getPrincipal();
		Assert.notNull(userAccount);

		result = this.candidateRepository.findByUserAccount(userAccount);
		Assert.notNull(result);

		return result;
	}

	public Candidate reconstruct(final ActorForm a) {
		final Candidate candidate = new Candidate();
		final Collection<Curricula> curriculas = new ArrayList<>();

		final UserAccount account = new UserAccount();
		account.setPassword(a.getPassword());
		account.setUsername(a.getUsername());

		candidate.setId(0);
		candidate.setVersion(0);
		candidate.setName(a.getName());
		candidate.setSurname(a.getSurname());
		candidate.setEmail(a.getEmail());
		candidate.setPhone(a.getPhone());
		candidate.setPostalAddress(a.getPostalAddress());

		candidate.setUserAccount(account);

		candidate.setCurriculas(curriculas);
		if (candidate.getPostalAddress() != "")
			Assert.isTrue(candidate.getPostalAddress().matches("^(\\d{5}$)"), "The format of the postaladdress is incorrect");

		Assert.isTrue(a.getPassword().equals(a.getSecondPassword()), "Passwords do not match");

		Assert.isTrue(a.getCheckBox(), "You must accept the term and conditions");

		return candidate;
	}

	public void saveForm(Candidate candidate) {

		Assert.notNull(candidate);
		String password;
		final Md5PasswordEncoder md5PassWordEncoder = new Md5PasswordEncoder();

		final Collection<Authority> auths = new ArrayList<>();
		final Authority auth = new Authority();
		auth.setAuthority("CANDIDATE");
		auths.add(auth);

		password = candidate.getUserAccount().getPassword();
		password = md5PassWordEncoder.encodePassword(password, null);

		candidate.getUserAccount().setPassword(password);

		candidate.getUserAccount().setAuthorities(auths);

		candidate = this.candidateRepository.saveAndFlush(candidate);
	}

	public void comprobacion(final Candidate candidate) {
		if (candidate.getPostalAddress() != "")
			Assert.isTrue(candidate.getPostalAddress().matches("^(\\d{5}$)"), "The format of the postaladdress is incorrect");
		for (final Actor actor : this.actorService.findAll())
			Assert.isTrue(!actor.getUserAccount().getUsername().equals(candidate.getUserAccount().getUsername()));
	}

	public void comprobacion2(final Candidate candidate) {
		if (candidate.getPostalAddress() != "")
			Assert.isTrue(candidate.getPostalAddress().matches("^(\\d{5}$)"), "The format of the postaladdress is incorrect");
	}

	public void checkIfCandidate() {
		boolean res = false;

		Collection<Authority> authority;
		authority = LoginService.getPrincipal().getAuthorities();
		for (final Authority a : authority)
			if (a.getAuthority().equals(Authority.CANDIDATE))
				res = true;
		Assert.isTrue(res);
	}
	public Collection<Candidate> candidatesInDescendingOrderByNumberOfCurricula() {
		Collection<Candidate> result;

		result = this.candidateRepository.candidatesInDescendingOrderByNumberOfCurricula();

		return result;
	}

	public Collection<Candidate> candidatesWithMoreCurriculas() {
		try {
			Collection<Candidate> result;

			result = this.candidateRepository.candidatesWithMoreCurriculas();

			return result;
		} catch (final Exception e) {
			return new ArrayList<Candidate>();
		}
	}

}
