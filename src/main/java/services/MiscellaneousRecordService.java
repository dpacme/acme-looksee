
package services;

import java.util.Collection;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import domain.Curricula;
import domain.MiscellaneousRecord;
import repositories.MiscellaneousRecordRepository;

@Service
@Transactional
public class MiscellaneousRecordService {

	// Managed Repository --------------------------------------------------------------
	@Autowired
	private MiscellaneousRecordRepository miscellaneousRecordRepository;


	// Constructor methods --------------------------------------------------------------
	public MiscellaneousRecordService() {
		super();
	}

	// Supporting services --------------------------------------------------------------

	// Simple CRUD methods --------------------------------------------------------------

	public MiscellaneousRecord findOne(final int miscellaneousRecordId) {
		Assert.isTrue(miscellaneousRecordId != 0);
		MiscellaneousRecord result;

		result = this.miscellaneousRecordRepository.findOne(miscellaneousRecordId);
		Assert.notNull(result);

		return result;
	}

	public Collection<MiscellaneousRecord> findAll() {
		Collection<MiscellaneousRecord> result;

		result = this.miscellaneousRecordRepository.findAll();
		Assert.notNull(result);

		return result;
	}

	public void save(final MiscellaneousRecord miscellaneousRecord) {
		Assert.notNull(miscellaneousRecord);
		this.miscellaneousRecordRepository.save(miscellaneousRecord);
	}

	public MiscellaneousRecord create(final Curricula curricula) {
		final MiscellaneousRecord miscellaneousRecord = new MiscellaneousRecord();
		miscellaneousRecord.setCurricula(curricula);
		curricula.getMiscellaneousRecords().add(miscellaneousRecord);
		return miscellaneousRecord;
	}

	public void delete(final MiscellaneousRecord miscellaneousRecord) {
		Assert.notNull(miscellaneousRecord);
		this.miscellaneousRecordRepository.delete(miscellaneousRecord);
	}

	public MiscellaneousRecord saveAndFlush(final MiscellaneousRecord miscellaneousRecord) {
		Assert.notNull(miscellaneousRecord);

		return this.miscellaneousRecordRepository.saveAndFlush(miscellaneousRecord);
	}

	public MiscellaneousRecord createMiscellaneous() {
		final MiscellaneousRecord miscellaneousRecord = new MiscellaneousRecord();
		return miscellaneousRecord;
	}
	// Other bussines methods -----------------------------------------------------

	public void comprobacion(final MiscellaneousRecord miscellaneousRecord) {
		Assert.isTrue(!(miscellaneousRecord.getTitle().isEmpty()));
	}

}
