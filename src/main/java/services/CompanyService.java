
package services;

import java.util.ArrayList;
import java.util.Collection;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.encoding.Md5PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import repositories.CompanyRepository;
import security.Authority;
import security.LoginService;
import security.UserAccount;
import domain.Actor;
import domain.Administrator;
import domain.Company;
import domain.Offer;
import forms.CompanyForm;

@Service
@Transactional
public class CompanyService {

	// Managed Repository --------------------------------------------------------------
	@Autowired
	private CompanyRepository		companyRepository;

	@Autowired
	private AdministratorService	administratorService;

	@Autowired
	private ActorService			actorService;


	// Constructor methods --------------------------------------------------------------
	public CompanyService() {
		super();
	}

	// Supporting services --------------------------------------------------------------

	// Simple CRUD methods --------------------------------------------------------------

	public Company create() {
		final Company company = new Company();
		return company;
	}

	public Company findOne(final int actorId) {
		Assert.isTrue(actorId != 0);
		Company result;

		result = this.companyRepository.findOne(actorId);
		Assert.notNull(result);

		return result;
	}

	public Collection<Company> findAll() {
		Collection<Company> result;

		result = this.companyRepository.findAll();
		Assert.notNull(result);

		return result;
	}

	public void save(final Company company) {
		Assert.notNull(company);

		this.companyRepository.save(company);
	}

	public Company saveAndFlush(final Company Company) {
		Assert.notNull(Company);

		return this.companyRepository.saveAndFlush(Company);
	}

	// Other bussines methods -----------------------------------------------------

	public Company findByPrincipal() {
		Company result;
		UserAccount userAccount;

		userAccount = LoginService.getPrincipal();
		Assert.notNull(userAccount);

		result = this.companyRepository.findByUserAccount(userAccount);
		Assert.notNull(result);

		return result;
	}

	public Company reconstruct(final CompanyForm a) {
		final Company company = new Company();
		final Collection<Offer> offers = new ArrayList<>();

		final UserAccount account = new UserAccount();
		account.setPassword(a.getPassword());
		account.setUsername(a.getUsername());

		company.setId(0);
		company.setVersion(0);
		company.setName(a.getName());
		company.setSurname(a.getSurname());
		company.setEmail(a.getEmail());
		company.setPhone(a.getPhone());
		company.setPostalAddress(a.getPostalAddress());

		//ATRIBUTOS DE COMPANY
		company.setBanned(false);
		company.setVatNumber(a.getVatNumber());
		company.setNameCompany(a.getNameCompany());

		company.setUserAccount(account);

		company.setOffers(offers);
		if (company.getPostalAddress() != "")
			Assert.isTrue(company.getPostalAddress().matches("^(\\d{5}$)"), "The format of the postaladdress is incorrect");

		Assert.isTrue(a.getPassword().equals(a.getSecondPassword()), "Passwords do not match");

		Assert.isTrue(a.getCheckBox(), "You must accept the term and conditions");

		return company;
	}

	public void saveForm(Company company) {

		Assert.notNull(company);
		String password;
		final Md5PasswordEncoder md5PassWordEncoder = new Md5PasswordEncoder();

		final Collection<Authority> auths = new ArrayList<>();
		final Authority auth = new Authority();
		auth.setAuthority("COMPANY");
		auths.add(auth);

		password = company.getUserAccount().getPassword();
		password = md5PassWordEncoder.encodePassword(password, null);

		company.getUserAccount().setPassword(password);

		company.getUserAccount().setAuthorities(auths);

		company = this.companyRepository.saveAndFlush(company);
	}

	public void comprobacion(final Company company) {
		if (company.getPostalAddress() != "")
			Assert.isTrue(company.getPostalAddress().matches("^(\\d{5}$)"), "The format of the postaladdress is incorrect");
		for (final Actor actor : this.actorService.findAll())
			Assert.isTrue(!actor.getUserAccount().getUsername().equals(company.getUserAccount().getUsername()));

	}

	public void comprobacion2(final Company company) {
		if (company.getPostalAddress() != "")
			Assert.isTrue(company.getPostalAddress().matches("^(\\d{5}$)"), "The format of the postaladdress is incorrect");

	}

	/**
	 * M�todo que devuelve aquellas compa��as que no est�n baneadas
	 * 
	 * @return
	 */
	public Collection<Company> findValidCompanies() {
		try {
			Collection<Company> result;
			result = this.companyRepository.findValidCompanies();
			return result;
		} catch (final IllegalArgumentException e) {
			throw e;
		} catch (final Exception e) {
			return new ArrayList<Company>();
		}
	}

	public Company banCompany(final int companyID) {
		final Administrator admin = this.administratorService.findByPrincipal();
		Assert.notNull(admin);

		final Company company = this.findOne(companyID);
		Assert.notNull(company);

		company.setBanned(true);
		return this.saveAndFlush(company);
	}

	public Company unbanCompany(final int companyID) {
		final Administrator admin = this.administratorService.findByPrincipal();
		Assert.notNull(admin);

		final Company company = this.findOne(companyID);
		Assert.notNull(company);

		company.setBanned(false);
		return this.saveAndFlush(company);
	}

	public void checkIfCompany() {
		boolean res = false;

		Collection<Authority> authority;
		authority = LoginService.getPrincipal().getAuthorities();
		for (final Authority a : authority)
			if (a.getAuthority().equals(Authority.COMPANY))
				res = true;
		Assert.isTrue(res);
	}

	/**
	 * M�todo que comprueba si existe un administrador logado
	 */
	public boolean isCompanyLogged() {
		Company result;
		boolean logado = false;
		UserAccount userAccount;
		try {
			userAccount = LoginService.getPrincipal();
			Assert.notNull(userAccount);
			result = this.companyRepository.findByUserAccount(userAccount);
			logado = result != null;
		} catch (final Exception e) {
			logado = false;
		}
		return logado;
	}

	public Collection<Company> companiesInDescendingOrderByNumberOfOffers() {
		Collection<Company> result;

		result = this.companyRepository.companiesInDescendingOrderByNumberOfOffers();

		return result;
	}

	public Collection<Company> companiesWithMoreOffers() {
		try {
			Collection<Company> result;

			result = this.companyRepository.companiesWithMoreOffers();

			return result;
		} catch (final Exception e) {
			return new ArrayList<Company>();
		}
	}

}
