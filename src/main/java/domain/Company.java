
package domain;

import java.util.Collection;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotBlank;
import org.hibernate.validator.constraints.SafeHtml;
import org.hibernate.validator.constraints.SafeHtml.WhiteListType;

@Entity
@Access(AccessType.PROPERTY)
public class Company extends Actor {

	private String	nameCompany;
	private String	vatNumber;
	private Boolean	banned;


	@NotBlank
	@SafeHtml(whitelistType = WhiteListType.NONE)
	public String getNameCompany() {
		return nameCompany;
	}

	public void setNameCompany(final String nameCompany) {
		this.nameCompany = nameCompany;
	}

	@NotBlank
	@Column(unique = true)
	@SafeHtml(whitelistType = WhiteListType.NONE)
	public String getVatNumber() {
		return vatNumber;
	}

	public void setVatNumber(final String vatNumber) {
		this.vatNumber = vatNumber;
	}

	@NotNull
	public Boolean getBanned() {
		return banned;
	}

	public void setBanned(final Boolean banned) {
		this.banned = banned;
	}


	// Relationships -------------------------------------------

	private Collection<Offer> offers;


	@Valid
	@OneToMany(mappedBy = "company")
	public Collection<Offer> getOffers() {
		return offers;
	}

	public void setOffers(final Collection<Offer> offers) {
		this.offers = offers;
	}

}
