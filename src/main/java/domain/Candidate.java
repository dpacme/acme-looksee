
package domain;

import java.util.Collection;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.validation.Valid;

@Entity
@Access(AccessType.PROPERTY)
public class Candidate extends Actor {

	// Relationships -------------------------------------------

	private Collection<Curricula> curriculas;


	@Valid
	@OneToMany(mappedBy = "candidate")
	public Collection<Curricula> getCurriculas() {
		return this.curriculas;
	}

	public void setCurriculas(final Collection<Curricula> curriculas) {
		this.curriculas = curriculas;
	}

}
