
package controllers;

import java.util.ArrayList;
import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.Assert;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import services.ApplyService;
import services.CandidateService;
import services.CurriculaService;
import domain.Apply;
import domain.Candidate;
import domain.Curricula;
import domain.PersonalRecord;

@Controller
@RequestMapping("/curricula")
public class CurriculaController extends AbstractController {

	// Services

	@Autowired
	private CurriculaService	curriculaService;

	@Autowired
	private CandidateService	candidateService;

	@Autowired
	private ApplyService		applyService;


	// Constructors -----------------------------------------------------------

	public CurriculaController() {
		super();
	}

	// Methods ------------------------------------------------------------------

	@RequestMapping(value = "/all", method = RequestMethod.GET)
	public ModelAndView all() {
		ModelAndView result;
		Collection<Curricula> aux = new ArrayList<Curricula>();
		Collection<Curricula> res = new ArrayList<Curricula>();
		final Candidate candidate = this.candidateService.findByPrincipal();

		aux = candidate.getCurriculas();
		res = this.curriculaService.filterCurriculas(aux);

		result = new ModelAndView("curricula/all");
		result.addObject("curriculas", res);
		result.addObject("requestURI", "curricula/all.do");

		return result;
	}

	@RequestMapping(value = "/search", method = RequestMethod.POST, params = "search")
	public ModelAndView search(final String keyword) {
		ModelAndView result;
		final Collection<Curricula> aux;
		final Collection<Curricula> res;

		aux = this.curriculaService.searchCurricula(keyword);
		res = this.curriculaService.filterCurriculas(aux);

		result = new ModelAndView("curricula/all");
		result.addObject("curriculas", res);
		result.addObject("requestURI", "curricula/search.do");

		return result;
	}

	@RequestMapping(value = "/search", method = RequestMethod.GET, params = "search")
	public ModelAndView search2(final String keyword) {
		ModelAndView result;
		final Collection<Curricula> aux;
		final Collection<Curricula> res;

		aux = this.curriculaService.searchCurricula(keyword);
		res = this.curriculaService.filterCurriculas(aux);

		result = new ModelAndView("curricula/all");
		result.addObject("curriculas", res);
		result.addObject("requestURI", "curricula/search.do");

		return result;
	}

	// VIEW
	@RequestMapping(value = "/view", method = RequestMethod.GET)
	public ModelAndView view(@RequestParam final int curriculaID) {
		ModelAndView result;

		try {
			final Curricula curricula = this.curriculaService.findOne(curriculaID);
			final PersonalRecord personalRecord = curricula.getPersonalRecord();

			Assert.isTrue(curricula.getCandidate().getId() == this.candidateService.findByPrincipal().getId());

			result = new ModelAndView("curricula/view");
			result.addObject("curricula", curricula);
			result.addObject("personalRecord", personalRecord);
			result.addObject("educationRecords", curricula.getEducationRecords());
			result.addObject("professionalRecords", curricula.getProfessionalRecords());
			result.addObject("endorserRecords", curricula.getEndorserRecords());
			result.addObject("miscellaneousRecords", curricula.getMiscellaneousRecords());
			result.addObject("requestURI", "curricula/view.do");

		} catch (final Throwable oops) {
			result = new ModelAndView("misc/error");
			result.addObject("codigoError", "error.authorization");
		}

		return result;

	}

	//Delete
	@RequestMapping(value = "/delete", method = RequestMethod.GET)
	public ModelAndView delete(@RequestParam final int curriculaID) {
		ModelAndView res;
		final Curricula curricula = this.curriculaService.findOne(curriculaID);

		try {
			Assert.isTrue(curricula.getCandidate().getId() == this.candidateService.findByPrincipal().getId(), "No estas autorizado");
			this.curriculaService.delete(curricula);
			res = new ModelAndView("redirect:/curricula/all.do");
		} catch (final Throwable oops) {
			res = new ModelAndView("misc/error");
			res.addObject("codigoError", "error.authorization");
		}

		return res;
	}

	@RequestMapping(value = "/show", method = RequestMethod.GET)
	public ModelAndView show(@RequestParam final Integer applyID) {
		ModelAndView result;

		final Apply apply = this.applyService.findOne(applyID);
		final Curricula curricula = this.curriculaService.findOne(apply.getCurricula().getId());
		final PersonalRecord personalRecord = curricula.getPersonalRecord();

		result = new ModelAndView("curricula/view");
		result.addObject("curricula", curricula);
		result.addObject("personalRecord", personalRecord);
		result.addObject("educationRecords", curricula.getEducationRecords());
		result.addObject("professionalRecords", curricula.getProfessionalRecords());
		result.addObject("endorserRecords", curricula.getEndorserRecords());
		result.addObject("miscellaneousRecords", curricula.getMiscellaneousRecords());
		result.addObject("requestURI", "curricula/show.do");

		return result;
	}
}
