
package controllers;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import domain.Administrator;
import domain.Apply;
import domain.Candidate;
import domain.Company;
import domain.Offer;
import services.AdministratorService;
import services.CandidateService;
import services.CompanyService;
import services.CurriculaService;
import services.OfferService;

@Controller
@RequestMapping("/offer")
public class OfferController extends AbstractController {

	// Services

	@Autowired
	private OfferService			offerService;

	@Autowired
	private AdministratorService	administratorService;

	@Autowired
	private CompanyService			companyService;

	@Autowired
	private CandidateService		candidateService;

	@Autowired
	private CurriculaService		curriculaService;


	// Constructors -----------------------------------------------------------

	public OfferController() {
		super();
	}

	// Methods ------------------------------------------------------------------

	@RequestMapping(value = "/all", method = RequestMethod.GET)
	public ModelAndView all() {
		ModelAndView result;
		Collection<Offer> res = new ArrayList<Offer>();
		Boolean isAdmin = false;

		if (administratorService.isAdminLogged()) {
			res = offerService.findAll();
			isAdmin = true;
		} else {
			res = offerService.filter(null);
			//Si est� logada una compa��a y est� baneada, se muestra para ella sus propias ofertas
			if (companyService.isCompanyLogged()) {
				final Company company = companyService.findByPrincipal();
				if (company.getBanned()) {
					res.addAll(offerService.filterNoDrafts(company.getOffers()));
				}
			}
		}

		result = new ModelAndView("offer/all");
		result.addObject("offers", res);
		result.addObject("isAdmin", isAdmin);
		result.addObject("pagesize", 5);
		result.addObject("requestURI", "offer/all.do");

		return result;
	}
	@RequestMapping(value = "/offersFromCompany", method = RequestMethod.GET)
	public ModelAndView offersFromCompany(@RequestParam final int companyId) {
		ModelAndView result;

		Company company = companyService.findOne(companyId);

		if (company.getBanned() && !administratorService.isAdminLogged()) {
			result = new ModelAndView("misc/error");
			result.addObject("codigoError", "error.authorization");
		} else {
			final Collection<Offer> res = offerService.filter(company.getOffers());

			//Si est� logada una compa��a y ve su listado de ofertas y est� baneada, se muestra para ella sus propias ofertas
			if (companyService.isCompanyLogged()) {
				Company companyLogged = companyService.findByPrincipal();
				if (company.getBanned() && company.getId() == companyLogged.getId()) {
					res.addAll(offerService.filterNoDrafts(companyLogged.getOffers()));
				}
			}

			result = new ModelAndView("offer/offersFromCompany");
			result.addObject("offers", res);
			result.addObject("pagesize", 5);
			result.addObject("requestURI", "offer/offersFromCompany.do");
		}

		return result;
	}
	@RequestMapping(value = "/search", method = RequestMethod.POST, params = "search")
	public ModelAndView search(final String keyword, final Double minSalary, final Double maxSalary) {
		ModelAndView result;
		Collection<Offer> foundOffers = new ArrayList<>();
		final Collection<Offer> res;

		foundOffers = offerService.searchOffer(keyword, minSalary, maxSalary);
		res = offerService.filter(foundOffers);

		result = new ModelAndView("offer/all");
		result.addObject("offers", res);
		result.addObject("pagesize", "");
		result.addObject("requestURI", "offer/search.do");

		return result;
	}

	@RequestMapping(value = "/search", method = RequestMethod.GET, params = "search")
	public ModelAndView searchGet(final String keyword, final Double minSalary, final Double maxSalary) {
		ModelAndView result;
		Collection<Offer> foundOffers = new ArrayList<>();
		final Collection<Offer> res;

		foundOffers = offerService.searchOffer(keyword, minSalary, maxSalary);
		res = offerService.filter(foundOffers);

		result = new ModelAndView("offer/all");
		result.addObject("offers", res);
		result.addObject("pagesize", "");
		result.addObject("requestURI", "offer/search.do");

		return result;
	}
	@RequestMapping(value = "/showDisplay", method = RequestMethod.GET)
	public ModelAndView showDisplay(@RequestParam final int offerId) {
		ModelAndView result;

		Offer offer = offerService.findOne(offerId);
		Company company;
		Administrator administrator = administratorService.create();
		try {
			company = companyService.findByPrincipal();
		} catch (final Exception e) {
			company = null;
		}

		boolean tienePermiso = true;

		try {
			administrator = administratorService.findByPrincipal();
		} catch (Exception e) {
			administrator = null;
		}

		if (administrator == null) {
			if (offer.getDraft() || offer.getCompany().getBanned()) {
				if (company != null) {
					if (offer.getCompany().getId() != company.getId()) {
						tienePermiso = false;
					}
				} else {
					tienePermiso = false;
				}
			}
		}

		if (tienePermiso) {
			//Comprobamos si el actor logado ya ha realizado un apply a este offer
			boolean applyRealized = false;
			result = new ModelAndView("offer/showDisplay");

			try {
				final Candidate candidate = candidateService.findByPrincipal();
				if (candidate != null) {
					Collection<Apply> applies = offer.getApplies();
					for (final Apply apply : applies) {
						if (apply.getCurricula().getCandidate().getId() == candidate.getId()) {
							applyRealized = true;
						}
					}
					result.addObject("allCurriculas", curriculaService.findCurriculasWithoutCopy(candidate.getId()));
				}
			} catch (final Throwable oops) {
				System.out.println(oops.getLocalizedMessage());
			}
			result.addObject("requestURI", "offer/showDisplay.do");
			result.addObject("offer", offer);
			result.addObject("applyRealized", applyRealized);
			result.addObject("currentDate", new Date());
		} else {
			result = new ModelAndView("misc/error");
			result.addObject("codigoError", "error.authorization");
		}

		return result;

	}

}
