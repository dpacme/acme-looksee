/*
 * AdministratorController.java
 *
 * Copyright (C) 2017 Universidad de Sevilla
 *
 * The use of this project is hereby constrained to the conditions of the
 * TDG Licence, a copy of which you may download from
 * http://www.tdg-seville.info/License.html
 */

package controllers;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import domain.Administrator;
import services.AdministratorService;

@Controller
@RequestMapping("/administrator")
public class AdministratorController extends AbstractController {

	// Services

	@Autowired
	private AdministratorService administratorService;


	// Constructors -----------------------------------------------------------

	public AdministratorController() {
		super();
	}

	// (MODIFICAR DATOS) Modificar datos
	@RequestMapping(value = "/edit", method = RequestMethod.GET)
	public ModelAndView edit() {
		ModelAndView res;
		final Administrator administrator = this.administratorService.findByPrincipal();

		res = this.createFormModelAndView(administrator);
		return res;
	}

	// (MODIFICAR DATOS) Guardar en la base de datos el nuevo candidate
	@RequestMapping(value = "/edit", method = RequestMethod.POST, params = "save")
	public ModelAndView saveEdit(@Valid final Administrator administrator, final BindingResult binding) {
		ModelAndView res;

		if (binding.hasErrors()) {
			res = this.createFormModelAndView(administrator);
			System.out.println(binding.getAllErrors());
		} else
			try {
				this.administratorService.comprobacion(administrator);
				this.administratorService.save(administrator);
				res = new ModelAndView("redirect:/");

			} catch (final Throwable oops) {
				System.out.println(oops);
				res = this.createFormModelAndView(administrator);
				if (oops.getLocalizedMessage().contains("The format of the postaladdress is incorrect"))
					res.addObject("postal", "postal");
			}

		return res;
	}

	// Creaci�n de ModelAndView para edit
	protected ModelAndView createFormModelAndView(final Administrator administrator) {
		ModelAndView res;

		res = this.createFormModelAndView(administrator, null);

		return res;
	}

	protected ModelAndView createFormModelAndView(final Administrator administrator, final String message) {
		ModelAndView res;

		res = new ModelAndView("administrator/edit");
		res.addObject("administrator", administrator);
		res.addObject("message", message);

		return res;
	}

}
