
package controllers.company;

import java.util.ArrayList;
import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import controllers.AbstractController;
import domain.Company;
import domain.Offer;
import services.CompanyService;

@Controller
@RequestMapping("/offer/company")
public class OfferCompanyController extends AbstractController {

	// Services

	@Autowired
	private CompanyService companyService;


	// Constructors -----------------------------------------------------------

	public OfferCompanyController() {
		super();
	}

	// Methods ------------------------------------------------------------------

	@RequestMapping(value = "/myOffers", method = RequestMethod.GET)
	public ModelAndView myOffers() {
		ModelAndView result;
		Collection<Offer> myOffers = new ArrayList<Offer>();

		final Company company = this.companyService.findByPrincipal();

		myOffers = company.getOffers();

		result = new ModelAndView("offer/myOffers");
		result.addObject("offers", myOffers);
		result.addObject("requestURI", "offer/company/myOffers.do");

		return result;
	}

}
