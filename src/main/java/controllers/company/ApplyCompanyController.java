
package controllers.company;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import services.ApplyService;
import services.CompanyService;
import services.OfferService;
import controllers.AbstractController;
import domain.Apply;
import domain.Company;
import domain.Offer;

@Controller
@RequestMapping("/apply/company")
public class ApplyCompanyController extends AbstractController {

	// Services

	@Autowired
	private ApplyService	applyService;

	@Autowired
	private CompanyService	companyService;

	@Autowired
	private OfferService	offerService;


	// Constructors -----------------------------------------------------------

	public ApplyCompanyController() {
		super();
	}

	// Methods ------------------------------------------------------------------

	@RequestMapping(value = "/listAppliesFromOffer", method = RequestMethod.GET)
	public ModelAndView listAppliesFromOffer(@RequestParam int offerID) {
		ModelAndView result;

		Company company = this.companyService.findByPrincipal();
		Offer offer = offerService.findOne(offerID);

		//Se comprueba que la compa�ia logada es la due�a de la oferta
		boolean tienePermiso = company.getId() == offer.getCompany().getId();

		if (tienePermiso) {
			Collection<Apply> applies = offer.getApplies();

			result = new ModelAndView("apply/appliesFromOffer");
			result.addObject("requestURI", "apply/company/listAppliesFromOffer.do");
			result.addObject("applies", applies);
		} else {
			result = new ModelAndView("misc/error");
			result.addObject("codigoError", "error.authorization");
		}

		return result;
	}

	@RequestMapping(value = "/setApplyAsPending", method = RequestMethod.GET)
	public ModelAndView setApplyAsPending(@RequestParam int applyID) {
		ModelAndView result;

		Company company = this.companyService.findByPrincipal();
		Apply apply = applyService.findOne(applyID);

		//Se comprueba que la compa�ia logada es la due�a de la oferta
		boolean tienePermiso = company.getId() == apply.getOffer().getCompany().getId();

		if (tienePermiso) {
			this.applyService.setApplyAsPending(applyID);

			result = listAppliesFromOffer(apply.getOffer().getId());
		} else {
			result = new ModelAndView("misc/error");
			result.addObject("codigoError", "error.authorization");
		}

		return result;
	}

	@RequestMapping(value = "/setApplyAsAccepted", method = RequestMethod.GET)
	public ModelAndView setApplyAsAccepted(@RequestParam int applyID) {
		ModelAndView result;

		Company company = this.companyService.findByPrincipal();
		Apply apply = applyService.findOne(applyID);

		//Se comprueba que la compa�ia logada es la due�a de la oferta
		boolean tienePermiso = company.getId() == apply.getOffer().getCompany().getId();

		if (tienePermiso) {
			this.applyService.setApplyAsAccepted(applyID);

			result = listAppliesFromOffer(apply.getOffer().getId());
		} else {
			result = new ModelAndView("misc/error");
			result.addObject("codigoError", "error.authorization");
		}

		return result;
	}

	@RequestMapping(value = "/setApplyAsRejected", method = RequestMethod.GET)
	public ModelAndView setApplyAsRejected(@RequestParam int applyID) {
		ModelAndView result;

		Company company = this.companyService.findByPrincipal();
		Apply apply = applyService.findOne(applyID);

		//Se comprueba que la compa�ia logada es la due�a de la oferta
		boolean tienePermiso = company.getId() == apply.getOffer().getCompany().getId();

		if (tienePermiso) {
			this.applyService.setApplyAsRejected(applyID);

			result = listAppliesFromOffer(apply.getOffer().getId());
		} else {
			result = new ModelAndView("misc/error");
			result.addObject("codigoError", "error.authorization");
		}

		return result;
	}
}
