
package controllers.company;

import java.util.ArrayList;
import java.util.Collection;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.Assert;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import domain.Offer;
import services.CompanyService;
import services.OfferService;

@Controller
@RequestMapping("/company/offer")
public class CompanyOfferController {

	// Services

	@Autowired
	private OfferService	offerService;

	@Autowired
	private CompanyService	companyService;


	// Constructors -----------------------------------------------------------

	public CompanyOfferController() {
		super();
	}

	// Methods ------------------------------------------------------------------

	// Creation--------------------------------------------
	@RequestMapping(value = "/create", method = RequestMethod.GET)
	public ModelAndView create() {
		ModelAndView res;
		final Offer offer;
		offer = this.offerService.create();

		res = new ModelAndView("company/offer/create");
		res.addObject("offer", offer);
		res.addObject("requestURI", "company/offer/create.do");

		return res;
	}

	@RequestMapping(value = "/create", method = RequestMethod.POST, params = "save")
	public ModelAndView saveCreate(@Valid final Offer offer, final BindingResult binding) {
		ModelAndView result;

		if (binding.hasErrors()) {
			result = new ModelAndView("company/offer/create");
			result.addObject("offer", offer);
			result.addObject("requestURI", "company/offer/create.do");
		} else
			try {
				this.offerService.save(offer);
				result = new ModelAndView("redirect:/company/offer/drafts.do");
			} catch (final Throwable oops) {
				result = new ModelAndView("company/offer/create");
				result.addObject("offer", offer);
				result.addObject("requestURI", "company/offer/create.do");

				if (oops.getLocalizedMessage().contains("The minimum wage must be less than the maximum"))
					result.addObject("salaries", "salaries");
			}

		return result;
	}

	@RequestMapping(value = "/myOffers", method = RequestMethod.GET)
	public ModelAndView listMyOffers() {
		ModelAndView result;
		Collection<Offer> allOffers = new ArrayList<Offer>();
		Collection<Offer> res = new ArrayList<Offer>();

		allOffers = this.companyService.findByPrincipal().getOffers();

		res = this.offerService.filterNoDrafts(allOffers);

		result = new ModelAndView("company/offer/myOffers");
		result.addObject("offers", res);
		result.addObject("requestURI", "company/offer/myOffers.do");

		return result;
	}

	@RequestMapping(value = "/drafts", method = RequestMethod.GET)
	public ModelAndView listDrafts() {
		ModelAndView result;
		Collection<Offer> allOffers = new ArrayList<Offer>();
		Collection<Offer> res = new ArrayList<Offer>();

		allOffers = this.companyService.findByPrincipal().getOffers();
		res = this.offerService.filterDrafts(allOffers);

		result = new ModelAndView("company/offer/drafts");
		result.addObject("offers", res);
		result.addObject("requestURI", "company/offer/drafts.do");

		return result;
	}

	//Edit
	@RequestMapping(value = "/edit", method = RequestMethod.GET)
	public ModelAndView edit(@RequestParam final int offerID) {
		ModelAndView result;
		Offer offer;

		try {
			offer = this.offerService.findOne(offerID);
			Assert.isTrue(offer.getCompany().getId() == this.companyService.findByPrincipal().getId(), "No estas autorizado");
			Assert.isTrue(offer.getDraft(), "No estas autorizado");
			result = this.createEditModelAndView(offer);
		} catch (final Throwable oops) {
			result = new ModelAndView("misc/error");
			result.addObject("codigoError", "error.authorization");
		}

		return result;
	}

	@RequestMapping(value = "/edit", method = RequestMethod.POST, params = "save")
	public ModelAndView saveEdit(@Valid final Offer offer, final BindingResult binding) {
		ModelAndView res;

		if (binding.hasErrors()) {
			res = this.createEditModelAndView(offer);
			System.out.println(binding.getAllErrors());
		} else
			try {

				this.offerService.save(offer);
				res = new ModelAndView("redirect:/company/offer/drafts.do");

			} catch (final Throwable oops) {
				System.out.println(oops);
				res = this.createEditModelAndView(offer);
				if (oops.getLocalizedMessage().contains("The minimum wage must be less than the maximum"))
					res.addObject("salaries", "salaries");
			}

		return res;
	}

	//Delete
	@RequestMapping(value = "/edit", method = RequestMethod.POST, params = "delete")
	public ModelAndView deleteEdit(@Valid final Offer offer, final BindingResult binding) {
		ModelAndView res;

		if (binding.hasErrors())
			res = this.createEditModelAndView(offer);
		else
			try {

				this.offerService.delete(offer);
				res = new ModelAndView("redirect:/company/offer/drafts.do");

			} catch (final Throwable oops) {
				System.out.println(oops);
				res = this.createEditModelAndView(offer, "offer.commit.error");
			}

		return res;
	}

	//Publish

	@RequestMapping(value = "/publish", method = RequestMethod.GET)
	public ModelAndView publish(@RequestParam final int offerID) {
		ModelAndView res;
		try {
			final Offer offer = this.offerService.findOne(offerID);
			Assert.isTrue(offer.getCompany().getId() == this.companyService.findByPrincipal().getId(), "No estas autorizado");
			this.offerService.publish(offer);
			res = new ModelAndView("redirect:/company/offer/myOffers.do");

		} catch (final Throwable oops) {
			System.out.println(oops);
			res = new ModelAndView("");
			if (oops.getLocalizedMessage().contains("The deadline must be at least one week after the current date")) {
				res = this.listDrafts();
				res.addObject("deadline", "deadline");
			}
			if (oops.getLocalizedMessage().contains("No estas autorizado")) {
				res = new ModelAndView("misc/error");
				res.addObject("codigoError", "error.authorization");
			}
		}

		return res;
	}

	// Ancillary methods ---------------------------------------------------------------

	protected ModelAndView createEditModelAndView(final Offer offer) {
		ModelAndView result;

		result = this.createEditModelAndView(offer, null);

		return result;
	}
	protected ModelAndView createEditModelAndView(final Offer offer, final String message) {
		ModelAndView result;

		result = new ModelAndView("company/offer/edit");

		result.addObject("offer", offer);
		result.addObject("message", message);

		result.addObject("requestURI", "company/offer/edit.do");

		return result;
	}
}
