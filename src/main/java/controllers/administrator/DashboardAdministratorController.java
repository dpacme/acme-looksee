
package controllers.administrator;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import controllers.AbstractController;
import domain.Candidate;
import domain.Company;
import services.AdministratorService;
import services.ApplyService;
import services.CandidateService;
import services.CompanyService;
import services.CurriculaService;
import services.OfferService;

@Controller
@RequestMapping("administrator")
public class DashboardAdministratorController extends AbstractController {

	// Services ---------------------------------------------------------------

	@Autowired
	AdministratorService	administratorService;

	@Autowired
	CandidateService		candidateService;

	@Autowired
	CompanyService			companyService;

	@Autowired
	CurriculaService		curriculaService;

	@Autowired
	OfferService			offerService;

	@Autowired
	ApplyService			applyService;
	// Constructors -----------------------------------------------------------


	public DashboardAdministratorController() {
		super();
	}

	// Dashboard
	// ----------------------------------------------------------------

	@RequestMapping(value = "/dashboard", method = RequestMethod.GET)
	public ModelAndView dashboard() {
		ModelAndView result;

		//The listing of candidates, sorted in descending order by number of curricula.
		Collection<Candidate> candidatesInDescendingOrderByNumberOfCurricula = candidateService.candidatesInDescendingOrderByNumberOfCurricula();

		//The listing of companies, sorted in descending order by number of offers.
		Collection<Company> companiesInDescendingOrderByNumberOfOffers = companyService.companiesInDescendingOrderByNumberOfOffers();

		//The average number of curricula per candidate.
		Double averageNumberCurriculaPerCandidate = curriculaService.averageNumberCurriculaPerCandidate();

		//The average number of offers per company.
		Double averageNumberOffersPerCompany = offerService.averageNumberOffersPerCompany();

		//The candidates who have registered more curricula.
		Collection<Candidate> candidatesWithMoreCurriculas = candidateService.candidatesWithMoreCurriculas();

		//The companies that have registered more offers.
		Collection<Company> companiesWithMoreOffers = companyService.companiesWithMoreOffers();

		//The average, the minimum, and the maximum number of applications per candidate.
		Double avgNumberApplicationsPerCandidate = applyService.avgNumberApplicationsPerCandidate();
		Double minNumberApplicationsPerCandidate = applyService.minNumberApplicationsPerCandidate();
		Double maxNumberApplicationsPerCandidate = applyService.maxNumberApplicationsPerCandidate();

		//The average, the minimum, and the maximum number of applications per offer.
		Double avgNumberApplicationsPerOffer = applyService.avgNumberApplicationsPerOffer();
		Double minNumberApplicationsPerOffer = applyService.minNumberApplicationsPerOffer();
		Double maxNumberApplicationsPerOffer = applyService.maxNumberApplicationsPerOffer();

		//The average, the minimum, and the maximum number of pending applications per company.
		Double avgNumberApplicationsPendingPerCompany = applyService.avgNumberApplicationsPendingPerCompany();
		Double minNumberApplicationsPendingPerCompany = applyService.minNumberApplicationsPendingPerCompany();
		Double maxNumberApplicationsPendingPerCompany = applyService.maxNumberApplicationsPendingPerCompany();

		//The average, the minimum, and the maximum number of accepted applications per company.
		Double avgNumberApplicationsAcceptedPerCompany = applyService.avgNumberApplicationsAcceptedPerCompany();
		Double minNumberApplicationsAcceptedPerCompany = applyService.minNumberApplicationsAcceptedPerCompany();
		Double maxNumberApplicationsAcceptedPerCompany = applyService.maxNumberApplicationsAcceptedPerCompany();

		//The average, the minimum, and the maximum number of rejected applications per company.
		Double avgNumberApplicationsRejectedPerCompany = applyService.avgNumberApplicationsRejectedPerCompany();
		Double minNumberApplicationsRejectedPerCompany = applyService.minNumberApplicationsRejectedPerCompany();
		Double maxNumberApplicationsRejectedPerCompany = applyService.maxNumberApplicationsRejectedPerCompany();

		//*************************************************************************//
		result = new ModelAndView("administrator/dashboard");

		result.addObject("candidatesInDescendingOrderByNumberOfCurricula", candidatesInDescendingOrderByNumberOfCurricula);
		result.addObject("companiesInDescendingOrderByNumberOfOffers", companiesInDescendingOrderByNumberOfOffers);
		result.addObject("averageNumberCurriculaPerCandidate", averageNumberCurriculaPerCandidate);
		result.addObject("averageNumberOffersPerCompany", averageNumberOffersPerCompany);
		result.addObject("candidatesWithMoreCurriculas", candidatesWithMoreCurriculas);
		result.addObject("companiesWithMoreOffers", companiesWithMoreOffers);
		result.addObject("avgNumberApplicationsPerCandidate", avgNumberApplicationsPerCandidate);
		result.addObject("minNumberApplicationsPerCandidate", minNumberApplicationsPerCandidate);
		result.addObject("maxNumberApplicationsPerCandidate", maxNumberApplicationsPerCandidate);
		result.addObject("avgNumberApplicationsPerOffer", avgNumberApplicationsPerOffer);
		result.addObject("minNumberApplicationsPerOffer", minNumberApplicationsPerOffer);
		result.addObject("maxNumberApplicationsPerOffer", maxNumberApplicationsPerOffer);
		result.addObject("avgNumberApplicationsPendingPerCompany", avgNumberApplicationsPendingPerCompany);
		result.addObject("minNumberApplicationsPendingPerCompany", minNumberApplicationsPendingPerCompany);
		result.addObject("maxNumberApplicationsPendingPerCompany", maxNumberApplicationsPendingPerCompany);
		result.addObject("avgNumberApplicationsAcceptedPerCompany", avgNumberApplicationsAcceptedPerCompany);
		result.addObject("minNumberApplicationsAcceptedPerCompany", minNumberApplicationsAcceptedPerCompany);
		result.addObject("maxNumberApplicationsAcceptedPerCompany", maxNumberApplicationsAcceptedPerCompany);
		result.addObject("avgNumberApplicationsRejectedPerCompany", avgNumberApplicationsRejectedPerCompany);
		result.addObject("minNumberApplicationsRejectedPerCompany", minNumberApplicationsRejectedPerCompany);
		result.addObject("maxNumberApplicationsRejectedPerCompany", maxNumberApplicationsRejectedPerCompany);

		result.addObject("requestURI", "administrator/dashboard.do");

		return result;
	}

}
