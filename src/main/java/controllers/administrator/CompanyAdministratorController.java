
package controllers.administrator;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import services.AdministratorService;
import services.CompanyService;
import controllers.AbstractController;

@Controller
@RequestMapping("/company/administrator")
public class CompanyAdministratorController extends AbstractController {

	// Services

	@Autowired
	private CompanyService			companyService;

	@Autowired
	private AdministratorService	administratorService;


	// Constructors -----------------------------------------------------------

	public CompanyAdministratorController() {
		super();
	}

	// Methods ------------------------------------------------------------------

	@RequestMapping(value = "/banCompany", method = RequestMethod.GET)
	public ModelAndView banCompany(@RequestParam int companyID) {
		ModelAndView result;

		//Se comprueba que el actor logado es administrador
		boolean tienePermiso = administratorService.isAdminLogged();

		if (tienePermiso) {
			companyService.banCompany(companyID);

			result = new ModelAndView("redirect:/company/all.do");
		} else {
			result = new ModelAndView("misc/error");
			result.addObject("codigoError", "error.authorization");
		}

		return result;
	}

	@RequestMapping(value = "/unbanCompany", method = RequestMethod.GET)
	public ModelAndView unbanCompany(@RequestParam int companyID) {
		ModelAndView result;

		//Se comprueba que el actor logado es administrador
		boolean tienePermiso = administratorService.isAdminLogged();

		if (tienePermiso) {
			companyService.unbanCompany(companyID);

			result = new ModelAndView("redirect:/company/all.do");
		} else {
			result = new ModelAndView("misc/error");
			result.addObject("codigoError", "error.authorization");
		}

		return result;
	}

}
