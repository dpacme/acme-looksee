
package controllers.Candidate;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.Assert;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import controllers.AbstractController;
import domain.Curricula;
import domain.ProfessionalRecord;
import services.CandidateService;
import services.CurriculaService;
import services.ProfessionalRecordService;

@Controller
@RequestMapping("/professionalRecord/candidate")
public class ProfessionalRecordCandidateController extends AbstractController {

	// Services

	@Autowired
	private ProfessionalRecordService	professionalRecordService;

	@Autowired
	private CurriculaService			curriculaService;

	@Autowired
	private CandidateService			candidateService;


	// Constructors -----------------------------------------------------------

	public ProfessionalRecordCandidateController() {
		super();
	}

	// Methods ------------------------------------------------------------------

	// Edit
	@RequestMapping(value = "/edit", method = RequestMethod.GET)
	public ModelAndView edit(@RequestParam final int professionalRecordID) {
		ModelAndView res;

		try {
			final ProfessionalRecord professionalRecord = this.professionalRecordService.findOne(professionalRecordID);
			Assert.isTrue(professionalRecord.getCurricula().getCandidate().getId() == this.candidateService.findByPrincipal().getId(), "No estas autorizado");
			res = this.createModelAndView(professionalRecord);
		} catch (final Throwable oops) {
			res = new ModelAndView("misc/error");
			res.addObject("codigoError", "error.authorization");
		}

		return res;
	}

	@RequestMapping(value = "/edit", method = RequestMethod.POST, params = "save")
	public ModelAndView saveEdit(@Valid final ProfessionalRecord professionalRecord, final BindingResult binding) {
		ModelAndView res;

		if (binding.hasErrors()) {
			res = this.createModelAndView(professionalRecord);
			System.out.println(binding.getAllErrors());
		} else
			try {
				this.professionalRecordService.save(professionalRecord);
				res = new ModelAndView("redirect:/curricula/view.do?curriculaID=" + professionalRecord.getCurricula().getId());

			} catch (final Throwable oops) {
				System.out.println(oops);
				res = this.createModelAndView(professionalRecord);
				if (oops.getLocalizedMessage().contains("The end date must be after the start date"))
					res.addObject("date", "date");
			}

		return res;
	}

	//Create
	@RequestMapping(value = "/create", method = RequestMethod.GET)
	public ModelAndView create(@RequestParam final int curriculaID) {
		ModelAndView res;

		try {
			final Curricula curricula = this.curriculaService.findOne(curriculaID);
			Assert.isTrue(curricula.getCandidate().getId() == this.candidateService.findByPrincipal().getId(), "No estas autorizado");
			final ProfessionalRecord professionalRecord = this.professionalRecordService.create(curricula);
			curricula.getProfessionalRecords().add(professionalRecord);
			res = this.createModelAndViewCreate(professionalRecord);
		} catch (final Throwable oops) {
			res = new ModelAndView("misc/error");
			res.addObject("codigoError", "error.authorization");
		}

		return res;
	}

	@RequestMapping(value = "/create", method = RequestMethod.POST, params = "save")
	public ModelAndView saveCreate(@Valid final ProfessionalRecord professionalRecord, final BindingResult binding) {
		ModelAndView res;

		if (binding.hasErrors()) {
			res = this.createModelAndViewCreate(professionalRecord);
			System.out.println(binding.getAllErrors());
		} else
			try {
				this.professionalRecordService.save(professionalRecord);
				res = new ModelAndView("redirect:/curricula/view.do?curriculaID=" + professionalRecord.getCurricula().getId());

			} catch (final Throwable oops) {
				System.out.println(oops);
				res = this.createModelAndViewCreate(professionalRecord);
				if (oops.getLocalizedMessage().contains("The end date must be after the start date"))
					res.addObject("date", "date");
			}

		return res;
	}

	//Delete
	@RequestMapping(value = "/delete", method = RequestMethod.GET)
	public ModelAndView delete(@RequestParam final int professionalRecordID) {
		ModelAndView res;
		final ProfessionalRecord professionalRecord = this.professionalRecordService.findOne(professionalRecordID);

		try {
			Assert.isTrue(professionalRecord.getCurricula().getCandidate().getId() == this.candidateService.findByPrincipal().getId(), "No estas autorizado");
			this.professionalRecordService.delete(professionalRecord);
			res = new ModelAndView("redirect:/curricula/view.do?curriculaID=" + professionalRecord.getCurricula().getId());
		} catch (final Throwable oops) {
			res = new ModelAndView("misc/error");
			res.addObject("codigoError", "error.authorization");
		}

		return res;
	}

	// Creaci�n de ModelAndView para edit
	protected ModelAndView createModelAndView(final ProfessionalRecord professionalRecord) {
		ModelAndView res;

		res = this.createModelAndView(professionalRecord, null);

		return res;
	}

	protected ModelAndView createModelAndView(final ProfessionalRecord professionalRecord, final String message) {
		ModelAndView res;

		res = new ModelAndView("professionalRecord/candidate/edit");
		res.addObject("professionalRecord", professionalRecord);
		res.addObject("message", message);
		res.addObject("curriculaID", professionalRecord.getCurricula().getId());
		res.addObject("requestURI", "professionalRecord/candidate/edit.do");

		return res;
	}

	// Creaci�n de ModelAndView para create
	protected ModelAndView createModelAndViewCreate(final ProfessionalRecord professionalRecord) {
		ModelAndView res;

		res = this.createModelAndViewCreate(professionalRecord, null);

		return res;
	}

	protected ModelAndView createModelAndViewCreate(final ProfessionalRecord professionalRecord, final String message) {
		ModelAndView res;

		res = new ModelAndView("professionalRecord/candidate/create");
		res.addObject("professionalRecord", professionalRecord);
		res.addObject("message", message);
		res.addObject("curriculaID", professionalRecord.getCurricula().getId());
		res.addObject("requestURI", "professionalRecord/candidate/create.do");

		return res;
	}

}
