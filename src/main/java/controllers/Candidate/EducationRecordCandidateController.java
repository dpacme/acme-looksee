
package controllers.Candidate;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.Assert;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import controllers.AbstractController;
import domain.Curricula;
import domain.EducationRecord;
import services.CandidateService;
import services.CurriculaService;
import services.EducationRecordService;

@Controller
@RequestMapping("/educationRecord/candidate")
public class EducationRecordCandidateController extends AbstractController {

	// Services

	@Autowired
	private EducationRecordService	educationRecordService;

	@Autowired
	private CurriculaService		curriculaService;

	@Autowired
	private CandidateService		candidateService;


	// Constructors -----------------------------------------------------------

	public EducationRecordCandidateController() {
		super();
	}

	// Methods ------------------------------------------------------------------

	// Edit
	@RequestMapping(value = "/edit", method = RequestMethod.GET)
	public ModelAndView edit(@RequestParam final int educationRecordID) {
		ModelAndView res;

		try {
			final EducationRecord educationRecord = this.educationRecordService.findOne(educationRecordID);
			Assert.isTrue(educationRecord.getCurricula().getCandidate().getId() == this.candidateService.findByPrincipal().getId(), "No estas autorizado");
			res = this.createModelAndView(educationRecord);
		} catch (final Throwable oops) {
			res = new ModelAndView("misc/error");
			res.addObject("codigoError", "error.authorization");
		}

		return res;
	}

	@RequestMapping(value = "/edit", method = RequestMethod.POST, params = "save")
	public ModelAndView saveEdit(@Valid final EducationRecord educationRecord, final BindingResult binding) {
		ModelAndView res;

		if (binding.hasErrors()) {
			res = this.createModelAndView(educationRecord);
			System.out.println(binding.getAllErrors());
		} else
			try {
				this.educationRecordService.save(educationRecord);
				res = new ModelAndView("redirect:/curricula/view.do?curriculaID=" + educationRecord.getCurricula().getId());

			} catch (final Throwable oops) {
				System.out.println(oops);
				res = this.createModelAndView(educationRecord);
				if (oops.getLocalizedMessage().contains("The end date must be after the start date"))
					res.addObject("date", "date");
			}

		return res;
	}

	//Create
	@RequestMapping(value = "/create", method = RequestMethod.GET)
	public ModelAndView create(@RequestParam final int curriculaID) {
		ModelAndView res;
		try {
			final Curricula curricula = this.curriculaService.findOne(curriculaID);
			Assert.isTrue(curricula.getCandidate().getId() == this.candidateService.findByPrincipal().getId(), "No estas autorizado");
			final EducationRecord educationRecord = this.educationRecordService.create(curricula);
			curricula.getEducationRecords().add(educationRecord);
			res = this.createModelAndViewCreate(educationRecord);
		} catch (final Throwable oops) {
			res = new ModelAndView("misc/error");
			res.addObject("codigoError", "error.authorization");
		}

		return res;
	}

	@RequestMapping(value = "/create", method = RequestMethod.POST, params = "save")
	public ModelAndView saveCreate(@Valid final EducationRecord educationRecord, final BindingResult binding) {
		ModelAndView res;

		if (binding.hasErrors()) {
			res = this.createModelAndViewCreate(educationRecord);
			System.out.println(binding.getAllErrors());
		} else
			try {
				this.educationRecordService.save(educationRecord);
				res = new ModelAndView("redirect:/curricula/view.do?curriculaID=" + educationRecord.getCurricula().getId());

			} catch (final Throwable oops) {
				System.out.println(oops);
				res = this.createModelAndViewCreate(educationRecord);
				if (oops.getLocalizedMessage().contains("The end date must be after the start date"))
					res.addObject("date", "date");
			}

		return res;
	}

	//Delete
	@RequestMapping(value = "/delete", method = RequestMethod.GET)
	public ModelAndView delete(@RequestParam final int educationRecordID) {
		ModelAndView res;
		final EducationRecord educationRecord = this.educationRecordService.findOne(educationRecordID);

		try {
			Assert.isTrue(educationRecord.getCurricula().getCandidate().getId() == this.candidateService.findByPrincipal().getId(), "No estas autorizado");
			this.educationRecordService.delete(educationRecord);
			res = new ModelAndView("redirect:/curricula/view.do?curriculaID=" + educationRecord.getCurricula().getId());
		} catch (final Throwable oops) {
			res = new ModelAndView("misc/error");
			res.addObject("codigoError", "error.authorization");
		}

		return res;
	}

	// Creaci�n de ModelAndView para edit
	protected ModelAndView createModelAndView(final EducationRecord educationRecord) {
		ModelAndView res;

		res = this.createModelAndView(educationRecord, null);

		return res;
	}

	protected ModelAndView createModelAndView(final EducationRecord educationRecord, final String message) {
		ModelAndView res;

		res = new ModelAndView("educationRecord/candidate/edit");
		res.addObject("educationRecord", educationRecord);
		res.addObject("curriculaID", educationRecord.getCurricula().getId());
		res.addObject("message", message);
		res.addObject("requestURI", "educationRecord/candidate/edit.do");

		return res;
	}

	// Creaci�n de ModelAndView para create
	protected ModelAndView createModelAndViewCreate(final EducationRecord educationRecord) {
		ModelAndView res;

		res = this.createModelAndViewCreate(educationRecord, null);

		return res;
	}

	protected ModelAndView createModelAndViewCreate(final EducationRecord educationRecord, final String message) {
		ModelAndView res;

		res = new ModelAndView("educationRecord/candidate/create");
		res.addObject("educationRecord", educationRecord);
		res.addObject("message", message);
		res.addObject("curriculaID", educationRecord.getCurricula().getId());
		res.addObject("requestURI", "educationRecord/candidate/create.do");

		return res;
	}

}
