
package controllers.Candidate;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import controllers.AbstractController;
import domain.Curricula;
import domain.PersonalRecord;
import services.CandidateService;
import services.CurriculaService;
import services.PersonalRecordService;

@Controller
@RequestMapping("/personalRecord/candidate")
public class PersonalRecordCandidateController extends AbstractController {

	// Services

	@Autowired
	private PersonalRecordService	personalRecordService;

	@Autowired
	private CandidateService		candidateService;

	@Autowired
	private CurriculaService		curriculaService;


	// Constructors -----------------------------------------------------------

	public PersonalRecordCandidateController() {
		super();
	}

	// Methods ------------------------------------------------------------------

	// Edit
	@RequestMapping(value = "/edit", method = RequestMethod.GET)
	public ModelAndView edit() {
		ModelAndView res;
		final PersonalRecord personalRecord = this.personalRecordService.findOne(this.candidateService.findByPrincipal().getId());

		res = this.createModelAndView(personalRecord);
		return res;
	}

	@RequestMapping(value = "/edit", method = RequestMethod.POST, params = "save")
	public ModelAndView saveEdit(@Valid final PersonalRecord personalRecord, final BindingResult binding) {
		ModelAndView res;

		if (binding.hasErrors()) {
			res = this.createModelAndView(personalRecord);
			System.out.println(binding.getAllErrors());
		} else
			try {
				this.personalRecordService.save(personalRecord);
				res = new ModelAndView("redirect:/curricula/all.do");

			} catch (final Throwable oops) {
				System.out.println(oops);
				res = this.createModelAndView(personalRecord);
			}

		return res;
	}

	@RequestMapping(value = "/create", method = RequestMethod.GET)
	public ModelAndView create() {
		ModelAndView res;
		final Curricula curricula = this.curriculaService.create();
		res = this.createModelAndView(curricula);
		return res;
	}

	@RequestMapping(value = "/create", method = RequestMethod.POST, params = "save")
	public ModelAndView saveCreate(@Valid final PersonalRecord personalRecord, final BindingResult binding) {
		ModelAndView res;

		if (binding.hasErrors()) {
			res = this.createModelAndView2(personalRecord);
			System.out.println(binding.getAllErrors());
		} else
			try {
				final Curricula curricula = this.curriculaService.create();
				personalRecord.getCurriculas().add(curricula);
				final PersonalRecord aux = this.personalRecordService.save(personalRecord);
				curricula.setPersonalRecord(aux);
				this.curriculaService.save(curricula);
				res = new ModelAndView("redirect:/curricula/all.do");

			} catch (final Throwable oops) {
				System.out.println(oops);
				res = this.createModelAndView2(personalRecord);
			}

		return res;
	}

	// Creaci�n de ModelAndView para edit
	protected ModelAndView createModelAndView(final PersonalRecord personalRecord) {
		ModelAndView res;

		res = this.createModelAndView(personalRecord, null);

		return res;
	}

	protected ModelAndView createModelAndView(final PersonalRecord personalRecord, final String message) {
		ModelAndView res;

		res = new ModelAndView("personalRecord/candidate/edit");
		res.addObject("personalRecord", personalRecord);
		res.addObject("message", message);
		res.addObject("requestURI", "personalRecord/candidate/edit.do");

		return res;
	}

	// Creaci�n de ModelAndView para create
	protected ModelAndView createModelAndView(final Curricula curricula) {
		ModelAndView res;

		res = this.createModelAndView(curricula, null);

		return res;
	}

	protected ModelAndView createModelAndView(final Curricula curricula, final String message) {
		ModelAndView result;

		if (curricula.getPersonalRecord() != null) {
			this.curriculaService.save(curricula);
			result = new ModelAndView("redirect:/curricula/all.do");
		} else {
			result = new ModelAndView("personalRecord/candidate/create");
			final PersonalRecord personalRecord = this.personalRecordService.create();
			result.addObject("personalRecord", personalRecord);
			result.addObject("message", message);
			result.addObject("requestURI", "personalRecord/candidate/create.do");
		}

		return result;
	}

	protected ModelAndView createModelAndView2(final PersonalRecord personalRecord) {
		ModelAndView res;

		res = this.createModelAndView2(personalRecord, null);

		return res;
	}

	protected ModelAndView createModelAndView2(final PersonalRecord personalRecord, final String message) {
		ModelAndView res;

		res = new ModelAndView("personalRecord/candidate/create");
		res.addObject("personalRecord", personalRecord);
		res.addObject("message", message);
		res.addObject("requestURI", "personalRecord/candidate/create.do");

		return res;
	}

}
