
package controllers.Candidate;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import controllers.AbstractController;
import domain.Apply;
import domain.Offer;
import services.ApplyService;
import services.OfferService;

@Controller
@RequestMapping("/apply/candidate")
public class ApplyCandidateController extends AbstractController {

	// Services

	@Autowired
	private ApplyService	applyService;

	@Autowired
	private OfferService	offerService;


	// Constructors -----------------------------------------------------------

	public ApplyCandidateController() {
		super();
	}

	// Methods ------------------------------------------------------------------

	@RequestMapping(value = "/list", method = RequestMethod.GET)
	public ModelAndView list() {
		ModelAndView result;

		Collection<Apply> applies = applyService.findApplyFromCandidate();
		result = new ModelAndView("apply/all");
		result.addObject("requestURI", "apply/candidate/list.do");
		result.addObject("applies", applies);

		return result;
	}

	@RequestMapping(value = "/applyOffer", method = RequestMethod.POST)
	public ModelAndView applyOffer(@RequestParam Integer offerId, @RequestParam Integer curriculaId) {
		ModelAndView result;

		Offer offer = applyService.registerApplicationOffer(offerId, curriculaId);

		result = new ModelAndView("redirect:/offer/showDisplay.do?offerId=" + offer.getId());

		return result;
	}
}
