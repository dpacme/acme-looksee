
package controllers.Candidate;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.Assert;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import controllers.AbstractController;
import domain.Curricula;
import domain.EndorserRecord;
import services.CandidateService;
import services.CurriculaService;
import services.EndorserRecordService;

@Controller
@RequestMapping("/endorserRecord/candidate")
public class EndorserRecordCandidateController extends AbstractController {

	// Services

	@Autowired
	private EndorserRecordService	endorserRecordService;

	@Autowired
	private CurriculaService		curriculaService;

	@Autowired
	private CandidateService		candidateService;


	// Constructors -----------------------------------------------------------

	public EndorserRecordCandidateController() {
		super();
	}

	// Methods ------------------------------------------------------------------

	// Edit
	@RequestMapping(value = "/edit", method = RequestMethod.GET)
	public ModelAndView edit(@RequestParam final int endorserRecordID) {
		ModelAndView res;

		try {
			final EndorserRecord endorserRecord = this.endorserRecordService.findOne(endorserRecordID);
			Assert.isTrue(endorserRecord.getCurricula().getCandidate().getId() == this.candidateService.findByPrincipal().getId(), "No estas autorizado");
			res = this.createModelAndView(endorserRecord);
		} catch (final Throwable oops) {
			res = new ModelAndView("misc/error");
			res.addObject("codigoError", "error.authorization");
		}

		return res;
	}

	@RequestMapping(value = "/edit", method = RequestMethod.POST, params = "save")
	public ModelAndView saveEdit(@Valid final EndorserRecord endorserRecord, final BindingResult binding) {
		ModelAndView res;

		if (binding.hasErrors()) {
			res = this.createModelAndView(endorserRecord);
			System.out.println(binding.getAllErrors());
		} else
			try {
				this.endorserRecordService.save(endorserRecord);
				res = new ModelAndView("redirect:/curricula/view.do?curriculaID=" + endorserRecord.getCurricula().getId());

			} catch (final Throwable oops) {
				System.out.println(oops);
				res = this.createModelAndView(endorserRecord);
			}

		return res;
	}

	//Create
	@RequestMapping(value = "/create", method = RequestMethod.GET)
	public ModelAndView create(@RequestParam final int curriculaID) {
		ModelAndView res;

		try {
			final Curricula curricula = this.curriculaService.findOne(curriculaID);
			Assert.isTrue(curricula.getCandidate().getId() == this.candidateService.findByPrincipal().getId(), "No estas autorizado");
			final EndorserRecord endorserRecord = this.endorserRecordService.create(curricula);
			curricula.getEndorserRecords().add(endorserRecord);
			res = this.createModelAndViewCreate(endorserRecord);
		} catch (final Throwable oops) {
			res = new ModelAndView("misc/error");
			res.addObject("codigoError", "error.authorization");
		}

		return res;
	}

	@RequestMapping(value = "/create", method = RequestMethod.POST, params = "save")
	public ModelAndView saveCreate(@Valid final EndorserRecord endorserRecord, final BindingResult binding) {
		ModelAndView res;

		if (binding.hasErrors()) {
			res = this.createModelAndViewCreate(endorserRecord);
			System.out.println(binding.getAllErrors());
		} else
			try {
				this.endorserRecordService.save(endorserRecord);
				res = new ModelAndView("redirect:/curricula/view.do?curriculaID=" + endorserRecord.getCurricula().getId());

			} catch (final Throwable oops) {
				System.out.println(oops);
				res = this.createModelAndViewCreate(endorserRecord);
			}

		return res;
	}

	//Delete
	@RequestMapping(value = "/delete", method = RequestMethod.GET)
	public ModelAndView delete(@RequestParam final int endorserRecordID) {
		ModelAndView res;
		final EndorserRecord endorserRecord = this.endorserRecordService.findOne(endorserRecordID);

		try {
			Assert.isTrue(endorserRecord.getCurricula().getCandidate().getId() == this.candidateService.findByPrincipal().getId(), "No estas autorizado");
			this.endorserRecordService.delete(endorserRecord);
			res = new ModelAndView("redirect:/curricula/view.do?curriculaID=" + endorserRecord.getCurricula().getId());
		} catch (final Throwable oops) {
			res = new ModelAndView("misc/error");
			res.addObject("codigoError", "error.authorization");
		}

		return res;
	}

	// Creaci�n de ModelAndView para edit
	protected ModelAndView createModelAndView(final EndorserRecord endorserRecord) {
		ModelAndView res;

		res = this.createModelAndView(endorserRecord, null);

		return res;
	}

	protected ModelAndView createModelAndView(final EndorserRecord endorserRecord, final String message) {
		ModelAndView res;

		res = new ModelAndView("endorserRecord/candidate/edit");
		res.addObject("endorserRecord", endorserRecord);
		res.addObject("curriculaID", endorserRecord.getCurricula().getId());
		res.addObject("message", message);
		res.addObject("requestURI", "endorserRecord/candidate/edit.do");

		return res;
	}

	// Creaci�n de ModelAndView para create
	protected ModelAndView createModelAndViewCreate(final EndorserRecord endorserRecord) {
		ModelAndView res;

		res = this.createModelAndViewCreate(endorserRecord, null);

		return res;
	}

	protected ModelAndView createModelAndViewCreate(final EndorserRecord endorserRecord, final String message) {
		ModelAndView res;

		res = new ModelAndView("endorserRecord/candidate/create");
		res.addObject("endorserRecord", endorserRecord);
		res.addObject("message", message);
		res.addObject("curriculaID", endorserRecord.getCurricula().getId());
		res.addObject("requestURI", "endorserRecord/candidate/create.do");

		return res;
	}

}
