
package controllers.Candidate;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.Assert;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import controllers.AbstractController;
import domain.Curricula;
import domain.MiscellaneousRecord;
import services.CandidateService;
import services.CurriculaService;
import services.MiscellaneousRecordService;

@Controller
@RequestMapping("/miscellaneousRecord/candidate")
public class MiscellaneousRecordCandidateController extends AbstractController {

	// Services

	@Autowired
	private MiscellaneousRecordService	miscellaneousRecordService;

	@Autowired
	private CurriculaService			curriculaService;

	@Autowired
	private CandidateService			candidateService;


	// Constructors -----------------------------------------------------------

	public MiscellaneousRecordCandidateController() {
		super();
	}

	// Methods ------------------------------------------------------------------

	// Edit
	@RequestMapping(value = "/edit", method = RequestMethod.GET)
	public ModelAndView edit(@RequestParam final int miscellaneousRecordID) {
		ModelAndView res;
		try {
			final MiscellaneousRecord miscellaneousRecord = this.miscellaneousRecordService.findOne(miscellaneousRecordID);
			Assert.isTrue(miscellaneousRecord.getCurricula().getCandidate().getId() == this.candidateService.findByPrincipal().getId(), "No estas autorizado");
			res = this.createModelAndView(miscellaneousRecord);
		} catch (final Throwable oops) {
			res = new ModelAndView("misc/error");
			res.addObject("codigoError", "error.authorization");
		}

		return res;
	}

	@RequestMapping(value = "/edit", method = RequestMethod.POST, params = "save")
	public ModelAndView saveEdit(@Valid final MiscellaneousRecord miscellaneousRecord, final BindingResult binding) {
		ModelAndView res;

		if (binding.hasErrors()) {
			res = this.createModelAndView(miscellaneousRecord);
			System.out.println(binding.getAllErrors());
		} else
			try {
				this.miscellaneousRecordService.save(miscellaneousRecord);
				res = new ModelAndView("redirect:/curricula/view.do?curriculaID=" + miscellaneousRecord.getCurricula().getId());

			} catch (final Throwable oops) {
				System.out.println(oops);
				res = this.createModelAndView(miscellaneousRecord);
			}

		return res;
	}

	//Create
	@RequestMapping(value = "/create", method = RequestMethod.GET)
	public ModelAndView create(@RequestParam final int curriculaID) {
		ModelAndView res;
		try {
			final Curricula curricula = this.curriculaService.findOne(curriculaID);
			Assert.isTrue(curricula.getCandidate().getId() == this.candidateService.findByPrincipal().getId(), "No estas autorizado");
			final MiscellaneousRecord miscellaneousRecord = this.miscellaneousRecordService.create(curricula);
			curricula.getMiscellaneousRecords().add(miscellaneousRecord);
			res = this.createModelAndViewCreate(miscellaneousRecord);
		} catch (final Throwable oops) {
			res = new ModelAndView("misc/error");
			res.addObject("codigoError", "error.authorization");
		}

		return res;
	}

	@RequestMapping(value = "/create", method = RequestMethod.POST, params = "save")
	public ModelAndView saveCreate(@Valid final MiscellaneousRecord miscellaneousRecord, final BindingResult binding) {
		ModelAndView res;

		if (binding.hasErrors()) {
			res = this.createModelAndViewCreate(miscellaneousRecord);
			System.out.println(binding.getAllErrors());
		} else
			try {
				this.miscellaneousRecordService.save(miscellaneousRecord);
				res = new ModelAndView("redirect:/curricula/view.do?curriculaID=" + miscellaneousRecord.getCurricula().getId());

			} catch (final Throwable oops) {
				System.out.println(oops);
				res = this.createModelAndViewCreate(miscellaneousRecord);
			}

		return res;
	}

	//Delete
	@RequestMapping(value = "/delete", method = RequestMethod.GET)
	public ModelAndView delete(@RequestParam final int miscellaneousRecordID) {
		ModelAndView res;
		final MiscellaneousRecord miscellaneousRecord = this.miscellaneousRecordService.findOne(miscellaneousRecordID);

		try {
			Assert.isTrue(miscellaneousRecord.getCurricula().getCandidate().getId() == this.candidateService.findByPrincipal().getId(), "No estas autorizado");
			this.miscellaneousRecordService.delete(miscellaneousRecord);
			res = new ModelAndView("redirect:/curricula/view.do?curriculaID=" + miscellaneousRecord.getCurricula().getId());
		} catch (final Throwable oops) {
			res = new ModelAndView("misc/error");
			res.addObject("codigoError", "error.authorization");
		}

		return res;
	}

	// Creaci�n de ModelAndView para edit
	protected ModelAndView createModelAndView(final MiscellaneousRecord miscellaneousRecord) {
		ModelAndView res;

		res = this.createModelAndView(miscellaneousRecord, null);

		return res;
	}

	protected ModelAndView createModelAndView(final MiscellaneousRecord miscellaneousRecord, final String message) {
		ModelAndView res;

		res = new ModelAndView("miscellaneousRecord/candidate/edit");
		res.addObject("miscellaneousRecord", miscellaneousRecord);
		res.addObject("curriculaID", miscellaneousRecord.getCurricula().getId());
		res.addObject("message", message);

		return res;
	}

	// Creaci�n de ModelAndView para create
	protected ModelAndView createModelAndViewCreate(final MiscellaneousRecord miscellaneousRecord) {
		ModelAndView res;

		res = this.createModelAndViewCreate(miscellaneousRecord, null);

		return res;
	}

	protected ModelAndView createModelAndViewCreate(final MiscellaneousRecord miscellaneousRecord, final String message) {
		ModelAndView res;

		res = new ModelAndView("miscellaneousRecord/candidate/create");
		res.addObject("miscellaneousRecord", miscellaneousRecord);
		res.addObject("message", message);
		res.addObject("curriculaID", miscellaneousRecord.getCurricula().getId());
		res.addObject("requestURI", "miscellaneousRecord/candidate/create.do");

		return res;
	}

}
