
package controllers.Candidate;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import services.CandidateService;
import controllers.AbstractController;
import domain.Candidate;
import forms.ActorForm;

@Controller
@RequestMapping("/candidate")
public class CandidateController extends AbstractController {

	// Services

	@Autowired
	private CandidateService	candidateService;


	// Constructors -----------------------------------------------------------

	public CandidateController() {
		super();
	}

	// Methods ------------------------------------------------------------------

	// (REGISTRO) Creaci�n de un candidato
	@RequestMapping(value = "/create", method = RequestMethod.GET)
	public ModelAndView create() {
		ModelAndView res;

		final ActorForm actorForm = new ActorForm();

		res = this.createFormModelAndView(actorForm);
		return res;
	}

	// (REGISTRO) Guardar en la base de datos el nuevo candidate
	@RequestMapping(value = "/create", method = RequestMethod.POST, params = "save")
	public ModelAndView save(@Valid final ActorForm actorForm, final BindingResult binding) {
		ModelAndView res;
		Candidate candidate;

		if (binding.hasErrors()) {
			res = this.createFormModelAndView(actorForm);
			//Comprobar codigo postal correcto
			if (!StringUtils.isEmpty(actorForm.getPostalAddress()) && !actorForm.getPostalAddress().matches("^(\\d{5}$)")) {
				res.addObject("postal", "postal");
			}
			System.out.println(binding.getAllErrors());
		} else
			try {

				candidate = this.candidateService.reconstruct(actorForm);

				this.candidateService.saveForm(candidate);
				res = new ModelAndView("redirect:/security/login.do");

			} catch (final Throwable oops) {
				res = this.createFormModelAndView(actorForm);
				System.out.println(oops.getLocalizedMessage());
				if (oops.getLocalizedMessage().contains("ConstraintViolationException"))
					res.addObject("duplicate", "duplicate");
				if (oops.getLocalizedMessage().contains("You must accept the term and conditions"))
					res.addObject("terms", "terms");
				if (oops.getLocalizedMessage().contains("Passwords do not match"))
					res.addObject("pass", "pass");
				if (oops.getLocalizedMessage().contains("The format of the postaladdress is incorrect"))
					res.addObject("postal", "postal");
			}

		return res;
	}

	// (MODIFICAR DATOS) Modificar datos
	@RequestMapping(value = "/edit", method = RequestMethod.GET)
	public ModelAndView edit() {
		ModelAndView res;
		final Candidate candidate = this.candidateService.findByPrincipal();

		res = this.createFormModelAndView(candidate);
		return res;
	}

	// (MODIFICAR DATOS) Guardar en la base de datos el nuevo candidate
	@RequestMapping(value = "/edit", method = RequestMethod.POST, params = "save")
	public ModelAndView saveEdit(@Valid final Candidate candidate, final BindingResult binding) {
		ModelAndView res;

		if (binding.hasErrors()) {
			res = this.createFormModelAndView(candidate);
			System.out.println(binding.getAllErrors());
		} else
			try {
				this.candidateService.comprobacion2(candidate);
				this.candidateService.save(candidate);
				res = new ModelAndView("redirect:/");

			} catch (final Throwable oops) {
				System.out.println(oops);
				res = this.createFormModelAndView(candidate);
				if (oops.getLocalizedMessage().contains("The format of the postaladdress is incorrect"))
					res.addObject("postal", "postal");
			}

		return res;
	}

	// Creaci�n de ModelAndView para formulario
	protected ModelAndView createFormModelAndView(final ActorForm actorForm) {
		ModelAndView res;

		res = this.createFormModelAndView(actorForm, null);

		return res;
	}

	protected ModelAndView createFormModelAndView(final ActorForm actorForm, final String message) {
		ModelAndView res;

		res = new ModelAndView("candidate/create");
		res.addObject("actorForm", actorForm);
		res.addObject("message", message);

		return res;
	}

	// Creaci�n de ModelAndView para edit
	protected ModelAndView createFormModelAndView(final Candidate candidate) {
		ModelAndView res;

		res = this.createFormModelAndView(candidate, null);

		return res;
	}

	protected ModelAndView createFormModelAndView(final Candidate candidate, final String message) {
		ModelAndView res;

		res = new ModelAndView("candidate/edit");
		res.addObject("candidate", candidate);
		res.addObject("message", message);

		return res;
	}
}
