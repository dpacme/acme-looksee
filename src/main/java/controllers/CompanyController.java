
package controllers;

import java.util.ArrayList;
import java.util.Collection;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import domain.Administrator;
import domain.Company;
import forms.CompanyForm;
import security.UserAccountService;
import services.AdministratorService;
import services.CompanyService;

@Controller
@RequestMapping("/company")
public class CompanyController extends AbstractController {

	// Services

	@Autowired
	private CompanyService			companyService;

	@Autowired
	private AdministratorService	administratorService;

	@Autowired
	private UserAccountService		userAccountService;


	// Constructors -----------------------------------------------------------

	public CompanyController() {
		super();
	}

	// Methods ------------------------------------------------------------------

	// (REGISTRO) Creaci�n de un candidato
	@RequestMapping(value = "/create", method = RequestMethod.GET)
	public ModelAndView create() {
		ModelAndView res;

		final CompanyForm companyForm = new CompanyForm();

		res = this.createFormModelAndView(companyForm);
		return res;
	}

	// (REGISTRO) Guardar en la base de datos el nuevo company
	@RequestMapping(value = "/create", method = RequestMethod.POST, params = "save")
	public ModelAndView save(@Valid final CompanyForm companyForm, final BindingResult binding) {
		ModelAndView res;
		Company company;

		if (binding.hasErrors()) {
			res = this.createFormModelAndView(companyForm);
			System.out.println(binding.getAllErrors());
		} else {
			try {

				company = companyService.reconstruct(companyForm);

				companyService.saveForm(company);
				res = new ModelAndView("redirect:/security/login.do");

			} catch (final Throwable oops) {
				res = this.createFormModelAndView(companyForm);
				System.out.println(oops.getLocalizedMessage());
				if (oops.getLocalizedMessage().contains("ConstraintViolationException")) {
					if (userAccountService.findByUsername(companyForm.getUsername()) != null) {
						res.addObject("duplicate", "duplicate");
					} else {
						res.addObject("duplicateVatNumber", "duplicateVatNumber");
					}
				}
				if (oops.getLocalizedMessage().contains("You must accept the term and conditions")) {
					res.addObject("terms", "terms");
				}
				if (oops.getLocalizedMessage().contains("Passwords do not match")) {
					res.addObject("pass", "pass");
				}
				if (oops.getLocalizedMessage().contains("The format of the postaladdress is incorrect")) {
					res.addObject("postal", "postal");
				}
			}
		}

		return res;
	}

	@RequestMapping(value = "/all", method = RequestMethod.GET)
	public ModelAndView all() {
		ModelAndView result;
		Collection<Company> res = new ArrayList<Company>();

		/*
		 * Si accede un administrador se mostrar�n todas las compa��as,
		 * inclu�das las baneadas
		 */

		if (administratorService.isAdminLogged()) {
			res = companyService.findAll();
		} else {
			//Si est� logueado un administrador, se muestran las compa��as no baneadas
			res = companyService.findValidCompanies();
		}

		result = new ModelAndView("company/all");
		result.addObject("companies", res);
		result.addObject("requestURI", "company/all.do");

		return result;
	}

	// (MODIFICAR DATOS) Modificar datos
	@RequestMapping(value = "/edit", method = RequestMethod.GET)
	public ModelAndView edit() {
		ModelAndView res;
		final Company company = companyService.findByPrincipal();

		res = this.createFormModelAndView(company);
		return res;
	}

	// (MODIFICAR DATOS) Guardar en la base de datos el nuevo company
	@RequestMapping(value = "/edit", method = RequestMethod.POST, params = "save")
	public ModelAndView saveEdit(@Valid final Company company, final BindingResult binding) {
		ModelAndView res;

		if (binding.hasErrors()) {
			res = this.createFormModelAndView(company);
			System.out.println(binding.getAllErrors());
		} else {
			try {
				companyService.comprobacion2(company);
				companyService.save(company);
				res = new ModelAndView("redirect:/");

			} catch (final Throwable oops) {
				System.out.println(oops);
				res = this.createFormModelAndView(company);
				if (oops.getLocalizedMessage().contains("The format of the postaladdress is incorrect")) {
					res.addObject("postal", "postal");
				}
				if (oops.getLocalizedMessage().contains("ConstraintViolationException")) {
					res.addObject("duplicateVatNumber", "duplicateVatNumber");
				}
			}
		}

		return res;
	}

	@RequestMapping(value = "/showDisplay", method = RequestMethod.GET)
	public ModelAndView showDisplay(@RequestParam int companyId) {
		ModelAndView result;
		boolean tienePermiso = true;
		Company company = companyService.create();
		Administrator administrator = administratorService.create();
		company = companyService.findOne(companyId);
		try {
			administrator = administratorService.findByPrincipal();
		} catch (Exception e) {
			administrator = null;
		}




		if (company.getBanned()) {
			if (administrator == null) {
				tienePermiso = false;
			}
		}

		if (tienePermiso) {
			result = new ModelAndView("company/showDisplay");
			result.addObject("requestURI", "company/showDisplay.do");
			result.addObject("company", company);
		} else {
			result = new ModelAndView("misc/error");
			result.addObject("codigoError", "error.authorization");
		}

		return result;

	}

	// Creaci�n de ModelAndView para formulario
	protected ModelAndView createFormModelAndView(final CompanyForm companyForm) {
		ModelAndView res;

		res = this.createFormModelAndView(companyForm, null);

		return res;
	}

	protected ModelAndView createFormModelAndView(final CompanyForm companyForm, final String message) {
		ModelAndView res;

		res = new ModelAndView("company/create");
		res.addObject("companyForm", companyForm);
		res.addObject("message", message);

		return res;
	}

	// Creaci�n de ModelAndView para edit
	protected ModelAndView createFormModelAndView(final Company company) {
		ModelAndView res;

		res = this.createFormModelAndView(company, null);

		return res;
	}

	protected ModelAndView createFormModelAndView(final Company company, final String message) {
		ModelAndView res;

		res = new ModelAndView("company/edit");
		res.addObject("company", company);
		res.addObject("message", message);

		return res;
	}
}
