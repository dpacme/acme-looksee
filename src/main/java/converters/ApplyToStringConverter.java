
package converters;

import javax.transaction.Transactional;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import domain.Apply;

@Component
@Transactional
public class ApplyToStringConverter implements Converter<Apply, String> {

	@Override
	public String convert(final Apply apply) {
		String res;

		if (apply == null)
			res = null;
		else
			res = String.valueOf(apply.getId());

		return res;

	}
}
