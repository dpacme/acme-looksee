
package converters;

import javax.transaction.Transactional;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import domain.Candidate;

@Component
@Transactional
public class CandidateToStringConverter implements Converter<Candidate, String> {

	@Override
	public String convert(final Candidate candidate) {
		String res;

		if (candidate == null)
			res = null;
		else
			res = String.valueOf(candidate.getId());

		return res;

	}
}
