
package converters;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import domain.Candidate;
import repositories.CandidateRepository;

@Component
@Transactional
public class StringToCandidateConverter implements Converter<String, Candidate> {

	@Autowired
	CandidateRepository candidateRepository;


	@Override
	public Candidate convert(final String text) {
		Candidate res;
		int id;
		try {
			if (StringUtils.isEmpty(text))
				res = null;
			else {
				id = Integer.valueOf(text);
				res = this.candidateRepository.findOne(id);
			}
		} catch (final Throwable oops) {
			throw new IllegalArgumentException(oops);
		}
		return res;
	}
}
