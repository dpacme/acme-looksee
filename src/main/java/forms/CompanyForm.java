
package forms;

import javax.persistence.Column;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.NotEmpty;
import org.hibernate.validator.constraints.SafeHtml;
import org.hibernate.validator.constraints.SafeHtml.WhiteListType;

public class CompanyForm {

	// Constructor

	public CompanyForm() {
		super();
	}


	// Atributos

	private String	name;
	private String	surname;
	private String	email;
	private String	phone;
	private String	postalAddress;

	private String	secondPassword;
	private Boolean	checkBox;
	private String	username;
	private String	password;

	// Atributos de company
	private String	nameCompany;
	private String	vatNumber;


	// Gets y Sets
	@SafeHtml(whitelistType = WhiteListType.NONE)
	@Size(min = 5, max = 32)
	@Column(unique = true)
	public String getUsername() {
		return username;
	}

	public void setUsername(final String username) {
		this.username = username;
	}

	@SafeHtml(whitelistType = WhiteListType.NONE)
	@Size(min = 5, max = 32)
	public String getPassword() {
		return password;
	}

	public void setPassword(final String password) {
		this.password = password;
	}

	@SafeHtml(whitelistType = WhiteListType.NONE)
	@Size(min = 5, max = 32)
	public String getSecondPassword() {
		return secondPassword;
	}

	public void setSecondPassword(final String secondPassword) {
		this.secondPassword = secondPassword;
	}

	public Boolean getCheckBox() {
		return checkBox;
	}

	public void setCheckBox(final Boolean checkBox) {
		this.checkBox = checkBox;
	}

	@SafeHtml(whitelistType = WhiteListType.NONE)
	@NotEmpty
	public String getName() {
		return name;
	}

	public void setName(final String name) {
		this.name = name;
	}

	@SafeHtml(whitelistType = WhiteListType.NONE)
	@NotEmpty
	public String getSurname() {
		return surname;
	}

	public void setSurname(final String surname) {
		this.surname = surname;
	}

	@SafeHtml(whitelistType = WhiteListType.NONE)
	@Email
	@NotEmpty
	public String getEmail() {
		return email;
	}

	public void setEmail(final String email) {
		this.email = email;
	}

	@SafeHtml(whitelistType = WhiteListType.NONE)
	public String getPhone() {
		return phone;
	}

	public void setPhone(final String phone) {
		this.phone = phone;
	}

	@SafeHtml(whitelistType = WhiteListType.NONE)
	public String getPostalAddress() {
		return postalAddress;
	}

	public void setPostalAddress(final String postalAddess) {
		postalAddress = postalAddess;
	}

	@SafeHtml(whitelistType = WhiteListType.NONE)
	@NotEmpty
	public String getNameCompany() {
		return nameCompany;
	}

	public void setNameCompany(String nameCompany) {
		this.nameCompany = nameCompany;
	}

	@SafeHtml(whitelistType = WhiteListType.NONE)
	@Pattern(regexp = "(?xi)^(\r\n" + "(AT)?U[0-9]{8} |                              # Austria\r\n" + "(BE)?0[0-9]{9} |                              # Belgium\r\n" + "(BG)?[0-9]{9,10} |                            # Bulgaria\r\n"
		+ "(HR)?[0-9]{11} |                              # Croatia\r\n" + "(CY)?[0-9]{8}L |                              # Cyprus\r\n" + "(CZ)?[0-9]{8,10} |                            # Czech Republic\r\n"
		+ "(DE)?[0-9]{9} |                               # Germany\r\n" + "(DK)?[0-9]{8} |                               # Denmark\r\n" + "(EE)?[0-9]{9} |                               # Estonia\r\n"
		+ "(EL|GR)?[0-9]{9} |                            # Greece\r\n" + "ES[A-Z][0-9]{7}(?:[0-9]|[A-Z]) |              # Spain\r\n" + "(FI)?[0-9]{8} |                               # Finland\r\n"
		+ "(FR)?[0-9A-Z]{2}[0-9]{9} |                    # France\r\n" + "(GB)?([0-9]{9}([0-9]{3})?|[A-Z]{2}[0-9]{3}) | # United Kingdom\r\n" + "(HU)?[0-9]{8} |                               # Hungary\r\n"
		+ "(IE)?[0-9]S[0-9]{5}L |                        # Ireland\r\n" + "(IT)?[0-9]{11} |                              # Italy\r\n" + "(LT)?([0-9]{9}|[0-9]{12}) |                   # Lithuania\r\n"
		+ "(LU)?[0-9]{8} |                               # Luxembourg\r\n" + "(LV)?[0-9]{11} |                              # Latvia\r\n" + "(MT)?[0-9]{8} |                               # Malta\r\n"
		+ "(NL)?[0-9]{9}B[0-9]{2} |                      # Netherlands\r\n" + "(PL)?[0-9]{10} |                              # Poland\r\n" + "(PT)?[0-9]{9} |                               # Portugal\r\n"
		+ "(RO)?[0-9]{2,10} |                            # Romania\r\n" + "(SE)?[0-9]{12} |                              # Sweden\r\n" + "(SI)?[0-9]{8} |                               # Slovenia\r\n"
		+ "(SK)?[0-9]{10}                                # Slovakia\r\n" + ")$")
	@NotEmpty
	@Column(unique = true)
	public String getVatNumber() {
		return vatNumber;
	}

	public void setVatNumber(String vatNumber) {
		this.vatNumber = vatNumber;
	}

}
