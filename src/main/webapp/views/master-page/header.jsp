<%--
 * header.jsp
 *
 * Copyright (C) 2017 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the 
 * TDG Licence, a copy of which you may download from 
 * http://www.tdg-seville.info/License.html
 --%>

<%@page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>

<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="security" uri="http://www.springframework.org/security/tags"%>

<div>
	<img src="images/logo.png" alt="Acme-LookSee Co., Inc." />
</div>

<div>
	<ul id="jMenu">
		<!-- Do not forget the "fNiv" class for the first level links !! -->
		
		<li>
			<a class="fNiv"> 
				<spring:message code="master.page.offers" />
			</a>
			<ul>
				<li class="arrow"></li>				
				<li><a href="offer/all.do"><spring:message code="master.page.allOffers" /></a></li>
				<security:authorize access="hasRole('COMPANY')">
					<li><a href="company/offer/create.do"><spring:message code="master.page.createOffer" /></a></li>
					<li><a href="company/offer/myOffers.do"><spring:message code="master.page.myOffers" /></a></li>
					<li><a href="company/offer/drafts.do"><spring:message code="master.page.drafts" /></a></li>
				</security:authorize>
			</ul>
		</li>
		
		<li>
			<a class="fNiv"> 
				<spring:message code="master.page.companies" />
			</a>
			<ul>
				<li class="arrow"></li>				
				<li><a href="company/all.do"><spring:message code="master.page.allCompanies" /></a></li>
			</ul>
		</li>
		
		<security:authorize access="isAnonymous()">
			<li><a class="fNiv" href="security/login.do"><spring:message code="master.page.login" /></a></li>			
			<li><a class="fNiv" href="candidate/create.do"><spring:message code="master.page.registerAsCandidate" /></a></li>
			<li><a class="fNiv" href="company/create.do"><spring:message code="master.page.registerAsCompany" /></a></li>
		</security:authorize>
		
		
		<security:authorize access="hasRole('CANDIDATE')">
		
			<li><a class="fNiv" href="candidate/edit.do"><spring:message code="master.page.modifyProfile" /></a></li>
			<li>
				<a class="fNiv"> 
					<spring:message code="master.page.curricula" />
				</a>
				<ul>
					<li class="arrow"></li>				
					<li><a href="curricula/all.do"><spring:message code="master.page.viewCurriculas" /></a></li>
				</ul>
			</li>
			
			<li><a class="fNiv" href="apply/candidate/list.do"><spring:message code="master.page.applicationCandidate" /></a></li>
		
		</security:authorize>
		
		<security:authorize access="hasRole('COMPANY')">
		
			<li><a class="fNiv" href="company/edit.do"><spring:message code="master.page.modifyProfile" /></a></li>
		
		</security:authorize>
		
		<security:authorize access="hasRole('ADMINISTRATOR')">
		
			<li><a class="fNiv" href="administrator/edit.do"><spring:message code="master.page.modifyProfile" /></a></li>
			<li><a class="fNiv" href="administrator/dashboard.do"><spring:message code="master.page.dashboard" /></a></li>

		</security:authorize>
		
		<security:authorize access="isAuthenticated()">
		
			<li>
				<a class="fNiv"> 
					<spring:message code="master.page.profile" /> 
			        (<security:authentication property="principal.username" />)
				</a>
				<ul>
					<li class="arrow"></li>				
					<li><a href="j_spring_security_logout"><spring:message code="master.page.logout" /> </a></li>
				</ul>
			</li>
		</security:authorize>
	</ul>
</div>

<div>
	<a href="?language=en">en</a> | <a href="?language=es">es</a>
</div>

