<%@page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<%@taglib prefix="security"
	uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="acme" tagdir="/WEB-INF/tags"%>

<div class="personalRecord" style="text-align: center">
	<b><spring:message code="curricula.personal.fullName" />:</b> ${personalRecord.fullName}<br>
	<p />
	<b><spring:message code="curricula.personal.email" />:</b> ${personalRecord.email}<br>
	<p />
	<b><spring:message code="curricula.personal.phone" />:</b> ${personalRecord.phone}<br>
	<p />
	<b><spring:message code="curricula.personal.linkedInUrl" />:</b> <a
		href="${personalRecord.linkedInUrl}"> ${personalRecord.linkedInUrl}</a><br>
	<p />
	<img src="${personalRecord.picture}" style="width: 250px; height: 250px;"><br>
	<p />
	<acme:button href="personalRecord/candidate/edit.do"
				name="edit" code="curricula.edit" />
</div>

<br/>
<b><spring:message code="curricula.educationRecords" /></b>
<p />

<display:table name="educationRecords" id="row" requestURI="${requestURI}"
	pagesize="5" class="displaytag">


	<spring:message code="curricula.educationRecord.title" var="titleHeader" />
	<display:column title="${titleHeader}" property="title" sortable="true"/>
	
	<spring:message code="curricula.educationRecord.startDate" var="startDateHeader" />
	<display:column title="${startDateHeader}" property="startDate" format="{0,date,dd/MM/YYYY}" sortable="true"/>
		
	<spring:message code="curricula.educationRecord.endDate" var="endDateHeader" />
	<display:column title="${endDateHeader}" property="endDate" format="{0,date,dd/MM/YYYY}" sortable="true"/>
	
	<spring:message code="curricula.educationRecord.institution" var="institutionHeader" />
	<display:column title="${institutionHeader}" property="institution" sortable="true"/>
	
	<spring:message code="curricula.educationRecord.attachment" var="attachmentHeader" />
	<display:column title="${attachmentHeader}" property="attachment"/>
	
	<spring:message code="curricula.educationRecord.comments" var="commentsHeader" />
	<display:column title="${commentsHeader}" property="comments"/>
	
	<display:column>
			<acme:button href="educationRecord/candidate/edit.do?educationRecordID=${row.id}"
				name="edit" code="curricula.edit" />
	</display:column>
	
	<display:column>
			<acme:button href="educationRecord/candidate/delete.do?educationRecordID=${row.id}"
				name="edit" code="curricula.delete" />
	</display:column>

</display:table>

<br/>
<br/>
<a href="educationRecord/candidate/create.do?curriculaID=${curricula.id}"><spring:message code="record.create"/></a>

<br/>
<br/>
<b><spring:message code="curricula.professionalRecords" /></b>
<p />

<display:table name="professionalRecords" id="row" requestURI="${requestURI}"
	pagesize="5" class="displaytag">


	<spring:message code="curricula.professionalRecord.company" var="companyHeader" />
	<display:column title="${companyHeader}" property="company" sortable="true"/>
	
	<spring:message code="curricula.professionalRecord.startDate" var="startDateHeader" />
	<display:column title="${startDateHeader}" property="startDate" format="{0,date,dd/MM/YYYY}" sortable="true"/>
		
	<spring:message code="curricula.professionalRecord.endDate" var="endDateHeader" />
	<display:column title="${endDateHeader}" property="endDate" format="{0,date,dd/MM/YYYY}" sortable="true"/>
	
	<spring:message code="curricula.professionalRecord.role" var="roleHeader" />
	<display:column title="${roleHeader}" property="role" sortable="true"/>
	
	<spring:message code="curricula.professionalRecord.attachment" var="attachmentHeader" />
	<display:column title="${attachmentHeader}" property="attachment"/>
	
	<spring:message code="curricula.professionalRecord.comments" var="commentsHeader" />
	<display:column title="${commentsHeader}" property="comments"/>
	
	<display:column>
			<acme:button href="professionalRecord/candidate/edit.do?professionalRecordID=${row.id}"
				name="edit" code="curricula.edit" />
	</display:column>

	<display:column>
			<acme:button href="professionalRecord/candidate/delete.do?professionalRecordID=${row.id}"
				name="edit" code="curricula.delete" />
	</display:column>

</display:table>

<br/>
<br/>
<a href="professionalRecord/candidate/create.do?curriculaID=${curricula.id}"><spring:message code="record.create"/></a>

<br/>
<br/>
<b><spring:message code="curricula.endorserRecords" /></b>
<p />

<display:table name="endorserRecords" id="row" requestURI="${requestURI}"
	pagesize="5" class="displaytag">


	<spring:message code="curricula.endorserRecord.fullName" var="fullNameHeader" />
	<display:column title="${fullNameHeader}" property="fullName" sortable="true"/>
	
	<spring:message code="curricula.endorserRecord.email" var="emailHeader" />
	<display:column title="${emailHeader}" property="email" sortable="true"/>
		
	<spring:message code="curricula.endorserRecord.phone" var="phoneHeader" />
	<display:column title="${phoneHeader}" property="phone" sortable="true"/>
	
	<spring:message code="curricula.endorserRecord.linkedInUrl" var="linkedInUrlHeader" />
	<display:column title="${linkedInUrlHeader}" property="linkedInUrl" sortable="true"/>
	
	<spring:message code="curricula.endorserRecord.comments" var="commentsHeader" />
	<display:column title="${commentsHeader}" property="comments"/>
	
	<display:column>
			<acme:button href="endorserRecord/candidate/edit.do?endorserRecordID=${row.id}"
				name="edit" code="curricula.edit" />
	</display:column>
	
	<display:column>
			<acme:button href="endorserRecord/candidate/delete.do?endorserRecordID=${row.id}"
				name="edit" code="curricula.delete" />
	</display:column>

</display:table>

<br/>
<br/>
<a href="endorserRecord/candidate/create.do?curriculaID=${curricula.id}"><spring:message code="record.create"/></a>

<br/>
<br/>
<b><spring:message code="curricula.miscellaneousRecords" /></b>
<p />

<display:table name="miscellaneousRecords" id="row" requestURI="${requestURI}"
	pagesize="5" class="displaytag">


	<spring:message code="curricula.miscellaneousRecord.title" var="titleHeader" />
	<display:column title="${titleHeader}" property="title" sortable="true"/>
	
	<spring:message code="curricula.miscellaneousRecord.attachment" var="attachmentHeader" />
	<display:column title="${attachmentHeader}" property="attachment"/>
	
	<spring:message code="curricula.miscellaneousRecord.comments" var="commentsHeader" />
	<display:column title="${commentsHeader}" property="comments"/>
	
	<display:column>
			<acme:button href="miscellaneousRecord/candidate/edit.do?miscellaneousRecordID=${row.id}"
				name="edit" code="curricula.edit" />
	</display:column>
	
	<display:column>
			<acme:button href="miscellaneousRecord/candidate/delete.do?miscellaneousRecordID=${row.id}"
				name="edit" code="curricula.delete" />
	</display:column>

</display:table>

<br/>
<br/>
<a href="miscellaneousRecord/candidate/create.do?curriculaID=${curricula.id}"><spring:message code="record.create"/></a>