<%@page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<%@taglib prefix="security"
	uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="acme" tagdir="/WEB-INF/tags"%>

<jstl:if test="${requestURI.contains('all') || requestURI.contains('search') }">
	<form method="POST" action="curricula/search.do">
	
		<spring:message code="curricula.keyword" var="keywordPlaceholder"/>
		<input type="text" id="keyword" name="keyword" placeholder="${keywordPlaceholder}"/> 
		
		<input type="submit" name="search" value="<spring:message code="curricula.search"/>" />
	</form>
</jstl:if>

<display:table name="curriculas" id="row" requestURI="${requestURI}"
	pagesize="5" class="displaytag">

	<spring:message code="curricula.ticker" var="ticker" />
	<display:column property="ticker" title="${ticker}" sortable="true"/>
	
	<display:column>
			<acme:button href="curricula/view.do?curriculaID=${row.id}"
				name="see" code="curricula.view" />
	</display:column>
	
	<display:column>
			<acme:button href="curricula/delete.do?curriculaID=${row.id}"
				name="see" code="curricula.delete" />
	</display:column>
	
</display:table>

<br/>
<a href="personalRecord/candidate/create.do"><spring:message code="curricula.create"/></a>

