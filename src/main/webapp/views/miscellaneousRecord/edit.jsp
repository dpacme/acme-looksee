<%@page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<%@taglib prefix="security"
	uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<%@taglib prefix="acme" tagdir="/WEB-INF/tags"%>

<!-- Edici�n de artist (como usuario autentificado) -->

<form:form action="${requestURI}" modelAttribute="miscellaneousRecord">

	<form:hidden path="id" />
	<form:hidden path="version" />
	<form:hidden path="curricula" />

	<!-- Campos obligatorios a rellenar -->

	<acme:textbox code="curricula.miscellaneousRecord.title" path="title" />
	
	<acme:textbox  code="curricula.miscellaneousRecord.attachment" path="attachment"/>
	
	<acme:textbox code="curricula.miscellaneousRecord.comments" path="comments"/>

	<br />
	
	

	<!-- Acciones -->
	
	<acme:submit name="save" code="curricula.miscellaneousRecord.save"/>
	
	<acme:cancel url="curricula/view.do?curriculaID=${curriculaID}" code="curricula.miscellaneousRecord.cancel"/>

</form:form>

<br>
