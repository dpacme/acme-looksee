<%@page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<%@taglib prefix="security"
	uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<%@taglib prefix="acme" tagdir="/WEB-INF/tags"%>

<!-- Edici�n de artist (como usuario autentificado) -->

<form:form action="${requestURI}" modelAttribute="personalRecord">

	<form:hidden path="id" />
	<form:hidden path="version" />
	<form:hidden path="curriculas" />

	<!-- Campos obligatorios a rellenar -->
	<jstl:if test="${requestURI.contains('create')}">
		<b><spring:message code="curricula.PersonalData" /></b>
	</jstl:if>

	<br />
	<br />

	<acme:textbox code="curricula.personal.fullName" path="fullName" />

	<acme:textbox code="curricula.personal.picture" path="picture" />

	<acme:textbox code="curricula.personal.email" path="email" />
	
	<acme:textbox id="phonePersonalRecord" code="curricula.personal.phone" path="phone" placeholder="+XX (YYY) ZZZZ"/>
	
	<acme:textbox code="curricula.personal.linkedInUrl" path="linkedInUrl"/>

	<br />
	
	

	<!-- Acciones -->
	
	<spring:message code="curricula.personalRecord.saveConfirm" var="saveConfirm"/>
	<acme:submit onclick="return comprobar()" name="save" code="curricula.personalRecord.save"/>
	
	<acme:cancel url="" code="curricula.personalRecord.cancel"/>

</form:form>

<br>

<script>
	function comprobar(){
		var phone = document.getElementById("phonePersonalRecord").value;
		var exp = "^(\\+\\d{2}[ ]?(\\(\\d{1,3}\\))?[ ]?\\d{4,}$)";
		if(!phone.match(exp) && phone != ""){
			return confirm("<spring:message code='curricula.personalRecord.saveConfirm'/>");
		}
	}
</script>

