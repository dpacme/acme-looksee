<%@page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<%@taglib prefix="security"
	uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="acme" tagdir="/WEB-INF/tags"%>

<jsp:useBean id="loginService" class="security.LoginService"
				scope="page" />

<display:table name="applies" id="row" requestURI="${requestURI}"
	pagesize="5" class="displaytag">

	<spring:message code="apply.candidate.fullname" var="nameHeader" />
	<display:column title="${nameHeader}" sortable="true">
		${row.curricula.candidate.name} ${row.curricula.candidate.surname} 
	</display:column>
	
	<spring:message code="apply.moment" var="momentCreateHeader" />
	<display:column property="momment" format="{0,date,dd/MM/YYYY}" 
	title="${momentCreateHeader}" sortable="true"/>
	
	<spring:message code="apply.status" var="statusHeader" />
	<display:column title="${statusHeader}" sortable="true">
		<jstl:if test="${row.status == 'accepted'}">
			<spring:message code="apply.status.accepted"/>
		</jstl:if>
		<jstl:if test="${row.status == 'rejected'}">
			<spring:message code="apply.status.rejected"/>
		</jstl:if>
		<jstl:if test="${row.status == 'pending'}">
			<spring:message code="apply.status.pending"/>
		</jstl:if>
	</display:column>
	
	<security:authorize access="hasRole('CANDIDATE')">
	
	<spring:message code="offer.title" var="titleHeader" />
	<display:column property="offer.title" 
	title="${titleHeader}" sortable="true"/>
	
	<spring:message code="offer.deadLine" var="deadlineHeader" />
	<display:column property="offer.deadLine" format="{0,date,dd/MM/YYYY}" 
	title="${deadlineHeader}" sortable="true"/>
	
	</security:authorize>
	
	<security:authorize access="hasRole('COMPANY') || hasRole('CANDIDATE')">
		
		<security:authorize access="hasRole('COMPANY')">
		<spring:message code="apply.curricula" var="curriculaHeader" />
		<display:column title="${curriculaHeader}">
			<acme:button href="curricula/show.do?applyID=${row.id}"
				name="see" code="apply.see" />
		</display:column>
		</security:authorize>
		
		<security:authorize access="hasRole('CANDIDATE')">
		<spring:message code="apply.curricula" var="curriculaHeader" />
		<display:column title="${curriculaHeader}">
		<jstl:if test="${row.getCurricula().getCandidate().getUserAccount().getId() == loginService.getPrincipal().getId()}">
			<acme:button href="curricula/show.do?applyID=${row.id}"
				name="see" code="apply.see" />		
		</jstl:if>
		</display:column>
		</security:authorize>
	</security:authorize>
	
		
	<security:authorize access="hasRole('COMPANY')">
		<jstl:if test="${row.getOffer().getCompany().getUserAccount().getId() == loginService.getPrincipal().getId()}">
			<spring:message code="apply.actions" var="actionsHeader" />
			<display:column title="${actionsHeader}">				
				<jstl:choose>
					<jstl:when test="${row.status != 'accepted'}">
						<div class="inline">
							<acme:button href="apply/company/setApplyAsAccepted.do?applyID=${row.id}"
							name="accepted" code="apply.setAsAccepted" />
						</div>
					</jstl:when>
					<jstl:otherwise>
						<div class="inline">
							<input type="button" href="#"
							disabled="disabled"
							name="accepted" value='<spring:message code="apply.setAsAccepted" />' />
						</div>
					</jstl:otherwise>
				</jstl:choose>
				<jstl:choose>
					<jstl:when test="${row.status != 'rejected'}">
						<div class="inline">
							<acme:button href="apply/company/setApplyAsRejected.do?applyID=${row.id}"
							name="rejected" code="apply.setAsRejected" />
						</div>
					</jstl:when>
					<jstl:otherwise>
						<div class="inline">
							<input type="button" href="#"
							disabled="disabled"
							name="rejected" value='<spring:message code="apply.setAsRejected" />' />
						</div>
					</jstl:otherwise>
				</jstl:choose>		
				<jstl:choose>
					<jstl:when test="${row.status != 'pending'}">
						<div class="inline">
							<acme:button href="apply/company/setApplyAsPending.do?applyID=${row.id}"
							name="pending" code="apply.setAsPending" />
						</div>
					</jstl:when>
					<jstl:otherwise>
						<div class="inline">
							<input type="button" href="#"
							disabled="disabled"
							name="pending" value='<spring:message code="apply.setAsPending" />' />
						</div>
					</jstl:otherwise>
				</jstl:choose>
			</display:column>
		</jstl:if>
	</security:authorize>
	

</display:table>

<script type="text/javascript">
	
	    var table = document.getElementById("row");    
	    var tbody = table.getElementsByTagName("tbody")[0];
	    var rows = tbody.getElementsByTagName("tr");
	    
	    for (i=0; i < rows.length; i++) {
	        var value = rows[i].getElementsByTagName("td")[2].firstChild.nodeValue.trim();
	        	        
	        if(value == "Pending"){
	        	rows[i].style.backgroundColor = "orange";
	        }else if(value == "Accepted"){
	        	rows[i].style.backgroundColor = "green";
	        }else if(value == "Rejected"){
	        	rows[i].style.backgroundColor = "red";
	        }else{
	        	rows[i].style.backgroundColor = "black";
	        	rows[i].style.color = "white";
	        }
	    }
	    
	</script>