<%@page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<%@taglib prefix="security"
	uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<%@taglib prefix="acme" tagdir="/WEB-INF/tags"%>

<!-- Edici�n de company (como usuario autentificado) -->

<form:form action="company/edit.do" modelAttribute="company">

	<form:hidden path="id" />
	<form:hidden path="version" />
	<form:hidden path="userAccount" />
	<form:hidden path="banned" />

	<!-- Campos obligatorios a rellenar -->

	<b><spring:message code="company.PersonalData" /></b>

	<br />

	<acme:textbox code="company.name" path="name" />

	<acme:textbox code="company.surname" path="surname" />

	<acme:textbox code="company.email" path="email" />
	
	<acme:textbox id="phone" code="company.phone" path="phone" placeholder="+XX (YYY) ZZZZ"/>
	
	<div style="overflow: hidden">
		<div class="inline">
			<acme:textbox code="company.postalAddress" path="postalAddress"
				placeholder="Ej: 41010" />
		</div>
		<jstl:if test="${postal != null}">
			<div>
				<span class="message"><spring:message code="${postal}" /></span>
			</div>
		</jstl:if>
	</div>
	
	<acme:textbox code="company.nameCompany" path="nameCompany" />
	
	<div style="overflow: hidden">
		<div class="inline">
			<acme:textbox code="company.vatNumber" path="vatNumber" placeholder="Ej: BG999999999"/>
		</div>
		<jstl:if test="${duplicateVatNumber != null}">
			<div>
				<span class="message"><spring:message
						code="${duplicateVatNumber}" /></span>
			</div>
		</jstl:if>
	</div>
	<br />
	
	

	<!-- Acciones -->
	
	<spring:message code="company.saveConfirm" var="saveConfirm"/>
	<acme:submit onclick="return comprobar()" name="save" code="company.save"/>
	
	<acme:cancel url="" code="company.cancel"/>

</form:form>

<br>


<script>
	function comprobar(){
		var phone = document.getElementById("phone").value;
		var exp = "^(\\+\\d{2}[ ]?(\\(\\d{1,3}\\))?[ ]?\\d{4,}$)";
		if(!(phone.match(exp))){
			return confirm("<spring:message code='candidate.saveConfirm'/>")
		}
	}
</script>

<!-- Errores -->

