<%--
 * index.jsp
 *
 * Copyright (C) 2014 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the 
 * TDG Licence, a copy of which you may download from 
 * http://www.tdg-seville.info/License.html
 --%>

<%@page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="security"
	uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>

<div>
	<ul>
		<li><b><spring:message code="company.nameCompany" />:</b>
			${company.nameCompany}</li>
		<li><b><spring:message code="company.name" />:</b>
			${company.name}</li>
		<li><b><spring:message code="company.surname" />:</b>
			${company.surname}</li>
		<li><b><spring:message code="company.email" />:</b>
			${company.email}</li>
		<li><b><spring:message code="company.phone" />:</b>
			${company.phone}</li>
		<li><b><spring:message code="company.postalAddress" />:</b>
			${company.postalAddress}</li>
	</ul>
</div>

<br />
