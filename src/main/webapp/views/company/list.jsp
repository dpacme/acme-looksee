<%@page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<%@taglib prefix="security"
	uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="acme" tagdir="/WEB-INF/tags"%>

<jsp:useBean id="loginService" class="security.LoginService"
	scope="page" />

<jsp:useBean id="administratorService"
	class="services.AdministratorService" scope="page" />

<display:table name="companies" id="row" requestURI="${requestURI}"
	pagesize="5" class="displaytag">

	<spring:message code="company.nameCompany" var="nameCompanyHeader" />
	<display:column property="nameCompany" title="${nameCompanyHeader}"
		sortable="true" />

	<spring:message code="company.vatNumber" var="vatNumberHeader" />
	<display:column property="vatNumber" title="${vatNumberHeader}"
		sortable="true" />

	<spring:message code="offer.company" var="companyHeader" />
	<display:column title="${companyHeader}">
		<acme:button href="company/showDisplay.do?companyId=${row.id}" name="see"
			code="offer.see" />
	</display:column>

	<!-- MBC Si el actor logado no es administrador, no se muestra la informaci�n sobre baneos -->
	<security:authorize access="hasRole('ADMINISTRATOR')">
		<spring:message code="company.banned" var="bannedHeader" />
		<display:column title="${bannedHeader}" sortable="true">
			<jstl:choose>
				<jstl:when test="${row.banned == true}">
					<spring:message code="company.banned.yes" />
				</jstl:when>
				<jstl:otherwise>
					<spring:message code="company.banned.no" />
				</jstl:otherwise>
			</jstl:choose>
		</display:column>
	</security:authorize>

	<spring:message code="company.offers" var="offersHeader" />
	<display:column title="${offersHeader}">
		<!-- 
			MBC Se muestra la opci�n de ver las ofertas de la compa��a si no est� baneada,
			o si se trata de la compa��a logada para que pueda seguir viendo sus propias ofertas				
		 -->
		<security:authorize access="isAnonymous()">
			<jstl:choose>
				<jstl:when test="${row.banned == false}">
					<acme:button href="offer/offersFromCompany.do?companyId=${row.id}"
						name="see" code="company.see" />
				</jstl:when>
				<jstl:otherwise>
					<spring:message code="company.see.offers.not.available" />
				</jstl:otherwise>
			</jstl:choose>
		</security:authorize>

		<security:authorize access="hasRole('ADMINISTRATOR')">
			<acme:button href="offer/offersFromCompany.do?companyId=${row.id}"
				name="see" code="company.see" />
		</security:authorize>

		<security:authorize
			access="isAuthenticated() && !hasRole('ADMINISTRATOR')">
			<jstl:choose>

				<jstl:when
					test="${administratorService.isAdminLogged() == true || row.banned == false ||
				row.getUserAccount().getId() == loginService.getPrincipal().getId()}">
					<acme:button href="offer/offersFromCompany.do?companyId=${row.id}"
						name="see" code="company.see" />
				</jstl:when>
				<jstl:otherwise>
					<spring:message code="company.see.offers.not.available" />
				</jstl:otherwise>
			</jstl:choose>
		</security:authorize>
	</display:column>



	<security:authorize access="hasRole('ADMINISTRATOR')">
		<spring:message code="company.actions" var="actionsHeader" />
		<display:column title="${actionsHeader}">
			<jstl:choose>
				<jstl:when test="${row.banned == false}">
					<div class="inline">
						<acme:button
							href="company/administrator/banCompany.do?companyID=${row.id}"
							name="ban" code="company.actions.ban" />
					</div>
					<div class="inline">
						<input type="button" href="#" disabled="disabled" name="unban"
							value='<spring:message code="company.actions.unban" />' />
					</div>
				</jstl:when>
				<jstl:otherwise>
					<div class="inline">
						<input type="button" href="#" disabled="disabled" name="ban"
							value='<spring:message code="company.actions.ban" />' />
					</div>
					<div class="inline">
						<acme:button
							href="company/administrator/unbanCompany.do?companyID=${row.id}"
							name="unban" code="company.actions.unban" />
					</div>
				</jstl:otherwise>
			</jstl:choose>
		</display:column>
	</security:authorize>

</display:table>

