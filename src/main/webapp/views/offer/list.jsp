<%@page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<%@taglib prefix="security"
	uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="acme" tagdir="/WEB-INF/tags"%>

<jstl:if
	test="${requestURI.contains('all') || requestURI.contains('search') }">
	<form method="POST" action="offer/search.do">

		<spring:message code="offer.keyword" var="keywordPlaceholder" />
		<input type="text" id="keyword" name="keyword"
			placeholder="${keywordPlaceholder}" />

		<spring:message code="offer.minSalary" var="minSalaryPlaceholder" />
		<input type="number" id="minSalary" name="minSalary" step="1" min="0"
			placeholder="${minSalaryPlaceholder}" />

		<spring:message code="offer.maxSalary" var="maxSalaryPlaceholder" />
		<input type="number" id="maxSalary" name="maxSalary" step="1" min="0"
			placeholder="${maxSalaryPlaceholder}" /> <input type="submit"
			name="search" value="<spring:message code="offer.search"/>" />
	</form>
</jstl:if>

<display:table name="offers" id="row" requestURI="${requestURI}"
	pagesize="5" class="displaytag">

	<spring:message code="offer.title" var="titleHeader" />
	<display:column property="title" title="${titleHeader}" sortable="true" />

	<spring:message code="offer.description" var="descriptionHeader" />
	<display:column property="description" title="${descriptionHeader}"
		sortable="true" />

	<spring:message code="offer.minSalary" var="minSalaryHeader" />
	<display:column property="minSalary" title="${minSalaryHeader}"
		sortable="true" />

	<spring:message code="offer.maxSalary" var="maxSalaryHeader" />
	<display:column property="maxSalary" title="${maxSalaryHeader}"
		sortable="true" />

	<spring:message code="offer.currency" var="currencyHeader" />
	<display:column property="currency" title="${currencyHeader}"
		sortable="true" />

	<spring:message code="offer.momentCreate" var="momentCreateHeader" />
	<display:column property="momentCreate" format="{0,date,dd/MM/YYYY}"
		title="${momentCreateHeader}" sortable="true" />

	<spring:message code="offer.deadLine" var="deadLineHeader" />
	<display:column property="deadLine" format="{0,date,dd/MM/YYYY HH:mm}"
		title="${deadLineHeader}" sortable="true" />

	<spring:message code="offer.company" var="companyHeader" />
	<display:column title="${companyHeader}">
		<acme:button href="company/showDisplay.do?companyId=${row.company.id}" name="see"
			code="offer.see" />
	</display:column>

	<spring:message code="offer.details" var="offerHeader" />
	<display:column title="${offerHeader}">
		<jstl:if test="${!row.getCompany().getBanned() || isAdmin}">
			<acme:button href="offer/showDisplay.do?offerId=${row.id}" name="see"
				code="offer.see" />
		</jstl:if>
	</display:column>


	<jsp:useBean id="loginService" class="security.LoginService"
		scope="page" />
	<jstl:if test="${!requestURI.contains('drafts')}">
		<security:authorize access="hasRole('COMPANY')">
			<spring:message code="offer.applies" var="appliesHeader" />
			<display:column title="${appliesHeader}">
				<jstl:if
					test="${row.getCompany().getUserAccount().getId() == loginService.getPrincipal().getId()}">
					<acme:button
						href="apply/company/listAppliesFromOffer.do?offerID=${row.id}"
						name="see" code="offer.see" />
				</jstl:if>
			</display:column>
		</security:authorize>
	</jstl:if>

	<jstl:if test="${requestURI.contains('drafts')}">
		<display:column>
			<acme:button href="company/offer/publish.do?offerID=${row.id}"
				name="see" code="offer.publish" />
		</display:column>
		<display:column>
			<acme:button href="company/offer/edit.do?offerID=${row.id}"
				name="see" code="offer.edit" />
		</display:column>
	</jstl:if>


</display:table>

<!-- Errores -->

<jstl:if test="${deadline != null}">
	<span class="message"><spring:message code="deadline" /></span>
</jstl:if>

