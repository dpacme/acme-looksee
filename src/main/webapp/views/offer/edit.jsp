<%@page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="security"
	uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>


<%@taglib prefix="acme" tagdir="/WEB-INF/tags"%>

<script>
	$(function() {


		$('.datetimepicker').datetimepicker({
			dateFormat : 'dd/mm/yy',
		});

	});
</script>

<form:form action="${requestURI}" modelAttribute="offer">

	<form:hidden path="id" />
	<form:hidden path="version" />
	<form:hidden path="momentCreate" />
	<form:hidden path="draft" />
	<form:hidden path="company" />
	<form:hidden path="applies" />

	<acme:textbox code="offer.title" path="title" />
	<acme:textbox code="offer.description" path="description" />
	
	<spring:message code="offer.minSalary" />
	<form:input type="number" min="0" step="1" path="minSalary"
							required="true" />
	<form:errors cssClass="error" path="minSalary" />
	<br/>
	<spring:message code="offer.maxSalary" />
	<form:input type="number" min="0" step="1" path="maxSalary"
							required="true" />
	<form:errors cssClass="error" path="maxSalary" />
	
	<acme:textbox code="offer.currency" path="currency" />
	
	<spring:message code="offer.deadLine" />
	<spring:message code="offer.date.click" var="datePlaceholder" />
	<form:input type="text" id="deadLine" path="deadLine" readonly="readonly"
		placeholder="${datePlaceholder}" class="datetimepicker" />
	<form:errors class="error" path="deadLine" />
	<br />
	
	<jstl:if test="${requestURI.contains('create')}">
		<acme:submit name="save" code="offer.save" />
	</jstl:if>
	<jstl:if test="${requestURI.contains('publish')}">
		<acme:submit name="save" code="offer.publish" />
	</jstl:if>
	<jstl:if test="${requestURI.contains('edit')}">
		<jstl:if test="${offer.id != 0}">
			<acme:submit name="save" code="offer.save" />
			<acme:submit name="delete" code="offer.delete" />
		</jstl:if>
	</jstl:if>
	<acme:cancel url="company/offer/myOffers.do" code="offer.cancel" />

</form:form>

<br/>
<spring:message code="offer.information" />

<!-- Errores -->

<jstl:if test="${salaries != null}">
	<span class="message"><spring:message code="${salaries}" /></span>
</jstl:if>
