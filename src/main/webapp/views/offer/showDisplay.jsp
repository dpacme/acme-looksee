<%--
 * index.jsp
 *
 * Copyright (C) 2014 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the 
 * TDG Licence, a copy of which you may download from 
 * http://www.tdg-seville.info/License.html
 --%>

<%@page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>

<%@taglib prefix="jstl"	uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="security" uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>

<div>
	<ul>
		<li>
			<b><spring:message code="offer.title"/>:</b>
			${offer.title}
		</li>
		<li>
			<b><spring:message code="offer.description"/>:</b>
			${offer.description}
		</li>
		<li>
			<b><spring:message code="offer.minSalary"/>:</b>
			${offer.minSalary}
		</li>
		<li>
			<b><spring:message code="offer.maxSalary"/>:</b>
			${offer.maxSalary}
		</li>
		<li>
			<b><spring:message code="offer.momentCreate"/>:</b>
			${offer.momentCreate}
		</li>
		<li>
			<b><spring:message code="offer.deadLine"/>:</b>
			${offer.deadLine}
		</li>
	</ul>
</div>

<br/>

<security:authorize access="hasRole('CANDIDATE')">
			<jstl:if test="${offer.deadLine >= currentDate && applyRealized == false}">
					<jstl:choose>
						<jstl:when test="${allCurriculas == null || empty allCurriculas}">
							<spring:message code="offer.applyCurricula" />
						</jstl:when>
						<jstl:otherwise>
							<spring:message code="offer.applyForOffer" />
							<br/>
							<form action="apply/candidate/applyOffer.do?" method="post">
							  <input type="hidden" name="offerId" value="${offer.id}"/>					 
							  <select name="curriculaId" style="margin-bottom: 10px">
						  		<jstl:forEach var="curricula" items="${allCurriculas}">
									    <option label = "${curricula.ticker}" value="${curricula.id}">
										${curricula.ticker}
										</option>	    
								</jstl:forEach>
							  </select>
								<input type="submit" value="<spring:message code="offer.applyFor" />"/>
							</form>
						</jstl:otherwise>
					</jstl:choose>
					
					
			</jstl:if>
</security:authorize>

<jstl:if test="${applyRealized}">
	<spring:message code="offer.applyRealized" />
</jstl:if>