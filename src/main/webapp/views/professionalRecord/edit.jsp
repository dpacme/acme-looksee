<%@page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<%@taglib prefix="security"
	uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<%@taglib prefix="acme" tagdir="/WEB-INF/tags"%>

<!-- Edici�n de artist (como usuario autentificado) -->

<form:form action="${requestURI}" modelAttribute="professionalRecord">

	<form:hidden path="id" />
	<form:hidden path="version" />
	<form:hidden path="curricula" />

	<!-- Campos obligatorios a rellenar -->

	<acme:textbox code="curricula.professionalRecord.company" path="company" />

	<acme:textbox code="curricula.professionalRecord.startDate" path="startDate" placeholder="dd/MM/yyyy"/>

	<acme:textbox code="curricula.professionalRecord.endDate" path="endDate" placeholder="dd/MM/yyyy"/>
	
	<acme:textbox code="curricula.professionalRecord.role" path="role"/>
	
	<acme:textbox code="curricula.professionalRecord.attachment" path="attachment"/>
	
	<acme:textbox code="curricula.professionalRecord.comments" path="comments"/>

	<br />
	
	

	<!-- Acciones -->
	
	<acme:submit name="save" code="curricula.professionalRecord.save"/>
	
	<acme:cancel url="curricula/view.do?curriculaID=${curriculaID}" code="curricula.professionalRecord.cancel"/>

</form:form>

<br>

<!-- Errores -->

<jstl:if test="${date != null}">
	<span class="message"><spring:message code="${date}" /></span>
</jstl:if>

