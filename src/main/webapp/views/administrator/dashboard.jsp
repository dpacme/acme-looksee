<%--
 * action-1.jsp
 *
 * Copyright (C) 2016 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the 
 * TDG Licence, a copy of which you may download from 
 * http://www.tdg-seville.info/License.html
 --%>

<%@page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="security"
	uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>


<security:authorize access="hasRole('ADMINISTRATOR')">

	<h2>
		<spring:message code="administrator.candidatesInDescendingOrderByNumberOfCurricula" />
	</h2>
	<display:table pagesize="5" class="displaytag" keepStatus="true" name="candidatesInDescendingOrderByNumberOfCurricula" requestURI="${requestURI}" id="row">

		<!-- Attributes -->
		
		<spring:message code="candidate.name" var="name" />
		<display:column property="name" title="${name}" sortable="true" />
		
		<spring:message code="candidate.surname" var="surname" />
		<display:column property="surname" title="${surname}" sortable="true" />
		
		<spring:message code="candidate.email" var="email" />
		<display:column property="email" title="${email}" sortable="true" />
		
		<spring:message code="candidate.phone" var="phone" />
		<display:column property="phone" title="${phone}" sortable="true" />
		
		<spring:message code="candidate.postalAddress" var="postalAddress" />
		<display:column property="postalAddress" title="${postalAddress}" sortable="true" />
		
		<spring:message code="candidate.curriculas" var="curriculas" />
		<display:column title="${curriculas}">
		${fn:length(row.curriculas)}
		</display:column>
		
	</display:table>
	<br/>
	
	<h2>
		<spring:message code="administrator.companiesInDescendingOrderByNumberOfOffers" />
	</h2>
	<display:table pagesize="5" class="displaytag" keepStatus="true" name="companiesInDescendingOrderByNumberOfOffers" requestURI="${requestURI}" id="row">

		<!-- Attributes -->
		
		<spring:message code="company.name" var="name" />
		<display:column property="name" title="${name}" sortable="true" />
		
		<spring:message code="company.surname" var="surname" />
		<display:column property="surname" title="${surname}" sortable="true" />
		
		<spring:message code="company.email" var="email" />
		<display:column property="email" title="${email}" sortable="true" />
		
		<spring:message code="company.phone" var="phone" />
		<display:column property="phone" title="${phone}" sortable="true" />
		
		<spring:message code="company.postalAddress" var="postalAddress" />
		<display:column property="postalAddress" title="${postalAddress}" sortable="true" />
		
		<spring:message code="company.offers" var="offers" />
		<display:column title="${offers}">
		${fn:length(row.offers)}
		</display:column>
		
	</display:table>
	<br/>
	
	<h2>
		<spring:message code="administrator.candidatesWithMoreCurriculas" />
	</h2>
	<display:table pagesize="5" class="displaytag" keepStatus="true" name="candidatesWithMoreCurriculas" requestURI="${requestURI}" id="row">

		<!-- Attributes -->
		
		<spring:message code="candidate.name" var="name" />
		<display:column property="name" title="${name}" sortable="true" />
		
		<spring:message code="candidate.surname" var="surname" />
		<display:column property="surname" title="${surname}" sortable="true" />
		
		<spring:message code="candidate.email" var="email" />
		<display:column property="email" title="${email}" sortable="true" />
		
		<spring:message code="candidate.phone" var="phone" />
		<display:column property="phone" title="${phone}" sortable="true" />
		
		<spring:message code="candidate.postalAddress" var="postalAddress" />
		<display:column property="postalAddress" title="${postalAddress}" sortable="true" />
		
		<spring:message code="candidate.curriculas" var="curriculas" />
		<display:column title="${curriculas}">
		${fn:length(row.curriculas)}
		</display:column>
		
	</display:table>
	<br/>
	
		<h2>
		<spring:message code="administrator.companiesWithMoreOffers" />
	</h2>
	<display:table pagesize="5" class="displaytag" keepStatus="true" name="companiesWithMoreOffers" requestURI="${requestURI}" id="row">

		<!-- Attributes -->
		
		<spring:message code="company.name" var="name" />
		<display:column property="name" title="${name}" sortable="true" />
		
		<spring:message code="company.surname" var="surname" />
		<display:column property="surname" title="${surname}" sortable="true" />
		
		<spring:message code="company.email" var="email" />
		<display:column property="email" title="${email}" sortable="true" />
		
		<spring:message code="company.phone" var="phone" />
		<display:column property="phone" title="${phone}" sortable="true" />
		
		<spring:message code="company.postalAddress" var="postalAddress" />
		<display:column property="postalAddress" title="${postalAddress}" sortable="true" />
		
		<spring:message code="company.offers" var="offers" />
		<display:column title="${offers}">
		${fn:length(row.offers)}
		</display:column>
		
	</display:table>
	<br/>
	
	<h2>
		<spring:message code="administrator.averageNumberCurriculaPerCandidate" />
	</h2>
	<jstl:out value="${averageNumberCurriculaPerCandidate}" />
	<br>
	
	<h2>
		<spring:message code="administrator.averageNumberOffersPerCompany" />
	</h2>
	<jstl:out value="${averageNumberOffersPerCompany}" />
	<br>
	
	<h2>
		<spring:message code="administrator.avgMinMaxNumberApplicationsPerCandidate" />
	</h2>
	<jstl:out value="${avgNumberApplicationsPerCandidate}" />
	<br>
	<jstl:out value="${minNumberApplicationsPerCandidate}" />
	<br>
	<jstl:out value="${maxNumberApplicationsPerCandidate}" />
	<br>
	
	<h2>
		<spring:message code="administrator.avgMinMaxNumberApplicationsPerOffer" />
	</h2>
	<jstl:out value="${avgNumberApplicationsPerOffer}" />
	<br>
	<jstl:out value="${minNumberApplicationsPerOffer}" />
	<br>
	<jstl:out value="${maxNumberApplicationsPerOffer}" />
	<br>
	
	<h2>
		<spring:message code="administrator.avgMinMaxNumberApplicationsPendingPerCompany" />
	</h2>
	<jstl:out value="${avgNumberApplicationsPendingPerCompany}" />
	<br>
	<jstl:out value="${minNumberApplicationsPendingPerCompany}" />
	<br>
	<jstl:out value="${maxNumberApplicationsPendingPerCompany}" />
	<br>
	
	<h2>
		<spring:message code="administrator.avgMinMaxNumberApplicationsAcceptedPerCompany" />
	</h2>
	<jstl:out value="${avgNumberApplicationsAcceptedPerCompany}" />
	<br>
	<jstl:out value="${minNumberApplicationsAcceptedPerCompany}" />
	<br>
	<jstl:out value="${maxNumberApplicationsAcceptedPerCompany}" />
	<br>
	
	<h2>
		<spring:message code="administrator.avgMinMaxNumberApplicationsRejectedPerCompany" />
	</h2>
	<jstl:out value="${avgNumberApplicationsRejectedPerCompany}" />
	<br>
	<jstl:out value="${minNumberApplicationsRejectedPerCompany}" />
	<br>
	<jstl:out value="${maxNumberApplicationsRejectedPerCompany}" />
	<br>
	



</security:authorize>