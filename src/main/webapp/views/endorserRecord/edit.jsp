<%@page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<%@taglib prefix="security"
	uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<%@taglib prefix="acme" tagdir="/WEB-INF/tags"%>

<!-- Edici�n de artist (como usuario autentificado) -->

<form:form action="${requestURI}" modelAttribute="endorserRecord">

	<form:hidden path="id" />
	<form:hidden path="version" />
	<form:hidden path="curricula" />

	<!-- Campos obligatorios a rellenar -->

	<acme:textbox code="curricula.endorserRecord.fullName" path="fullName" />

	<acme:textbox code="curricula.endorserRecord.email" path="email"/>

	<acme:textbox id="phoneEndorser" code="curricula.endorserRecord.phone" path="phone" />
	
	<acme:textbox code="curricula.endorserRecord.linkedInUrl" path="linkedInUrl"/>
	
	<acme:textbox code="curricula.endorserRecord.comments" path="comments"/>

	<br />
	
	

	<!-- Acciones -->
	
	<acme:submit onclick="return comprobar()" name="save" code="curricula.endorserRecord.save"/>
	
	<acme:cancel url="curricula/view.do?curriculaID=${curriculaID}" code="curricula.endorserRecord.cancel"/>

</form:form>

<br>

<script>
	function comprobar(){
		var phone = document.getElementById("phoneEndorser").value;
		var exp = "^(\\+\\d{2}[ ]?(\\(\\d{1,3}\\))?[ ]?\\d{4,}$)";
		if(!(phone.match(exp))){
			return confirm("<spring:message code='curricula.endorseRecord.saveConfirm'/>")
		}
	}
</script>


