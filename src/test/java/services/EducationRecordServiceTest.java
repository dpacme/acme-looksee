
package services;

import java.util.Collection;
import java.util.Date;
import java.util.List;

import javax.transaction.Transactional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.util.Assert;

import domain.Candidate;
import domain.Curricula;
import domain.EducationRecord;
import utilities.AbstractTest;

@Transactional
@ContextConfiguration(locations = {
	"classpath:spring/junit.xml"
})
@RunWith(SpringJUnit4ClassRunner.class)
public class EducationRecordServiceTest extends AbstractTest {

	// Services and repositories
	@Autowired
	private CurriculaService		curriculaService;

	@Autowired
	private EducationRecordService	educationRecordService;

	@Autowired
	private CandidateService		candidateService;

	// Templates --------------------------------------------------------------


	// An actor who is authenticated as a candidate must be able to: Edit his or her curricula.
	// Comprobamos que los curricula se editan correctamente y para los tests negativos vemos si salta la excepci�n correcta si se le pasan par�metros incorrectos.
	// Test positivo y 2 tests negativosTest positivo y 2 tests negativos

	protected void template2(final String username, final Integer curriculaId, final Class<?> expected) {
		Class<?> caught;

		caught = null;
		try {
			this.authenticate(username);
			System.out.println("#editCurricula");
			final Curricula curricula = this.curriculaService.findOne(curriculaId);
			final Collection<EducationRecord> educationRecords = curricula.getEducationRecords();
			Integer i = 1;
			for (final EducationRecord o : educationRecords) {
				o.setComments("Comentario random" + i);
				i++;
				System.out.println(o.getTitle());
				final EducationRecord educationRecordSaved = this.educationRecordService.saveAndFlush(o);
				Assert.isTrue(educationRecordSaved != null && educationRecordSaved.getId() != 0);
				System.out.println(educationRecordSaved.getComments());
			}

			this.unauthenticate();
		} catch (final Throwable oops) {
			caught = oops.getClass();
		}

		this.checkExceptions(expected, caught);
	}

	// An actor who is authenticated as a candidate must be able to: List his or her curricula.
	// Comprobamos que los curricula se listan correctamente y para los tests negativos vemos si salta la excepci�n correcta si se le pasan par�metros incorrectos.
	// Test positivo y 2 tests negativos
	protected void template3(final String username, final Integer curriculaId, final Class<?> expected) {
		Class<?> caught;

		caught = null;
		try {
			this.authenticate(username);
			final Curricula curricula = this.curriculaService.findOne(curriculaId);
			System.out.println("#listCurriculaByCandidate");
			Assert.isTrue(curricula.getEducationRecords() != null && !curricula.getEducationRecords().isEmpty());
			for (final EducationRecord o : curricula.getEducationRecords())
				System.out.println(o.getTitle());

			this.unauthenticate();
		} catch (final Throwable oops) {
			caught = oops.getClass();
		}

		this.checkExceptions(expected, caught);
	}

	/*
	 * An actor who is authenticated as a candidate must be able to: Edit an existing curriculum.
	 *
	 * En este caso se realizara la acci�n de crear un registro educativo en una curricula ya existente. Para forzar el fallo se pueden dar los siguientes casos
	 * Para forzar el fallo se pueden dar los siguientes casos:
	 *
	 * - El usuario logueado no es un candidate
	 * - La curricula no pertenece al usuario logueado
	 * - La curricula no existe
	 * - Los campos no son correctos
	 * - El startdate es mayor que el enddate
	 *
	 */

	public void create(final String username, final int curriculaID, final String title, final String startDate, final String endDate, final String institution, final String attachment, final String comments, final Class<?> expected) {

		Class<?> caught = null;

		try {

			this.authenticate(username);

			this.candidateService.checkIfCandidate();

			final Curricula curricula = this.curriculaService.findOne(curriculaID);
			Assert.isTrue(this.candidateService.findByPrincipal().getId() == curricula.getCandidate().getId());
			final EducationRecord educationRecord = this.educationRecordService.create(curricula);
			educationRecord.setTitle(title);
			educationRecord.setStartDate(new Date(startDate));
			educationRecord.setEndDate(new Date(endDate));
			educationRecord.setInstitution(institution);
			educationRecord.setAttachment(attachment);
			educationRecord.setComments(comments);
			this.educationRecordService.comprobacion(educationRecord);
			this.educationRecordService.save(educationRecord);

			this.unauthenticate();

		} catch (final Throwable oops) {

			caught = oops.getClass();

		}

		this.checkExceptions(expected, caught);
	}

	/*
	 * An actor who is authenticated as a candidate must be able to: Edit an existing curriculum.
	 *
	 * En este caso se realizara la acci�n de borrar un registro educativo en una curricula ya existente. Para forzar el fallo se pueden dar los siguientes casos
	 * Para forzar el fallo se pueden dar los siguientes casos:
	 *
	 * - El usuario logueado no es un candidate
	 * - La curricula no pertenece al usuario logueado
	 * - La curricula no existe
	 * - El education record no existe
	 * - El education record no pertenece a la curricula
	 *
	 */
	public void delete(final String username, final int curriculaID, final int educationRecordID, final Class<?> expected) {

		Class<?> caught = null;

		try {

			this.authenticate(username);

			this.candidateService.checkIfCandidate();

			final Curricula curricula = this.curriculaService.findOne(curriculaID);
			final EducationRecord educationRecord = this.educationRecordService.findOne(educationRecordID);
			Assert.isTrue(this.candidateService.findByPrincipal().getId() == curricula.getCandidate().getId());
			Assert.isTrue(curricula.getEducationRecords().contains(educationRecord));

			this.educationRecordService.delete(educationRecord);

			this.unauthenticate();

		} catch (final Throwable oops) {

			caught = oops.getClass();

		}

		this.checkExceptions(expected, caught);
	}

	// Drivers ----------------------------------------------------------------

	@Test
	public void createDriver() {

		final Object testingData[][] = {
			// Crear education record como no autentificado -> false
			{
				null, 987, "Title create test", "16/07/2010", "16/07/2012", "Institution name test", "http://www.attachment.com", "Comments test", IllegalArgumentException.class
			},
			// Crear education record como admin -> false
			{
				"admin", 987, "Title create test", "16/07/2010", "16/07/2012", "Institution name test", "http://www.attachment.com", "Comments test", IllegalArgumentException.class
			},
			// Crear education record como company -> false
			{
				"company1", 987, "Title create test", "16/07/2010", "16/07/2012", "Institution name test", "http://www.attachment.com", "Comments test", IllegalArgumentException.class
			},
			// La curricula no pertenece al usuario logueado-> false
			{
				"candidate3", 987, "Title create test", "16/07/2010", "16/07/2012", "Institution name test", "http://www.attachment.com", "Comments test", IllegalArgumentException.class
			},
			// La curricula no existe -> false
			{
				"candidate1", 645645, "Title create test", "16/07/2010", "16/07/2012", "Institution name test", "http://www.attachment.com", "Comments test", IllegalArgumentException.class
			},
			// Los campos no son correctos -> false
			{
				"candidate1", 987, "", "16/07/2010", "16/07/2012", "", "http://www.attachment.com", "Comments test", IllegalArgumentException.class
			},
			// El startDate es mayor que el endDate -> false
			{
				"candidate1", 987, "Title create test", "16/07/2012", "16/07/2010", "Institution name test", "http://www.attachment.com", "Comments test", IllegalArgumentException.class
			},
			// Crear correctamente -> true
			{
				"candidate1", 987, "Title create test", "16/07/2010", "16/07/2012", "Institution name test", "http://www.attachment.com", "Comments test", null
			}

		};
		for (int i = 0; i < testingData.length; i++)
			this.create((String) testingData[i][0], (int) testingData[i][1], (String) testingData[i][2], (String) testingData[i][3], (String) testingData[i][4], (String) testingData[i][5], (String) testingData[i][6], (String) testingData[i][7],
				(Class<?>) testingData[i][8]);
	}

	@Test
	public void deleteDriver() {

		final Object testingData[][] = {
			// Delete education record como no autentificado -> false
			{
				null, 987, 997, IllegalArgumentException.class
			},
			// Delete education record como admin -> false
			{
				"admin", 987, 997, IllegalArgumentException.class
			},
			// Delete education record como company -> false
			{
				"company1", 987, 997, IllegalArgumentException.class
			},
			// La curricula no pertenece al usuario logueado-> false
			{
				"candidate3", 987, 997, IllegalArgumentException.class
			},
			// La curricula no existe -> false
			{
				"candidate1", 645645, 997, IllegalArgumentException.class
			},
			// El education record no existe -> false
			{
				"candidate1", 987, 651651, IllegalArgumentException.class
			},
			// El education record no pertenece a la curricula -> false
			{
				"candidate1", 987, 998, IllegalArgumentException.class
			},
			// Borrar education record -> true
			{
				"candidate1", 987, 997, null
			}

		};
		for (int i = 0; i < testingData.length; i++)
			this.delete((String) testingData[i][0], (int) testingData[i][1], (int) testingData[i][2], (Class<?>) testingData[i][3]);
	}

	@Test
	public void listEducationRecordByCurricula() {

		this.authenticate("candidate1");
		final Candidate candidate = this.candidateService.findByPrincipal();
		final List<Curricula> curriculas = (List<Curricula>) candidate.getCurriculas();
		final Object testingData[][] = {
			{
				"candidate1", curriculas.get(0).getId(), null
			}, {
				"candidate1", null, NullPointerException.class
			}, {
				"candidate1", 0, IllegalArgumentException.class
			}
		};

		for (int i = 0; i < testingData.length; i++)
			this.template3((String) testingData[i][0], (Integer) testingData[i][1], (Class<?>) testingData[i][2]);
	}

	@Test
	public void editCurriculaDriver() {

		this.authenticate("candidate1");
		final Candidate candidate = this.candidateService.findByPrincipal();
		final List<Curricula> curriculas = (List<Curricula>) candidate.getCurriculas();

		final Object testingData[][] = {
			{
				"candidate1", curriculas.get(0).getId(), null
			}, {
				"candidate1", null, NullPointerException.class
			}, {
				"candidate1", 0, IllegalArgumentException.class
			}
		};

		for (int i = 0; i < testingData.length; i++)
			this.template2((String) testingData[i][0], (Integer) testingData[i][1], (Class<?>) testingData[i][2]);
	}

}
