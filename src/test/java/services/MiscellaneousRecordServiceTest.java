
package services;

import java.util.Collection;
import java.util.List;

import javax.transaction.Transactional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.util.Assert;

import domain.Candidate;
import domain.Curricula;
import domain.MiscellaneousRecord;
import utilities.AbstractTest;

@Transactional
@ContextConfiguration(locations = {
	"classpath:spring/junit.xml"
})
@RunWith(SpringJUnit4ClassRunner.class)
public class MiscellaneousRecordServiceTest extends AbstractTest {

	// Services and repositories
	@Autowired
	private CurriculaService			curriculaService;

	@Autowired
	private MiscellaneousRecordService	miscellaneousRecordService;

	@Autowired
	private CandidateService			candidateService;

	// Templates --------------------------------------------------------------


	// An actor who is authenticated as a candidate must be able to: Edit his or her curricula.
	// Comprobamos que los curricula se editan correctamente y para los tests negativos vemos si salta la excepci�n correcta si se le pasan par�metros incorrectos.
	// Test positivo y 2 tests negativosTest positivo y 2 tests negativos

	protected void template2(final String username, final Integer curriculaId, final Class<?> expected) {
		Class<?> caught;

		caught = null;
		try {
			this.authenticate(username);
			System.out.println("#editCurricula");
			final Curricula curricula = this.curriculaService.findOne(curriculaId);
			final Collection<MiscellaneousRecord> miscellaneousRecords = curricula.getMiscellaneousRecords();
			Integer i = 1;
			for (final MiscellaneousRecord o : miscellaneousRecords) {
				o.setComments("Comentario random" + i);
				i++;
				System.out.println(o.getTitle());
				final MiscellaneousRecord miscellaneousRecordSaved = this.miscellaneousRecordService.saveAndFlush(o);
				Assert.isTrue(miscellaneousRecordSaved != null && miscellaneousRecordSaved.getId() != 0);
				System.out.println(miscellaneousRecordSaved.getComments());
			}

			this.unauthenticate();
		} catch (final Throwable oops) {
			caught = oops.getClass();
		}

		this.checkExceptions(expected, caught);
	}

	// An actor who is authenticated as a candidate must be able to: List his or her curricula.
	// Comprobamos que los curricula se listan correctamente y para los tests negativos vemos si salta la excepci�n correcta si se le pasan par�metros incorrectos.
	// Test positivo y 2 tests negativos
	protected void template3(final String username, final Integer curriculaId, final Class<?> expected) {
		Class<?> caught;

		caught = null;
		try {
			this.authenticate(username);
			final Curricula curricula = this.curriculaService.findOne(curriculaId);
			System.out.println("#listCurriculaByCandidate");
			Assert.isTrue(curricula.getMiscellaneousRecords() != null && !curricula.getMiscellaneousRecords().isEmpty());
			for (final MiscellaneousRecord o : curricula.getMiscellaneousRecords())
				System.out.println(o.getTitle());

			this.unauthenticate();
		} catch (final Throwable oops) {
			caught = oops.getClass();
		}

		this.checkExceptions(expected, caught);
	}

	/*
	 * An actor who is authenticated as a candidate must be able to: Edit an existing curriculum.
	 *
	 * En este caso se realizara la acci�n de crear un registro adicional en una curricula ya existente. Para forzar el fallo se pueden dar los siguientes casos
	 * Para forzar el fallo se pueden dar los siguientes casos:
	 *
	 * - El usuario logueado no es un candidate
	 * - La curricula no pertenece al usuario logueado
	 * - La curricula no existe
	 * - Los campos no son correctos
	 *
	 */

	public void create(final String username, final int curriculaID, final String title, final String attachment, final String comments, final Class<?> expected) {

		Class<?> caught = null;

		try {

			this.authenticate(username);

			this.candidateService.checkIfCandidate();

			final Curricula curricula = this.curriculaService.findOne(curriculaID);
			Assert.isTrue(this.candidateService.findByPrincipal().getId() == curricula.getCandidate().getId());
			final MiscellaneousRecord miscellaneousRecord = this.miscellaneousRecordService.create(curricula);
			miscellaneousRecord.setTitle(title);
			miscellaneousRecord.setAttachment(attachment);
			miscellaneousRecord.setComments(comments);
			this.miscellaneousRecordService.comprobacion(miscellaneousRecord);
			this.miscellaneousRecordService.save(miscellaneousRecord);

			this.unauthenticate();

		} catch (final Throwable oops) {

			caught = oops.getClass();

		}

		this.checkExceptions(expected, caught);
	}

	/*
	 * An actor who is authenticated as a candidate must be able to: Edit an existing curriculum.
	 *
	 * En este caso se realizara la acci�n de borrar una referencia en una curricula ya existente. Para forzar el fallo se pueden dar los siguientes casos
	 * Para forzar el fallo se pueden dar los siguientes casos:
	 *
	 * - El usuario logueado no es un candidate
	 * - La curricula no pertenece al usuario logueado
	 * - La curricula no existe
	 * - El endorser record no existe
	 * - El endorser record no pertenece a la curricula
	 *
	 */
	public void delete(final String username, final int curriculaID, final int miscellaneousRecordID, final Class<?> expected) {

		Class<?> caught = null;

		try {

			this.authenticate(username);

			this.candidateService.checkIfCandidate();

			final Curricula curricula = this.curriculaService.findOne(curriculaID);
			final MiscellaneousRecord miscellaneousRecord = this.miscellaneousRecordService.findOne(miscellaneousRecordID);
			Assert.isTrue(this.candidateService.findByPrincipal().getId() == curricula.getCandidate().getId());
			Assert.isTrue(curricula.getMiscellaneousRecords().contains(miscellaneousRecord));

			this.miscellaneousRecordService.delete(miscellaneousRecord);

			this.unauthenticate();

		} catch (final Throwable oops) {

			caught = oops.getClass();

		}

		this.checkExceptions(expected, caught);
	}

	// Drivers ----------------------------------------------------------------

	@Test
	public void createDriver() {

		final Object testingData[][] = {
			// Crear micellaneous record como no autentificado -> false
			{
				null, 987, "Title test", "http://www.attachment.com", "Comments test", IllegalArgumentException.class
			},
			// Crear micellaneous record como admin -> false
			{
				"admin", 987, "Title test", "http://www.attachment.com", "Comments test", IllegalArgumentException.class
			},
			// Crear micellaneous record como company -> false
			{
				"company1", 987, "Title test", "http://www.attachment.com", "Comments test", IllegalArgumentException.class
			},
			// La curricula no pertenece al usuario logueado-> false
			{
				"candidate3", 987, "Title test", "http://www.attachment.com", "Comments test", IllegalArgumentException.class
			},
			// La curricula no existe -> false
			{
				"candidate1", 645645, "Title test", "http://www.attachment.com", "Comments test", IllegalArgumentException.class
			},
			// Los campos no son correctos -> false
			{
				"candidate1", 987, "", "http://www.attachment.com", "Comments test", IllegalArgumentException.class
			},
			// Crear correctamente -> true
			{
				"candidate1", 987, "Title test", "http://www.attachment.com", "Comments test", null
			}

		};
		for (int i = 0; i < testingData.length; i++)
			this.create((String) testingData[i][0], (int) testingData[i][1], (String) testingData[i][2], (String) testingData[i][3], (String) testingData[i][4], (Class<?>) testingData[i][5]);
	}

	@Test
	public void deleteDriver() {

		final Object testingData[][] = {
			// Delete miscellaneous record como no autentificado -> false
			{
				null, 987, 1015, IllegalArgumentException.class
			},
			// Delete miscellaneous record como admin -> false
			{
				"admin", 987, 1015, IllegalArgumentException.class
			},
			// Delete miscellaneous record como company -> false
			{
				"company1", 987, 1015, IllegalArgumentException.class
			},
			// La curricula no pertenece al usuario logueado-> false
			{
				"candidate3", 987, 1015, IllegalArgumentException.class
			},
			// La curricula no existe -> false
			{
				"candidate1", 645645, 1015, IllegalArgumentException.class
			},
			// El miscellaneous record no existe -> false
			{
				"candidate1", 987, 669669, IllegalArgumentException.class
			},
			// El miscellaneous record no pertenece a la curricula -> false
			{
				"candidate1", 987, 1018, IllegalArgumentException.class
			},
			// Borrar miscellaneous record -> true
			{
				"candidate1", 987, 1015, null
			}

		};
		for (int i = 0; i < testingData.length; i++)
			this.delete((String) testingData[i][0], (int) testingData[i][1], (int) testingData[i][2], (Class<?>) testingData[i][3]);
	}

	@Test
	public void listMiscellaneousRecordByCurricula() {

		this.authenticate("candidate1");
		final Candidate candidate = this.candidateService.findByPrincipal();
		final List<Curricula> curriculas = (List<Curricula>) candidate.getCurriculas();
		final Object testingData[][] = {
			{
				"candidate1", curriculas.get(0).getId(), null
			}, {
				"candidate1", null, NullPointerException.class
			}, {
				"candidate1", 0, IllegalArgumentException.class
			}
		};

		for (int i = 0; i < testingData.length; i++)
			this.template3((String) testingData[i][0], (Integer) testingData[i][1], (Class<?>) testingData[i][2]);
	}

	@Test
	public void editCurriculaDriver() {

		this.authenticate("candidate1");
		final Candidate candidate = this.candidateService.findByPrincipal();
		final List<Curricula> curriculas = (List<Curricula>) candidate.getCurriculas();

		final Object testingData[][] = {
			{
				"candidate1", curriculas.get(0).getId(), null
			}, {
				"candidate1", null, NullPointerException.class
			}, {
				"candidate1", 0, IllegalArgumentException.class
			}
		};

		for (int i = 0; i < testingData.length; i++)
			this.template2((String) testingData[i][0], (Integer) testingData[i][1], (Class<?>) testingData[i][2]);
	}

}
