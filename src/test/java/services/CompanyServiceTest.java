
package services;

import java.util.Collection;
import java.util.List;

import javax.transaction.Transactional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.util.Assert;

import utilities.AbstractTest;
import domain.Company;
import forms.CompanyForm;

@Transactional
@ContextConfiguration(locations = {
	"classpath:spring/junit.xml"
})
@RunWith(SpringJUnit4ClassRunner.class)
public class CompanyServiceTest extends AbstractTest {

	// Services and repositories
	@Autowired
	private CompanyService	companyService;


	// Templates --------------------------------------------------------------

	/*
	 * An actor who is not authenticated must be able to:
	 * Register in the system as an company.
	 * 
	 * En este caso de uso se llevara a cabo el registro de un company en el sistema
	 * Para forzar el error pueden darse varios casos:
	 * 
	 * � El usuario esta autentificado
	 * � Atributos del registro incorrectos
	 * � No aceptar las condiciones
	 * � Nombre de usuario ya existente
	 * � Contrase�as no coinciden
	 * � Vat incorrecto
	 * � Nombre de compa��a vac�o
	 */
	public void registerCompany(String username, String name, String surname, String email, String phone, String postalAddress, String newUsername, String password, String secondPassword, String nameCompany, String vatNumber, Boolean checkBox,
		Class<?> expected) {

		Class<?> caught = null;

		try {

			authenticate(username);

			// Comprobamos que no este autentificado
			Assert.isTrue(username == null);

			// Inicializamos los atributos para la creaci�n
			CompanyForm actor = new CompanyForm();

			actor.setName(name);
			actor.setSurname(surname);
			actor.setEmail(email);
			actor.setPhone(phone);
			actor.setPostalAddress(postalAddress);
			actor.setNameCompany(nameCompany);
			actor.setVatNumber(vatNumber);

			actor.setUsername(newUsername);
			actor.setPassword(password);
			actor.setSecondPassword(secondPassword);

			actor.setCheckBox(checkBox);

			//Reconsturimos
			Company company = companyService.reconstruct(actor);

			//Comprobamos atributos
			companyService.comprobacion(company);

			//Guardamos
			companyService.saveForm(company);

			unauthenticate();

		} catch (Throwable oops) {

			caught = oops.getClass();

		}

		checkExceptions(expected, caught);
	}

	// Drivers ----------------------------------------------------------------------

	@Test
	public void registerCompanyDriver() {

		Object testingData[][] = {
			// Creaci�n de company como autentificado (1) -> false
			{
				"admin", "NameTest1", "SurnameTest1", "email@domain.com", "+34678432123", "41010", "username1", "password1", "password1", "nameCompanyTest1", "EE123456781", true, IllegalArgumentException.class
			},
			// Creaci�n de company como autentificado (2) -> false
			{
				"company1", "NameTest1", "SurnameTest1", "email@domain.com", "+34678432123", "41010", "username2", "password1", "password1", "nameCompanyTest1", "EE123456782", true, IllegalArgumentException.class
			},
			// Creaci�n de company como autentificado (3) -> false
			{
				"company1", "NameTest1", "SurnameTest1", "email@domain.com", "+34678432123", "41010", "username3", "password1", "password1", "nameCompanyTest1", "EE123456783", true, IllegalArgumentException.class
			},
			// Creaci�n de company con postalAddress incorrecto -> false
			{
				null, "NameTest1", "SurnameTest1", "email@domain.com", "+34678432123", "56118916511", "username4", "password1", "password1", "nameCompanyTest1", "EE123456784", true, IllegalArgumentException.class
			},
			// Creaci�n de company sin aceptar t�rminos -> false
			{
				null, "NameTest1", "SurnameTest1", "email@domain.com", "+34678432123", "41010", "username5", "password1", "password1", "nameCompanyTest1", "EE123456785", false, IllegalArgumentException.class
			},
			// Creaci�n de company con usuario no �nico -> false
			{
				null, "NameTest1", "SurnameTest1", "email@domain.com", "+34678432123", "41010", "company1", "password1", "password1", "nameCompanyTest1", "EE123456786", true, IllegalArgumentException.class
			},
			// Creaci�n de company con contrase�as no coincidentes -> false
			{
				null, "NameTest1", "SurnameTest1", "email@domain.com", "+34678432123", "41010", "username7", "password1", "password2", "nameCompanyTest1", "EE123456787", true, IllegalArgumentException.class
			},
			// Creaci�n de company con todo correcto -> true
			{
				null, "NameTest1", "SurnameTest1", "email@domain.com", "+34678432123", "41010", "username8", "password1", "password1", "nameCompanyTest1", "EE123456788", true, null
			},
			// Creaci�n de company con codigopostal vacio -> true
			{
				null, "NameTest1", "SurnameTest1", "email@domain.com", "+34678432123", "", "username9", "password1", "password1", "nameCompanyTest1", "EE123456771", true, null
			},
			// Creaci�n de company con tel�fono incorrecto -> true
			{
				null, "NameTest1", "SurnameTest1", "email@domain.com", "+34678432123", "41010", "username10", "password1", "password1", "nameCompanyTest1", "EE123456772", true, null
			},
			// Creaci�n de company con vat incorrecto-> true
			{
				null, "NameTest1", "SurnameTest1", "email@domain.com", "+34678432123", "41010", "username8", "password1", "password1", "nameCompanyTest1", "45", true, IllegalArgumentException.class
			},
			// Creaci�n de company con nombre de la compa��a vac�o -> true
			{
				null, "NameTest1", "SurnameTest1", "email@domain.com", "+34678432123", "41010", "username8", "password1", "password1", "", "EE123456773", true, IllegalArgumentException.class
			},

		};
		for (int i = 0; i < testingData.length; i++) {
			registerCompany((String) testingData[i][0], (String) testingData[i][1], (String) testingData[i][2], (String) testingData[i][3], (String) testingData[i][4], (String) testingData[i][5], (String) testingData[i][6], (String) testingData[i][7],
				(String) testingData[i][8], (String) testingData[i][9], (String) testingData[i][10], (Boolean) testingData[i][11], (Class<?>) testingData[i][12]);
		}
	}

	//////////////////////////////////////////////******************************************//////////////////////////////////////////////////////

	/*
	 * An actor who is not authenticated must be able to:
	 * The listing of companies, sorted in descending order by number of offers.
	 * 
	 * En este caso de uso se llevara a cabo el listar las compa�ias que no est�n baneadas
	 * No procede tests negativos al no recibir parametros, se comprueba si todas las compa��as devueltas no estan baneadas.
	 */

	@Test
	public void listOfCompaniesNotBanned() {

		Collection<Company> companies = companyService.findValidCompanies();

		for (Company company : companies) {
			Assert.isTrue(!company.getBanned());
		}

	}

	//////////////////////////////////////////////******************************************//////////////////////////////////////////////////////

	/**
	 * An actor who is authenticated as an administrator must be able to:
	 * Ban or unban a company if he or she thinks that their offers are inappropriate.
	 * Banning a company means that their offers are not displayed by the system,
	 * but the ac-tor that represents it can login and perform any operation with the system.
	 * 
	 * Test correspondiente al baneo de una compa��a
	 * 
	 * Test positivo logado como admin y una company correcta
	 * Test negativo logado como admin y una company incorrecta
	 * Test negativo logado como candidate
	 */
	@Test
	public void banCompany() {

		/*
		 * Preparaci�n test positivo.
		 * 
		 * Obtenemos compa��a
		 */
		List<Company> companies = (List<Company>) companyService.findAll();
		Assert.isTrue(companies != null && !companies.isEmpty(), "No existen compa��as");
		Company company1 = companies.get(0);

		/*
		 * Preparaci�n test negativo.
		 * 
		 * Obtenemos un id de company incorrecto
		 */
		int idCompanyIncorrecto = 219038123;

		final Object testingData[][] = {
			{
				"admin", company1.getId(), null
			}, {
				"admin", idCompanyIncorrecto, IllegalArgumentException.class
			}, {
				"candidate1", 0, IllegalArgumentException.class
			}
		};

		for (int i = 0; i < testingData.length; i++)
			template1((String) testingData[i][0], (Integer) testingData[i][1], (Class<?>) testingData[i][2]);
	}
	protected void template1(String username, Integer companyID, final Class<?> expected) {
		Class<?> caught;

		caught = null;
		try {
			authenticate(username);
			Company company = companyService.banCompany(companyID);
			System.out.println("#applyOffer");
			Assert.isTrue(company.getBanned() == true);
			unauthenticate();
		} catch (Throwable oops) {
			caught = oops.getClass();
		}

		checkExceptions(expected, caught);
	}

	//////////////////////////////////////////////******************************************//////////////////////////////////////////////////////

	/**
	 * An actor who is authenticated as an administrator must be able to:
	 * Ban or unban a company if he or she thinks that their offers are inappropriate.
	 * Banning a company means that their offers are not displayed by the system,
	 * but the ac-tor that represents it can login and perform any operation with the system.
	 * 
	 * Test correspondiente a deshacer el baneo de una compa��a
	 * 
	 * Test positivo logado como admin y una company correcta
	 * Test negativo logado como admin y una company incorrecta
	 * Test negativo logado como candidate
	 */
	@Test
	public void unbanCompany() {

		/*
		 * Preparaci�n test positivo.
		 * 
		 * Obtenemos compa��a
		 */
		List<Company> companies = (List<Company>) companyService.findAll();
		Assert.isTrue(companies != null && !companies.isEmpty(), "No existen compa��as");
		Company company1 = companies.get(0);

		/*
		 * Preparaci�n test negativo.
		 * 
		 * Obtenemos un id de company incorrecto
		 */
		int idCompanyIncorrecto = 219038123;

		final Object testingData[][] = {
			{
				"admin", company1.getId(), null
			}, {
				"admin", idCompanyIncorrecto, IllegalArgumentException.class
			}, {
				"candidate1", 0, IllegalArgumentException.class
			}
		};

		for (int i = 0; i < testingData.length; i++)
			template2((String) testingData[i][0], (Integer) testingData[i][1], (Class<?>) testingData[i][2]);
	}
	protected void template2(String username, Integer companyID, final Class<?> expected) {
		Class<?> caught;

		caught = null;
		try {
			authenticate(username);
			Company company = companyService.unbanCompany(companyID);
			System.out.println("#applyOffer");
			Assert.isTrue(company.getBanned() == false);
			unauthenticate();
		} catch (Throwable oops) {
			caught = oops.getClass();
		}

		checkExceptions(expected, caught);
	}

}
