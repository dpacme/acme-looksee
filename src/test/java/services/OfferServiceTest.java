
package services;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;

import javax.transaction.Transactional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.util.Assert;

import domain.Offer;
import utilities.AbstractTest;

@Transactional
@ContextConfiguration(locations = {
	"classpath:spring/junit.xml"
})
@RunWith(SpringJUnit4ClassRunner.class)
public class OfferServiceTest extends AbstractTest {

	// Services and repositories
	@Autowired
	private OfferService	offerService;

	@Autowired
	private CompanyService	companyService;

	// Templates --------------------------------------------------------------


	/*
	 * An actor who is not authenticated/ authenticated must be able to: Search for offers that contain a
	 * single key word in their title or description, provide a given salary (in a given currency), and are still open.
	 *
	 * En este caso de uso se realizar� la acci�n de buscar las ofertas atendiendo a palabras claves y rangos de salario. Todos los actores
	 * pueden acceder a ello, no existen restricciones en la palabra clave ni en los rangos salariales y no participa
	 * ninguna id por lo que no existen situaciones que puedan forzar el error.
	 *
	 */
	public void searchOffer(final String username, final String keyword, final Double minSalary, final Double maxSalary, final Class<?> expected) {

		Class<?> caught = null;

		try {

			this.authenticate(username);

			Collection<Offer> foundOffers = new ArrayList<>();
			final Collection<Offer> res;

			foundOffers = this.offerService.searchOffer(keyword, minSalary, maxSalary);
			res = this.offerService.filter(foundOffers);

			this.unauthenticate();

		} catch (final Throwable oops) {

			caught = oops.getClass();

		}

		this.checkExceptions(expected, caught);
	}

	/*
	 * An actor who is authenticated as a company must be able to: Publish an offer. Writing an offer may require some time,
	 * so the system must allow to write as many drafts as necessary before an offer is finally published.
	 * For an offer to be published, its deadline must be at least one week ahead.
	 *
	 * En este caso de uso se realizara la acci�n de publicar una oferta. Para forzar el fallo se pueden dar los siguientes casos:
	 *
	 * - El usuario logueado no es una compa�ia
	 * - El id de la oferta no existe
	 * - La oferta ya est� publicada
	 * - La fecha limite no es superior en una semana a la actual
	 * - La oferta no pertenece a la compa�ia logueada
	 *
	 * ESTE CASO DE PRUEBA PUEDE FALLAR M�S ADELANTE PUESTO QUE EN EL MOMENTO EN EL QUE SE HIZO EL TEST LAS FECHAS ERAN LAS CORRECTAS
	 */
	public void publishOffer(final String username, final int offerID, final Class<?> expected) {

		Class<?> caught = null;

		try {

			this.authenticate(username);
			this.companyService.checkIfCompany();

			final Offer offer = this.offerService.findOne(offerID);
			this.offerService.publish(offer);

			this.unauthenticate();

		} catch (final Throwable oops) {

			caught = oops.getClass();

		}

		this.checkExceptions(expected, caught);
	}

	/*
	 * An actor who is authenticated as a company must be able to: Publish an offer. Writing an offer may require some time,
	 * so the system must allow to write as many drafts as necessary before an offer is finally published.
	 * For an offer to be published, its deadline must be at least one week ahead.
	 *
	 * En este caso de uso se realizara la acci�n crear el borrador de una oferta. Para forzar el fallo se pueden dar los siguientes casos:
	 *
	 * - El usuario logueado no es una compa�ia
	 * - El salario m�nimo es mayor que el m�ximo
	 * - Los campos no son correctos
	 *
	 * ESTE CASO DE PRUEBA PUEDE FALLAR M�S ADELANTE PUESTO QUE EN EL MOMENTO EN EL QUE SE HIZO EL TEST LAS FECHAS ERAN LAS CORRECTAS
	 */
	public void createOffer(final String username, final String title, final String description, final Double minSalary, final Double maxSalary, final String currency, final String deadline, final Class<?> expected) {

		Class<?> caught = null;

		try {

			this.authenticate(username);
			this.companyService.checkIfCompany();

			final Offer offer = this.offerService.create();
			offer.setTitle(title);
			offer.setDescription(description);
			offer.setMinSalary(minSalary);
			offer.setMaxSalary(maxSalary);
			offer.setCurrency(currency);
			offer.setDeadLine(new Date(deadline));

			this.offerService.save(offer);

			this.unauthenticate();

		} catch (final Throwable oops) {

			caught = oops.getClass();

		}

		this.checkExceptions(expected, caught);
	}

	/*
	 * An actor who is authenticated as a company must be able to: Publish an offer. Writing an offer may require some time,
	 * so the system must allow to write as many drafts as necessary before an offer is finally published.
	 * For an offer to be published, its deadline must be at least one week ahead.
	 *
	 * En este caso de uso se realizara la acci�n editar el borrador de una oferta. Para forzar el fallo se pueden dar los siguientes casos:
	 *
	 * - El usuario logueado no es una compa�ia
	 * - El salario m�nimo es mayor que el m�ximo
	 * - Los campos no son correctos
	 * - El id de la oferta no existe
	 * - La oferta no pertenece a la compa�ia logueada
	 *
	 * ESTE CASO DE PRUEBA PUEDE FALLAR M�S ADELANTE PUESTO QUE EN EL MOMENTO EN EL QUE SE HIZO EL TEST LAS FECHAS ERAN LAS CORRECTAS
	 */
	public void editOffer(final String username, final int offerID, final String title, final String description, final Double minSalary, final Double maxSalary, final String currency, final String deadline, final Class<?> expected) {

		Class<?> caught = null;

		try {

			this.authenticate(username);
			this.companyService.checkIfCompany();

			final Offer offer = this.offerService.findOne(offerID);

			Assert.isTrue(offer.getDraft());
			Assert.isTrue(offer.getCompany().getId() == this.companyService.findByPrincipal().getId());

			offer.setTitle(title);
			offer.setDescription(description);
			offer.setMinSalary(minSalary);
			offer.setMaxSalary(maxSalary);
			offer.setCurrency(currency);
			offer.setDeadLine(new Date(deadline));

			this.offerService.save(offer);

			this.unauthenticate();

		} catch (final Throwable oops) {

			caught = oops.getClass();

		}

		this.checkExceptions(expected, caught);
	}

	/*
	 * Listar ofertas - filtradas para que no sean borradores o de una compa�ia baneada
	 *
	 * Probamos a pasarle como parametro: una lista vacia, una lista de ofertas y null.
	 * No proceden tests negativos.
	 *
	 */
	@Test
	public void filterOffers() {

		final Collection<Offer> offersFree = new ArrayList<>();

		final Collection<Offer> offersTest = this.offerService.filter(this.offerService.findAll());

		final Collection<Offer> offersTest2 = this.offerService.filter(offersFree);

		final Collection<Offer> offersTest3 = this.offerService.filter(null);

		for (final Offer offer : offersTest)
			Assert.isTrue((!offer.getDraft() && !offer.getCompany().getBanned()));

		for (final Offer offer : offersTest2)
			Assert.isTrue((!offer.getDraft() && !offer.getCompany().getBanned()));

		for (final Offer offer : offersTest3)
			Assert.isTrue((!offer.getDraft() && !offer.getCompany().getBanned()));

	}

	// Drivers ----------------------------------------------------------------

	@Test
	public void searchOfferDriver() {

		final Object testingData[][] = {
			// B�squeda como no autentificado y solo por el campo keyword-> true
			{
				null, "Title offer 1", null, null, null
			},
			// B�squeda como admin y completando todos lo campos -> true
			{
				"admin", "Description", 200.0, 500.0, null
			},
			// B�squeda como candidate y buscando solo por rango -> true
			{
				"candidate1", "", 200.0, 500.0, null
			},
			// B�squeda como company e informando todos los campos -> true
			{
				"company1", "title", 200.0, 500.0, null
			}
		};
		for (int i = 0; i < testingData.length; i++)
			this.searchOffer((String) testingData[i][0], (String) testingData[i][1], (Double) testingData[i][2], (Double) testingData[i][3], (Class<?>) testingData[i][4]);
	}

	@Test
	public void publishOfferDriver() {

		final Object testingData[][] = {
			// Publicar una oferta sin autentificarse-> false
			{
				null, 979, IllegalArgumentException.class
			},
			// Publicar una oferta logueado como candidate -> false
			{
				"candidate1", 979, IllegalArgumentException.class
			},
			// El id de la oferta no existe -> false
			{
				"company2", 6426984, IllegalArgumentException.class
			},
			// Publicar una oferta ya publicada -> false
			{
				"company1", 978, IllegalArgumentException.class
			},
			// Publicar una oferta cuya fecha limite no es superior en una semana a la actual -> false
			{
				"company3", 981, IllegalArgumentException.class
			},
			// Publicar una oferta que no pertenece a la compa�ia logueada -> false
			{
				"company1", 979, IllegalArgumentException.class
			},
			// Publicar una oferta correctamente -> false
			{
				"company2", 979, null
			}
		};
		for (int i = 0; i < testingData.length; i++)
			this.publishOffer((String) testingData[i][0], (int) testingData[i][1], (Class<?>) testingData[i][2]);
	}

	@Test
	public void createOfferDriver() {

		final Object testingData[][] = {
			// Crear una oferta sin autentificarse-> false
			{
				null, "T�tulo", "Descripci�n", 200.0, 500.0, "EUR", "07/15/2017 19:00", IllegalArgumentException.class
			},
			// Crear una oferta logueado como candidate -> false
			{
				"candidate1", "T�tulo", "Descripci�n", 200.0, 500.0, "EUR", "07/15/2017 19:00", IllegalArgumentException.class
			},
			// Crear una oferta con un salario m�nimo mayor que el m�ximo
			{
				"company1", "T�tulo", "Descripci�n", 500.0, 300.0, "EUR", "07/15/2017 19:00", IllegalArgumentException.class
			},
			// Crear una oferta con alg�n campo vacio -> false
			{
				"company1", "T�tulo", "", 200.0, 500.0, "EUR", "07/15/2017 19:00", IllegalArgumentException.class
			},
			// Crear una oferta correctamente -> true
			{
				"company1", "T�tulo", "Descripci�n", 200.0, 500.0, "EUR", "07/15/2017 19:00", null
			}
		};
		for (int i = 0; i < testingData.length; i++)
			this.createOffer((String) testingData[i][0], (String) testingData[i][1], (String) testingData[i][2], (Double) testingData[i][3], (Double) testingData[i][4], (String) testingData[i][5], (String) testingData[i][6], (Class<?>) testingData[i][7]);
	}

	@Test
	public void editOfferDriver() {

		final Object testingData[][] = {
			// Editar una oferta sin autentificarse-> false
			{
				null, 979, "T�tulo", "Descripci�n", 200.0, 500.0, "EUR", "07/15/2017 19:00", IllegalArgumentException.class
			},
			// Editar una oferta logueado como candidate -> false
			{
				"candidate1", 979, "T�tulo", "Descripci�n", 200.0, 500.0, "EUR", "07/15/2017 19:00", IllegalArgumentException.class
			},
			// Editar una oferta con un salario m�nimo mayor que el m�ximo
			{
				"company3", 979, "T�tulo", "Descripci�n", 500.0, 300.0, "EUR", "07/15/2017 19:00", IllegalArgumentException.class
			},
			// Editar una oferta con alg�n campo vacio -> false
			{
				"company3", 795, "T�tulo", "", 200.0, 500.0, "EUR", "07/15/2017 19:00", IllegalArgumentException.class
			},
			// Editar una oferta que no pertenece a la compa�ia logueada -> false
			{
				"company1", 979, "T�tulo", "Descripci�n", 200.0, 500.0, "EUR", "07/15/2017 19:00", IllegalArgumentException.class
			},
			// La id de la oferta no existe -> false
			{
				"company3", 6444567, "T�tulo", "Descripci�n", 200.0, 500.0, "EUR", "07/15/2017 19:00", IllegalArgumentException.class
			},
			// Editar una oferta correctamente -> true
			{
				"company2", 979, "T�tulo", "Descripci�n", 200.0, 500.0, "EUR", "08/27/2017 19:00", null
			}
		};
		for (int i = 0; i < testingData.length; i++)
			this.editOffer((String) testingData[i][0], (int) testingData[i][1], (String) testingData[i][2], (String) testingData[i][3], (Double) testingData[i][4], (Double) testingData[i][5], (String) testingData[i][6], (String) testingData[i][7],
				(Class<?>) testingData[i][8]);
	}

}
