
package services;

import java.util.Collection;
import java.util.List;

import javax.transaction.Transactional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.util.Assert;

import domain.Candidate;
import domain.Curricula;
import domain.EndorserRecord;
import utilities.AbstractTest;

@Transactional
@ContextConfiguration(locations = {
	"classpath:spring/junit.xml"
})
@RunWith(SpringJUnit4ClassRunner.class)
public class EndorserRecordServiceTest extends AbstractTest {

	// Services and repositories
	@Autowired
	private CurriculaService		curriculaService;

	@Autowired
	private EndorserRecordService	endorserRecordService;

	@Autowired
	private CandidateService		candidateService;

	// Templates --------------------------------------------------------------


	// An actor who is authenticated as a candidate must be able to: Edit his or her curricula.
	// Comprobamos que los curricula se editan correctamente y para los tests negativos vemos si salta la excepci�n correcta si se le pasan par�metros incorrectos.
	// Test positivo y 2 tests negativosTest positivo y 2 tests negativos

	protected void template2(final String username, final Integer curriculaId, final Class<?> expected) {
		Class<?> caught;

		caught = null;
		try {
			this.authenticate(username);
			System.out.println("#editCurricula");
			final Curricula curricula = this.curriculaService.findOne(curriculaId);
			final Collection<EndorserRecord> endorserRecords = curricula.getEndorserRecords();
			Integer i = 1;
			for (final EndorserRecord o : endorserRecords) {
				o.setComments("Comentario random" + i);
				i++;
				System.out.println(o.getFullName());
				final EndorserRecord endorserRecordSaved = this.endorserRecordService.saveAndFlush(o);
				Assert.isTrue(endorserRecordSaved != null && endorserRecordSaved.getId() != 0);
				System.out.println(endorserRecordSaved.getComments());
			}

			this.unauthenticate();
		} catch (final Throwable oops) {
			caught = oops.getClass();
		}

		this.checkExceptions(expected, caught);
	}

	// An actor who is authenticated as a candidate must be able to: List his or her curricula.
	// Comprobamos que los curricula se listan correctamente y para los tests negativos vemos si salta la excepci�n correcta si se le pasan par�metros incorrectos.
	// Test positivo y 2 tests negativos
	protected void template3(final String username, final Integer curriculaId, final Class<?> expected) {
		Class<?> caught;

		caught = null;
		try {
			this.authenticate(username);
			final Curricula curricula = this.curriculaService.findOne(curriculaId);
			System.out.println("#listCurriculaByCandidate");
			Assert.isTrue(curricula.getEndorserRecords() != null && !curricula.getEndorserRecords().isEmpty());
			for (final EndorserRecord o : curricula.getEndorserRecords())
				System.out.println(o.getFullName());

			this.unauthenticate();
		} catch (final Throwable oops) {
			caught = oops.getClass();
		}

		this.checkExceptions(expected, caught);
	}

	/*
	 * An actor who is authenticated as a candidate must be able to: Edit an existing curriculum.
	 *
	 * En este caso se realizara la acci�n de crear una referencia en una curricula ya existente. Para forzar el fallo se pueden dar los siguientes casos
	 * Para forzar el fallo se pueden dar los siguientes casos:
	 *
	 * - El usuario logueado no es un candidate
	 * - La curricula no pertenece al usuario logueado
	 * - La curricula no existe
	 * - Los campos no son correctos
	 *
	 */

	public void create(final String username, final int curriculaID, final String fullname, final String email, final String phone, final String linkedInUrl, final String comments, final Class<?> expected) {

		Class<?> caught = null;

		try {

			this.authenticate(username);

			this.candidateService.checkIfCandidate();

			final Curricula curricula = this.curriculaService.findOne(curriculaID);
			Assert.isTrue(this.candidateService.findByPrincipal().getId() == curricula.getCandidate().getId());
			final EndorserRecord endorserRecord = this.endorserRecordService.create(curricula);
			endorserRecord.setFullName(fullname);
			endorserRecord.setEmail(email);
			endorserRecord.setPhone(phone);
			endorserRecord.setLinkedInUrl(linkedInUrl);
			endorserRecord.setComments(comments);
			this.endorserRecordService.comprobacion(endorserRecord);
			this.endorserRecordService.save(endorserRecord);

			this.unauthenticate();

		} catch (final Throwable oops) {

			caught = oops.getClass();

		}

		this.checkExceptions(expected, caught);
	}

	/*
	 * An actor who is authenticated as a candidate must be able to: Edit an existing curriculum.
	 *
	 * En este caso se realizara la acci�n de borrar una referencia en una curricula ya existente. Para forzar el fallo se pueden dar los siguientes casos
	 * Para forzar el fallo se pueden dar los siguientes casos:
	 *
	 * - El usuario logueado no es un candidate
	 * - La curricula no pertenece al usuario logueado
	 * - La curricula no existe
	 * - El endorser record no existe
	 * - El endorser record no pertenece a la curricula
	 *
	 */
	public void delete(final String username, final int curriculaID, final int endorserRecordID, final Class<?> expected) {

		Class<?> caught = null;

		try {

			this.authenticate(username);

			this.candidateService.checkIfCandidate();

			final Curricula curricula = this.curriculaService.findOne(curriculaID);
			final EndorserRecord endorserRecord = this.endorserRecordService.findOne(endorserRecordID);
			Assert.isTrue(this.candidateService.findByPrincipal().getId() == curricula.getCandidate().getId());
			Assert.isTrue(curricula.getEndorserRecords().contains(endorserRecord));

			this.endorserRecordService.delete(endorserRecord);

			this.unauthenticate();

		} catch (final Throwable oops) {

			caught = oops.getClass();

		}

		this.checkExceptions(expected, caught);
	}

	// Drivers ----------------------------------------------------------------

	@Test
	public void createDriver() {

		final Object testingData[][] = {
			// Crear endorser record como no autentificado -> false
			{
				null, 987, "Full name test", "email@dominio.com", "666666666", "http://www.linkedinurl.com", "Comments test", IllegalArgumentException.class
			},
			// Crear endorser record como admin -> false
			{
				"admin", 987, "Full name test", "email@dominio.com", "666666666", "http://www.linkedinurl.com", "Comments test", IllegalArgumentException.class
			},
			// Crear endorser record como company -> false
			{
				"company1", 987, "Full name test", "email@dominio.com", "666666666", "http://www.linkedinurl.com", "Comments test", IllegalArgumentException.class
			},
			// La curricula no pertenece al usuario logueado-> false
			{
				"candidate3", 987, "Full name test", "email@dominio.com", "666666666", "http://www.linkedinurl.com", "Comments test", IllegalArgumentException.class
			},
			// La curricula no existe -> false
			{
				"candidate1", 645645, "Full name test", "email@dominio.com", "666666666", "http://www.linkedinurl.com", "Comments test", IllegalArgumentException.class
			},
			// Los campos no son correctos -> false
			{
				"candidate1", 987, "", "", "666666666", "http://www.linkedinurl.com", "Comments test", IllegalArgumentException.class
			},
			// Crear correctamente -> true
			{
				"candidate1", 987, "Full name test", "email@dominio.com", "666666666", "http://www.linkedinurl.com", "Comments test", null
			}

		};
		for (int i = 0; i < testingData.length; i++)
			this.create((String) testingData[i][0], (int) testingData[i][1], (String) testingData[i][2], (String) testingData[i][3], (String) testingData[i][4], (String) testingData[i][5], (String) testingData[i][6], (Class<?>) testingData[i][7]);
	}

	@Test
	public void deleteDriver() {

		final Object testingData[][] = {
			// Delete endorser record como no autentificado -> false
			{
				null, 987, 1003, IllegalArgumentException.class
			},
			// Delete endorser record como admin -> false
			{
				"admin", 987, 1003, IllegalArgumentException.class
			},
			// Delete endorser record como company -> false
			{
				"company1", 987, 1003, IllegalArgumentException.class
			},
			// La curricula no pertenece al usuario logueado-> false
			{
				"candidate3", 987, 1003, IllegalArgumentException.class
			},
			// La curricula no existe -> false
			{
				"candidate1", 645645, 1003, IllegalArgumentException.class
			},
			// El endorser record no existe -> false
			{
				"candidate1", 987, 657657, IllegalArgumentException.class
			},
			// El endorser record no pertenece a la curricula -> false
			{
				"candidate1", 987, 1007, IllegalArgumentException.class
			},
			// Borrar endorser record -> true
			{
				"candidate1", 987, 1003, null
			}

		};
		for (int i = 0; i < testingData.length; i++)
			this.delete((String) testingData[i][0], (int) testingData[i][1], (int) testingData[i][2], (Class<?>) testingData[i][3]);
	}

	@Test
	public void listEndorserRecordByCurricula() {

		this.authenticate("candidate1");
		final Candidate candidate = this.candidateService.findByPrincipal();
		final List<Curricula> curriculas = (List<Curricula>) candidate.getCurriculas();
		final Object testingData[][] = {
			{
				"candidate1", curriculas.get(0).getId(), null
			}, {
				"candidate1", null, NullPointerException.class
			}, {
				"candidate1", 0, IllegalArgumentException.class
			}
		};

		for (int i = 0; i < testingData.length; i++)
			this.template3((String) testingData[i][0], (Integer) testingData[i][1], (Class<?>) testingData[i][2]);
	}

	@Test
	public void editCurriculaDriver() {

		this.authenticate("candidate1");
		final Candidate candidate = this.candidateService.findByPrincipal();
		final List<Curricula> curriculas = (List<Curricula>) candidate.getCurriculas();

		final Object testingData[][] = {
			{
				"candidate1", curriculas.get(0).getId(), null
			}, {
				"candidate1", null, NullPointerException.class
			}, {
				"candidate1", 0, IllegalArgumentException.class
			}
		};

		for (int i = 0; i < testingData.length; i++)
			this.template2((String) testingData[i][0], (Integer) testingData[i][1], (Class<?>) testingData[i][2]);
	}
}
