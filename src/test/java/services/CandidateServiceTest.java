
package services;

import java.util.List;

import javax.transaction.Transactional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.util.Assert;

import domain.Candidate;
import forms.ActorForm;
import utilities.AbstractTest;

@Transactional
@ContextConfiguration(locations = {
	"classpath:spring/junit.xml"
})
@RunWith(SpringJUnit4ClassRunner.class)
public class CandidateServiceTest extends AbstractTest {

	// Services and repositories
	@Autowired
	private CandidateService candidateService;

	// Templates --------------------------------------------------------------


	/*
	 * An actor who is not authenticated must be able to:
	 * Register in the system as an candidate.
	 *
	 * En este caso de uso se llevara a cabo el registro de un candidate en el sistema
	 * Para forzar el error pueden darse varios casos:
	 *
	 * � El usuario esta autentificado
	 * � Atributos del registro incorrectos
	 * � No aceptar las condiciones
	 * � Nombre de usuario ya existente
	 * � Contrase�as no coinciden
	 */
	public void registerCandidate(final String username, final String name, final String surname, final String email, final String phone, final String postalAddress, final String newUsername, final String password, final String secondPassword,
		final Boolean checkBox, final Class<?> expected) {

		Class<?> caught = null;

		try {

			this.authenticate(username);

			// Comprobamos que no este autentificado
			Assert.isTrue(username == null);

			// Inicializamos los atributos para la creaci�n
			final ActorForm actor = new ActorForm();

			actor.setName(name);
			actor.setSurname(surname);
			actor.setEmail(email);
			actor.setPhone(phone);
			actor.setPostalAddress(postalAddress);

			actor.setUsername(newUsername);
			actor.setPassword(password);
			actor.setSecondPassword(secondPassword);

			actor.setCheckBox(checkBox);

			//Reconsturimos
			final Candidate candidate = this.candidateService.reconstruct(actor);

			//Comprobamos atributos
			this.candidateService.comprobacion(candidate);

			//Guardamos
			this.candidateService.saveForm(candidate);

			this.unauthenticate();

		} catch (final Throwable oops) {

			caught = oops.getClass();

		}

		this.checkExceptions(expected, caught);
	}
	
	// An actor who is authenticated must be able to: Edit his or her personal data.
	// Comprobamos que se puede editar un candidate correctamente y para los tests negativos vemos si salta la excepci�n correcta si pasamos parametros nulos o incorrectos.
	// Test positivo y 2 tests negativos

	protected void template2(String username, Integer candidateId, final Class<?> expected) {
		Class<?> caught;

		caught = null;
		try {
			this.authenticate(username);
			System.out.println("#editCandidate");
			Candidate candidate = this.candidateService.findOne(candidateId);
			candidate.setName("Name Modified");
			candidate.setEmail("EmailModified@GMAIL.COM");
			Candidate candidateSaved = this.candidateService.saveAndFlush(candidate);
			Assert.isTrue(candidateSaved != null && candidateSaved.getId() != 0);

			System.out.println(candidateSaved.getName() + "-" + candidateSaved.getEmail());

			this.unauthenticate();
		} catch (Throwable oops) {
			caught = oops.getClass();
		}

		this.checkExceptions(expected, caught);
	}


	// Drivers ----------------------------------------------------------------------
	
	@Test
	public void editCandidateDriver() {

		List<Candidate> candidates = (List<Candidate>) this.candidateService.findAll();
		final Object testingData[][] = {
			{
				"candidate1", candidates.get(0).getId(), null
			}, {
				"candidate1", null, NullPointerException.class
			}, {
				"candidate1", 0, IllegalArgumentException.class
			}
		};

		for (int i = 0; i < testingData.length; i++)
			this.template2((String) testingData[i][0], (Integer) testingData[i][1], (Class<?>) testingData[i][2]);
	}
	
	@Test
	public void registerCandidateDriver() {

		final Object testingData[][] = {
			// Creaci�n de candidate como autentificado (1) -> false
			{
				"admin", "NameTest1", "SurnameTest1", "email@domain.com", "678432123", "41010", "username1", "password1", "password1", true, IllegalArgumentException.class
			},
			// Creaci�n de candidate como autentificado (2) -> false
			{
				"candidate1", "NameTest1", "SurnameTest1", "email@domain.com", "678432123", "41010", "username2", "password1", "password1", true, IllegalArgumentException.class
			},
			// Creaci�n de candidate como autentificado (3) -> false
			{
				"company1", "NameTest1", "SurnameTest1", "email@domain.com", "678432123", "41010", "username3", "password1", "password1", true, IllegalArgumentException.class
			},
			// Creaci�n de candidate con postalAddress incorrecto -> false
			{
				null, "NameTest1", "SurnameTest1", "email@domain.com", "678432123", "56118916511", "username4", "password1", "password1", true, IllegalArgumentException.class
			},
			// Creaci�n de candidate sin aceptar t�rminos -> false
			{
				null, "NameTest1", "SurnameTest1", "email@domain.com", "678432123", "41010", "username5", "password1", "password1", false, IllegalArgumentException.class
			},
			// Creaci�n de candidate con usuario no �nico -> false
			{
				null, "NameTest1", "SurnameTest1", "email@domain.com", "678432123", "41010", "candidate1", "password1", "password1", true, IllegalArgumentException.class
			},
			// Creaci�n de candidate con contrase�as no coincidentes -> false
			{
				null, "NameTest1", "SurnameTest1", "email@domain.com", "678432123", "41010", "username7", "password1", "password2", true, IllegalArgumentException.class
			},
			// Creaci�n de candidate con todo correcto -> true
			{
				null, "NameTest1", "SurnameTest1", "email@domain.com", "678432123", "41010", "username8", "password1", "password1", true, null
			},
			// Creaci�n de candidate con codigopostal vacio -> true
			{
				null, "NameTest1", "SurnameTest1", "email@domain.com", "678432123", "", "username9", "password1", "password1", true, null
			},
			// Creaci�n de candidate con tel�fono incorrecto -> true
			{
				null, "NameTest1", "SurnameTest1", "email@domain.com", "678432123", "41010", "username10", "password1", "password1", true, null
			}
		};
		for (int i = 0; i < testingData.length; i++)
			this.registerCandidate((String) testingData[i][0], (String) testingData[i][1], (String) testingData[i][2], (String) testingData[i][3], (String) testingData[i][4], (String) testingData[i][5], (String) testingData[i][6],
				(String) testingData[i][7], (String) testingData[i][8], (Boolean) testingData[i][9], (Class<?>) testingData[i][10]);
	}
}
