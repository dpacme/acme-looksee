
package services;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.transaction.Transactional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.util.Assert;

import domain.Candidate;
import domain.Curricula;
import domain.PersonalRecord;
import utilities.AbstractTest;

@Transactional
@ContextConfiguration(locations = {
	"classpath:spring/junit.xml"
})
@RunWith(SpringJUnit4ClassRunner.class)
public class CurriculaServiceTest extends AbstractTest {

	// Services and repositories
	@Autowired
	private CurriculaService		curriculaService;

	@Autowired
	private CandidateService		candidateService;

	@Autowired
	private PersonalRecordService	personalRecordService;

	// Templates --------------------------------------------------------------


	// An actor who is authenticated as a candidate must be able to: List his or her curricula.
	// Comprobamos que los curricula se listan correctamente y para los tests negativos vemos si salta la excepci�n correcta si se le pasan par�metros incorrectos.
	// Test positivo y 2 tests negativos
	protected void template3(final String username, final Integer candidateId, final Class<?> expected) {
		Class<?> caught;

		caught = null;
		try {
			this.authenticate(username);
			final Candidate candidate = this.candidateService.findOne(candidateId);
			System.out.println("#listCurriculaByCandidate");
			Assert.isTrue(candidate.getCurriculas() != null && !candidate.getCurriculas().isEmpty());
			for (final Curricula o : candidate.getCurriculas())
				System.out.println(o.getTicker());

			this.unauthenticate();
		} catch (final Throwable oops) {
			caught = oops.getClass();
		}

		this.checkExceptions(expected, caught);
	}

	/*
	 * An actor who is authenticated as a candidate must be able to: Search for specific curricula using a single key word that can appear in any record.
	 *
	 * En este caso de uso se llevara a cabo la acci�n de buscar una curricula cuyos record contengan la palabra clave introducida por el usuario.
	 * Para forzar el fallo se pueden dar los siguientes casos:
	 *
	 * - El usuario logueado no es un candidate
	 *
	 */
	public void searchCurricula(final String username, final String keyword, final Class<?> expected) {

		Class<?> caught = null;

		try {

			this.authenticate(username);

			this.candidateService.checkIfCandidate();

			final Collection<Curricula> res = this.curriculaService.searchCurricula(keyword);

			this.unauthenticate();

		} catch (final Throwable oops) {

			caught = oops.getClass();

		}

		this.checkExceptions(expected, caught);
	}

	/*
	 * An actor who is authenticated as a candidate must be able to: Edit an existing curriculum.
	 *
	 * En este caso se realizara la acci�n de crear una curricula. Para forzar el fallo se pueden dar los siguientes casos
	 * Para forzar el fallo se pueden dar los siguientes casos:
	 *
	 * - El usuario logueado no es un candidate
	 * - Los campos del personal record no son correctos
	 *
	 */
	public void create(final String username, final String fullname, final String picture, final String email, final String phone, final String linkedInUrl, final Class<?> expected) {

		Class<?> caught = null;

		try {

			this.authenticate(username);

			this.candidateService.checkIfCandidate();

			final Curricula curricula = this.curriculaService.create();

			if (curricula.getPersonalRecord() != null)
				this.curriculaService.save(curricula);
			else {
				final PersonalRecord personalRecord = this.personalRecordService.create();
				personalRecord.setCurriculas(new ArrayList<Curricula>());
				personalRecord.setFullName(fullname);
				personalRecord.setPicture(picture);
				personalRecord.setEmail(email);
				personalRecord.setPhone(phone);
				personalRecord.setLinkedInUrl(linkedInUrl);

				personalRecord.getCurriculas().add(curricula);
				this.personalRecordService.comprobacion(personalRecord);
				final PersonalRecord aux = this.personalRecordService.save(personalRecord);
				curricula.setPersonalRecord(aux);
				this.curriculaService.save(curricula);
			}

			this.unauthenticate();

		} catch (final Throwable oops) {

			caught = oops.getClass();

		}

		this.checkExceptions(expected, caught);
	}

	/*
	 * An actor who is authenticated as a candidate must be able to: Edit an existing curriculum.
	 *
	 * En este caso se realizara la acci�n de borrar una curricula. Para forzar el fallo se pueden dar los siguientes casos
	 * Para forzar el fallo se pueden dar los siguientes casos:
	 *
	 * - El usuario logueado no es un candidate
	 * - La curricula no pertenece al usuario logueado
	 * - La curricula no existe
	 *
	 */
	public void delete(final String username, final int curriculaID, final Class<?> expected) {

		Class<?> caught = null;

		try {

			this.authenticate(username);

			this.candidateService.checkIfCandidate();

			final Curricula curricula = this.curriculaService.findOne(curriculaID);
			Assert.isTrue(this.candidateService.findByPrincipal().getId() == curricula.getCandidate().getId());

			this.curriculaService.delete(curricula);

			this.unauthenticate();

		} catch (final Throwable oops) {

			caught = oops.getClass();

		}

		this.checkExceptions(expected, caught);
	}

	// Drivers ----------------------------------------------------------------

	@Test
	public void searchCurriculaDriver() {

		final Object testingData[][] = {
			// B�squeda como no autentificado -> false
			{
				null, "Comments education record 1", IllegalArgumentException.class
			},
			// B�squeda como admin -> false
			{
				"admin", "Comments education record 1", IllegalArgumentException.class
			},
			// B�squeda como company -> false
			{
				"company1", "Comments education record 1", IllegalArgumentException.class
			},
			// B�squeda como candidate-> true
			{
				"candidate1", "Comments education record 1", null
			}
		};
		for (int i = 0; i < testingData.length; i++)
			this.searchCurricula((String) testingData[i][0], (String) testingData[i][1], (Class<?>) testingData[i][2]);
	}

	@Test
	public void createDriver() {

		final Object testingData[][] = {
			// Crear curricula como no autentificado -> false
			{
				null, "Full name create test", "http://www.imagen.com", "email@dominio.com", "+34 666666666", "http://www.linkedinurl.com", IllegalArgumentException.class
			},
			// Crear curricula como admin -> false
			{
				"admin", "Full name create test", "http://www.imagen.com", "email@dominio.com", "+34 666666666", "http://www.linkedinurl.com", IllegalArgumentException.class
			},
			// Crear curricula como company -> false
			{
				"company1", "Full name create test", "http://www.imagen.com", "email@dominio.com", "+34 666666666", "http://www.linkedinurl.com", IllegalArgumentException.class
			},
			// Crear curricula como candidate y campos incorrectos-> false
			{
				"candidate3", "", "http://www.imagen.com", "", "+34 666666666", "http://www.linkedinurl.com", IllegalArgumentException.class
			},
			// Crear curricula como candidate y campos correctos -> true
			{
				"candidate1", "Full name create test", "http://www.imagen.com", "email@dominio.com", "+34 666666666", "http://www.linkedinurl.com", null
			}

		};
		for (int i = 0; i < testingData.length; i++)
			this.create((String) testingData[i][0], (String) testingData[i][1], (String) testingData[i][2], (String) testingData[i][3], (String) testingData[i][4], (String) testingData[i][5], (Class<?>) testingData[i][6]);
	}

	@Test
	public void deleteDriver() {

		final Object testingData[][] = {
			// Borrar curricula como no autentificado -> false
			{
				null, 987, IllegalArgumentException.class
			},
			// Borrar curricula como admin -> false
			{
				"admin", 987, IllegalArgumentException.class
			},
			// Borrar curricula como company -> false
			{
				"company1", 987, IllegalArgumentException.class
			},
			// Borra curricula que no pertenece al usuario logueado-> false
			{
				"candidate1", 645645, IllegalArgumentException.class
			},
			// Borrar curricula correctamente -> true
			{
				"candidate1", 987, null
			}

		};
		for (int i = 0; i < testingData.length; i++)
			this.delete((String) testingData[i][0], (int) testingData[i][1], (Class<?>) testingData[i][2]);
	}

	@Test
	public void listCurriculaByCandidate() {

		final List<Candidate> candidates = (List<Candidate>) this.candidateService.findAll();
		final Object testingData[][] = {
			{
				"candidate1", candidates.get(0).getId(), null
			}, {
				"candidate1", null, NullPointerException.class
			}, {
				"candidate1", 0, IllegalArgumentException.class
			}
		};

		for (int i = 0; i < testingData.length; i++)
			this.template3((String) testingData[i][0], (Integer) testingData[i][1], (Class<?>) testingData[i][2]);
	}
}
