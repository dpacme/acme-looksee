
package services;

import java.util.Collection;
import java.util.Date;
import java.util.List;

import javax.transaction.Transactional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.util.Assert;

import domain.Candidate;
import domain.Curricula;
import domain.EducationRecord;
import domain.PersonalRecord;
import domain.ProfessionalRecord;
import utilities.AbstractTest;

@Transactional
@ContextConfiguration(locations = {
	"classpath:spring/junit.xml"
})
@RunWith(SpringJUnit4ClassRunner.class)
public class PersonalRecordServiceTest extends AbstractTest {

	// Services and repositories
	@Autowired
	private CurriculaService			curriculaService;

	@Autowired
	private PersonalRecordService	personalRecordService;

	@Autowired
	private CandidateService			candidateService;

	// Templates --------------------------------------------------------------

	// An actor who is authenticated as a candidate must be able to: Edit his or her curricula.
	// Comprobamos que los curricula se editan correctamente y para los tests negativos vemos si salta la excepci�n correcta si se le pasan par�metros incorrectos.
	// Test positivo y 2 tests negativosTest positivo y 2 tests negativos

	protected void template2(String username, Integer curriculaId, final Class<?> expected) {
		Class<?> caught;

		caught = null;
		try {
			this.authenticate(username);
			System.out.println("#editCurricula");
			Curricula curricula = this.curriculaService.findOne(curriculaId);
			PersonalRecord personalRecord = curricula.getPersonalRecord();
			personalRecord.setFullName("Name random");
			PersonalRecord personalRecordSaved = this.personalRecordService.saveAndFlush(personalRecord);
			Assert.isTrue(personalRecordSaved != null && personalRecordSaved.getId() != 0);
			System.out.println(personalRecordSaved.getFullName());

			this.unauthenticate();
		} catch (Throwable oops) {
			caught = oops.getClass();
		}

		this.checkExceptions(expected, caught);
	}
	
	// An actor who is authenticated as a candidate must be able to: List his or her curricula.
	// Comprobamos que los curricula se listan correctamente y para los tests negativos vemos si salta la excepci�n correcta si se le pasan par�metros incorrectos.
	// Test positivo y 2 tests negativos
	protected void template3(String username, Integer curriculaId, Class<?> expected) {
		Class<?> caught;

		caught = null;
		try {
			authenticate(username);
			Curricula curricula = curriculaService.findOne(curriculaId);
			System.out.println("#listCurriculaByCandidate");
			Assert.isTrue(curricula.getPersonalRecord() != null);
			System.out.println(curricula.getPersonalRecord().getFullName());

			unauthenticate();
		} catch (Throwable oops) {
			caught = oops.getClass();
		}

		checkExceptions(expected, caught);
	}


	// Drivers ----------------------------------------------------------------


	@Test
	public void listPersonalRecordByCurricula() {

		authenticate("candidate1");
		Candidate candidate = candidateService.findByPrincipal();
		List<Curricula> curriculas = (List<Curricula>) candidate.getCurriculas();
		Object testingData[][] = {
			{
				"candidate1", curriculas.get(0).getId(), null
			}, {
				"candidate1", null, NullPointerException.class
			}, {
				"candidate1", 0, IllegalArgumentException.class
			}
		};

		for (int i = 0; i < testingData.length; i++)
			template3((String) testingData[i][0], (Integer) testingData[i][1], (Class<?>) testingData[i][2]);
	}
	
	@Test
	public void editCurriculaDriver() {
		
		authenticate("candidate1");
		Candidate candidate = candidateService.findByPrincipal();
		List<Curricula> curriculas = (List<Curricula>) candidate.getCurriculas();

		final Object testingData[][] = {
			{
				"candidate1", curriculas.get(0).getId(), null
			}, {
				"candidate1", null, NullPointerException.class
			}, {
				"candidate1", 0, IllegalArgumentException.class
			}
		};

		for (int i = 0; i < testingData.length; i++)
			this.template2((String) testingData[i][0], (Integer) testingData[i][1], (Class<?>) testingData[i][2]);
	}

}
