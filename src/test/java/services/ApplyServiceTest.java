
package services;

import java.util.Collection;
import java.util.List;

import javax.transaction.Transactional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.util.Assert;

import domain.Apply;
import domain.Candidate;
import domain.Company;
import domain.Curricula;
import domain.Offer;
import utilities.AbstractTest;

@Transactional
@ContextConfiguration(locations = {
	"classpath:spring/junit.xml"
})
@RunWith(SpringJUnit4ClassRunner.class)
public class ApplyServiceTest extends AbstractTest {

	// Services and repositories
	@Autowired
	private OfferService		offerService;

	@Autowired
	private ApplyService		applyService;

	@Autowired
	private CandidateService	candidateService;

	@Autowired
	private CompanyService		companyService;

	// Templates --------------------------------------------------------------


	// An actor who is authenticated as a candidate must be able to: List his or her applications and sort them by status, application moment, or deadline.
	// Comprobamos que los applications se listan correctamente y para el test negativo vemos si salta la excepci�n correcta si entra una persona no autenticada.
	// Test positivo y test negativo
	@Test
	public void listApplications() {

		final Object testingData[][] = {
			{
				"candidate1", null, null
			}, {
				"", null, IllegalArgumentException.class
			}
		};

		for (int i = 0; i < testingData.length; i++)
			template((String) testingData[i][0], (Integer) testingData[i][1], (Class<?>) testingData[i][2]);
	}
	protected void template(String username, Integer applyId, final Class<?> expected) {
		Class<?> caught;

		caught = null;
		try {
			authenticate(username);
			Collection<Apply> applies = applyService.findApplyFromCandidate();
			System.out.println("#listApplies");
			Assert.isTrue(applies != null && !applies.isEmpty());
			for (Apply o : applies)
				System.out.println(o.getMomment() + "-" + o.getStatus() + "-" + o.getOffer().getTitle());
			unauthenticate();
		} catch (Throwable oops) {
			caught = oops.getClass();
		}

		checkExceptions(expected, caught);
	}

	// An actor who is authenticated as a candidate must be able to: Apply for an offer as long as its deadline has not elapsed.
	// Comprobamos que la solicitud se hace correctamente y para los tests negativos vemos si salta la excepci�n correcta si pasamos parametros nulos y si pasamos un valor de id incorrecto del Offer.
	// Test positivo y 2 tests negativo
	@Test
	public void applyOffer() {

		List<Offer> offers = (List<Offer>) offerService.findAll();
		Integer offerId = offers.get(0).getId();

		final Object testingData[][] = {
			{
				"candidate1", offerId, null
			}, {
				"candidate1", null, IllegalArgumentException.class
			}, {
				"candidate1", 0, IllegalArgumentException.class
			}
		};

		for (int i = 0; i < testingData.length; i++)
			template2((String) testingData[i][0], (Integer) testingData[i][1], (Class<?>) testingData[i][2]);
	}
	protected void template2(String username, Integer offerId, final Class<?> expected) {
		Class<?> caught;

		caught = null;
		try {
			authenticate(username);
			Candidate candidate = candidateService.findByPrincipal();
			List<Curricula> curriculas = (List<Curricula>) candidate.getCurriculas();
			Offer offer = applyService.registerApplicationOffer(offerId, curriculas.get(0).getId());
			System.out.println("#applyOffer");
			Assert.isTrue(offer != null);
			System.out.println(offer.getTitle() + "-" + offer.getApplies());
			unauthenticate();
		} catch (Throwable oops) {
			caught = oops.getClass();
		}

		checkExceptions(expected, caught);
	}

	/**
	 * An actor who is authenticated as a company must be able to: Accept or reject any application for their offers;
	 * note that accepting or rejecting an application does not require the corresponding deadline to have elapsed since they can be assessed at any time.
	 *
	 * Test correspondiente al establecimiento de un apply como pendiente
	 *
	 * Test positivo logado como company y un apply asociado a una oferta de dicha company
	 * Test negativo logado como company y un apply de una company distinta
	 * Test negativo logado como cadidate
	 */
	@Test
	public void setApplyAsPending() {

		/*
		 * Preparaci�n test positivo.
		 *
		 * Obtenemos compa��a con ofertas y applies
		 */
		authenticate("company1");

		Company company1 = companyService.findByPrincipal();
		Assert.notNull(company1, "Es necesario que exista la company1 dada de alta en el populate");

		List<Offer> offers = (List<Offer>) company1.getOffers();
		Assert.isTrue(offers != null && !offers.isEmpty(), "Para realizar el test, la compa��a debe tener ofertas");

		Offer offer = offers.get(0);
		List<Apply> applies = (List<Apply>) offer.getApplies();
		Assert.isTrue(applies != null && !applies.isEmpty(), "Para realizar el test, la oferta debe tener applies");
		Apply apply = applies.get(0);

		unauthenticate();

		/*
		 * Preparaci�n test negativo.
		 *
		 * Obtenemos un apply de una company distinta
		 */
		List<Apply> allApplies = (List<Apply>) applyService.findAll();
		Apply applyOtherCompany = null;
		for (Apply applyAux : allApplies)
			if (applyAux.getOffer().getCompany().getId() != company1.getId()) {
				applyOtherCompany = applyAux;
				break;
			}
		Assert.isTrue(applyOtherCompany != null, "No se encuentra un apply de una compa��a distinta para el test negativo");

		final Object testingData[][] = {
			{
				"company1", apply.getId(), null
			}, {
				"company1", applyOtherCompany.getId(), IllegalArgumentException.class
			}, {
				"candidate1", 0, IllegalArgumentException.class
			}
		};

		for (int i = 0; i < testingData.length; i++)
			template3((String) testingData[i][0], (Integer) testingData[i][1], (Class<?>) testingData[i][2]);
	}
	protected void template3(String username, Integer applyId, final Class<?> expected) {
		Class<?> caught;

		caught = null;
		try {
			authenticate(username);
			Apply apply = applyService.setApplyAsPending(applyId);
			System.out.println("#setApplyAsPending");
			Assert.isTrue(apply.getStatus().equals("pending"));
			unauthenticate();
		} catch (Throwable oops) {
			caught = oops.getClass();
		}

		checkExceptions(expected, caught);
	}

	/**
	 * An actor who is authenticated as a company must be able to: Accept or reject any application for their offers;
	 * note that accepting or rejecting an application does not require the corresponding deadline to have elapsed since they can be assessed at any time.
	 *
	 * Test correspondiente al establecimiento de un apply como aceptado
	 *
	 * Test positivo logado como company y un apply asociado a una oferta de dicha company
	 * Test negativo logado como company y un apply de una company distinta
	 * Test negativo logado como cadidate
	 */
	@Test
	public void setApplyAsAccepted() {

		/*
		 * Preparaci�n test positivo.
		 *
		 * Obtenemos compa��a con ofertas y applies
		 */
		authenticate("company1");

		Company company1 = companyService.findByPrincipal();
		Assert.notNull(company1, "Es necesario que exista la company1 dada de alta en el populate");

		List<Offer> offers = (List<Offer>) company1.getOffers();
		Assert.isTrue(offers != null && !offers.isEmpty(), "Para realizar el test, la compa��a debe tener ofertas");

		Offer offer = offers.get(0);
		List<Apply> applies = (List<Apply>) offer.getApplies();
		Assert.isTrue(applies != null && !applies.isEmpty(), "Para realizar el test, la oferta debe tener applies");
		Apply apply = applies.get(0);

		unauthenticate();

		/*
		 * Preparaci�n test negativo.
		 *
		 * Obtenemos un apply de una company distinta
		 */
		List<Apply> allApplies = (List<Apply>) applyService.findAll();
		Apply applyOtherCompany = null;
		for (Apply applyAux : allApplies)
			if (applyAux.getOffer().getCompany().getId() != company1.getId()) {
				applyOtherCompany = applyAux;
				break;
			}
		Assert.isTrue(applyOtherCompany != null, "No se encuentra un apply de una compa��a distinta para el test negativo");

		final Object testingData[][] = {
			{
				"company1", apply.getId(), null
			}, {
				"company1", applyOtherCompany.getId(), IllegalArgumentException.class
			}, {
				"candidate1", 0, IllegalArgumentException.class
			}
		};

		for (int i = 0; i < testingData.length; i++)
			template4((String) testingData[i][0], (Integer) testingData[i][1], (Class<?>) testingData[i][2]);
	}
	protected void template4(String username, Integer applyId, final Class<?> expected) {
		Class<?> caught;

		caught = null;
		try {
			authenticate(username);
			Apply apply = applyService.setApplyAsAccepted(applyId);
			System.out.println("#setApplyAsAccepted");
			Assert.isTrue(apply.getStatus().equals("accepted"));
			unauthenticate();
		} catch (Throwable oops) {
			caught = oops.getClass();
		}

		checkExceptions(expected, caught);
	}

	/**
	 * An actor who is authenticated as a company must be able to: Accept or reject any application for their offers;
	 * note that accepting or rejecting an application does not require the corresponding deadline to have elapsed since they can be assessed at any time.
	 *
	 * Test correspondiente al establecimiento de un apply como rechazado
	 *
	 * Test positivo logado como company y un apply asociado a una oferta de dicha company
	 * Test negativo logado como company y un apply de una company distinta
	 * Test negativo logado como cadidate
	 */
	@Test
	public void setApplyAsRejected() {

		/*
		 * Preparaci�n test positivo.
		 *
		 * Obtenemos compa��a con ofertas y applies
		 */
		authenticate("company1");

		Company company1 = companyService.findByPrincipal();
		Assert.notNull(company1, "Es necesario que exista la company1 dada de alta en el populate");

		List<Offer> offers = (List<Offer>) company1.getOffers();
		Assert.isTrue(offers != null && !offers.isEmpty(), "Para realizar el test, la compa��a debe tener ofertas");

		Offer offer = offers.get(0);
		List<Apply> applies = (List<Apply>) offer.getApplies();
		Assert.isTrue(applies != null && !applies.isEmpty(), "Para realizar el test, la oferta debe tener applies");
		Apply apply = applies.get(0);

		unauthenticate();

		/*
		 * Preparaci�n test negativo.
		 *
		 * Obtenemos un apply de una company distinta
		 */
		List<Apply> allApplies = (List<Apply>) applyService.findAll();
		Apply applyOtherCompany = null;
		for (Apply applyAux : allApplies)
			if (applyAux.getOffer().getCompany().getId() != company1.getId()) {
				applyOtherCompany = applyAux;
				break;
			}
		Assert.isTrue(applyOtherCompany != null, "No se encuentra un apply de una compa��a distinta para el test negativo");

		final Object testingData[][] = {
			{
				"company1", apply.getId(), null
			}, {
				"company1", applyOtherCompany.getId(), IllegalArgumentException.class
			}, {
				"candidate1", 0, IllegalArgumentException.class
			}
		};

		for (int i = 0; i < testingData.length; i++)
			template5((String) testingData[i][0], (Integer) testingData[i][1], (Class<?>) testingData[i][2]);
	}
	protected void template5(String username, Integer applyId, final Class<?> expected) {
		Class<?> caught;

		caught = null;
		try {
			authenticate(username);
			Apply apply = applyService.setApplyAsRejected(applyId);
			System.out.println("#setApplyAsRejected");
			Assert.isTrue(apply.getStatus().equals("rejected"));
			unauthenticate();
		} catch (Throwable oops) {
			caught = oops.getClass();
		}

		checkExceptions(expected, caught);
	}

}
