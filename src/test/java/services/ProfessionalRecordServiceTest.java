
package services;

import java.util.Collection;
import java.util.Date;
import java.util.List;

import javax.transaction.Transactional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.util.Assert;

import domain.Candidate;
import domain.Curricula;
import domain.ProfessionalRecord;
import utilities.AbstractTest;

@Transactional
@ContextConfiguration(locations = {
	"classpath:spring/junit.xml"
})
@RunWith(SpringJUnit4ClassRunner.class)
public class ProfessionalRecordServiceTest extends AbstractTest {

	// Services and repositories
	@Autowired
	private CurriculaService			curriculaService;

	@Autowired
	private ProfessionalRecordService	professionalRecordService;

	@Autowired
	private CandidateService			candidateService;

	// Templates --------------------------------------------------------------


	// An actor who is authenticated as a candidate must be able to: Edit his or her curricula.
	// Comprobamos que los curricula se editan correctamente y para los tests negativos vemos si salta la excepci�n correcta si se le pasan par�metros incorrectos.
	// Test positivo y 2 tests negativosTest positivo y 2 tests negativos

	protected void template2(final String username, final Integer curriculaId, final Class<?> expected) {
		Class<?> caught;

		caught = null;
		try {
			this.authenticate(username);
			System.out.println("#editCurricula");
			final Curricula curricula = this.curriculaService.findOne(curriculaId);
			final Collection<ProfessionalRecord> professionalRecords = curricula.getProfessionalRecords();
			Integer i = 1;
			for (final ProfessionalRecord o : professionalRecords) {
				o.setComments("Comentario random" + i);
				i++;
				System.out.println(o.getComments());
				final ProfessionalRecord professionalRecordSaved = this.professionalRecordService.saveAndFlush(o);
				Assert.isTrue(professionalRecordSaved != null && professionalRecordSaved.getId() != 0);
				System.out.println(professionalRecordSaved.getComments());
			}

			this.unauthenticate();
		} catch (final Throwable oops) {
			caught = oops.getClass();
		}

		this.checkExceptions(expected, caught);
	}

	// An actor who is authenticated as a candidate must be able to: List his or her curricula.
	// Comprobamos que los curricula se listan correctamente y para los tests negativos vemos si salta la excepci�n correcta si se le pasan par�metros incorrectos.
	// Test positivo y 2 tests negativos
	protected void template3(final String username, final Integer curriculaId, final Class<?> expected) {
		Class<?> caught;

		caught = null;
		try {
			this.authenticate(username);
			final Curricula curricula = this.curriculaService.findOne(curriculaId);
			System.out.println("#listCurriculaByCandidate");
			Assert.isTrue(curricula.getProfessionalRecords() != null && !curricula.getProfessionalRecords().isEmpty());
			for (final ProfessionalRecord o : curricula.getProfessionalRecords())
				System.out.println(o.getCompany());

			this.unauthenticate();
		} catch (final Throwable oops) {
			caught = oops.getClass();
		}

		this.checkExceptions(expected, caught);
	}

	/*
	 * An actor who is authenticated as a candidate must be able to: Edit an existing curriculum.
	 *
	 * En este caso se realizara la acci�n de crear un registro professional en una curricula ya existente. Para forzar el fallo se pueden dar los siguientes casos
	 * Para forzar el fallo se pueden dar los siguientes casos:
	 *
	 * - El usuario logueado no es un candidate
	 * - La curricula no pertenece al usuario logueado
	 * - La curricula no existe
	 * - Los campos no son correctos
	 * - El startdate es mayor que el enddate
	 *
	 */

	public void create(final String username, final int curriculaID, final String company, final String startDate, final String endDate, final String role, final String attachment, final String comments, final Class<?> expected) {

		Class<?> caught = null;

		try {

			this.authenticate(username);

			this.candidateService.checkIfCandidate();

			final Curricula curricula = this.curriculaService.findOne(curriculaID);
			Assert.isTrue(this.candidateService.findByPrincipal().getId() == curricula.getCandidate().getId());
			final ProfessionalRecord professionalRecord = this.professionalRecordService.create(curricula);
			professionalRecord.setCompany(company);
			professionalRecord.setStartDate(new Date(startDate));
			professionalRecord.setEndDate(new Date(endDate));
			professionalRecord.setRole(role);
			professionalRecord.setAttachment(attachment);
			professionalRecord.setComments(comments);
			this.professionalRecordService.comprobacion(professionalRecord);
			this.professionalRecordService.save(professionalRecord);

			this.unauthenticate();

		} catch (final Throwable oops) {

			caught = oops.getClass();

		}

		this.checkExceptions(expected, caught);
	}

	/*
	 * An actor who is authenticated as a candidate must be able to: Edit an existing curriculum.
	 *
	 * En este caso se realizara la acci�n de borrar un registro profesional en una curricula ya existente. Para forzar el fallo se pueden dar los siguientes casos
	 * Para forzar el fallo se pueden dar los siguientes casos:
	 *
	 * - El usuario logueado no es un candidate
	 * - La curricula no pertenece al usuario logueado
	 * - La curricula no existe
	 * - El professional record no existe
	 * - El professional record no pertenece a la curricula
	 *
	 */
	public void delete(final String username, final int curriculaID, final int professionalRecordID, final Class<?> expected) {

		Class<?> caught = null;

		try {

			this.authenticate(username);

			this.candidateService.checkIfCandidate();

			final Curricula curricula = this.curriculaService.findOne(curriculaID);
			final ProfessionalRecord professionalRecord = this.professionalRecordService.findOne(professionalRecordID);
			Assert.isTrue(this.candidateService.findByPrincipal().getId() == curricula.getCandidate().getId());
			Assert.isTrue(curricula.getProfessionalRecords().contains(professionalRecord));

			this.professionalRecordService.delete(professionalRecord);

			this.unauthenticate();

		} catch (final Throwable oops) {

			caught = oops.getClass();

		}

		this.checkExceptions(expected, caught);
	}

	// Drivers ----------------------------------------------------------------

	@Test
	public void createDriver() {

		final Object testingData[][] = {
			// Crear professional record como no autentificado -> false
			{
				null, 987, "Company name test", "16/07/2010", "16/07/2012", "Role test", "http://www.attachment.com", "Comments test", IllegalArgumentException.class
			},
			// Crear professional record como admin -> false
			{
				"admin", 987, "Company name test", "16/07/2010", "16/07/2012", "Role test", "http://www.attachment.com", "Comments test", IllegalArgumentException.class
			},
			// Crear professional record como company -> false
			{
				"company1", 987, "Company name test", "16/07/2010", "16/07/2012", "Role test", "http://www.attachment.com", "Comments test", IllegalArgumentException.class
			},
			// La curricula no pertenece al usuario logueado-> false
			{
				"candidate3", 987, "Company name test", "16/07/2010", "16/07/2012", "Role test", "http://www.attachment.com", "Comments test", IllegalArgumentException.class
			},
			// La curricula no existe -> false
			{
				"candidate1", 645645, "Company name test", "16/07/2010", "16/07/2012", "Role test", "http://www.attachment.com", "Comments test", IllegalArgumentException.class
			},
			// Los campos no son correctos -> false
			{
				"candidate1", 987, "", "16/07/2010", "16/07/2012", "", "http://www.attachment.com", "Comments test", IllegalArgumentException.class
			},
			// El startDate es mayor que el endDate -> false
			{
				"candidate1", 987, "Company name test", "16/07/2012", "16/07/2010", "Role test", "http://www.attachment.com", "Comments test", IllegalArgumentException.class
			},
			// Crear correctamente -> true
			{
				"candidate1", 987, "Company name test", "16/07/2010", "16/07/2012", "Role test", "http://www.attachment.com", "Comments test", null
			}

		};
		for (int i = 0; i < testingData.length; i++)
			this.create((String) testingData[i][0], (int) testingData[i][1], (String) testingData[i][2], (String) testingData[i][3], (String) testingData[i][4], (String) testingData[i][5], (String) testingData[i][6], (String) testingData[i][7],
				(Class<?>) testingData[i][8]);
	}

	@Test
	public void deleteDriver() {

		final Object testingData[][] = {
			// Delete professional record como no autentificado -> false
			{
				null, 987, 1009, IllegalArgumentException.class
			},
			// Delete professional record como admin -> false
			{
				"admin", 987, 1009, IllegalArgumentException.class
			},
			// Delete professional record como company -> false
			{
				"company1", 987, 1009, IllegalArgumentException.class
			},
			// La curricula no pertenece al usuario logueado-> false
			{
				"candidate3", 987, 1009, IllegalArgumentException.class
			},
			// La curricula no existe -> false
			{
				"candidate1", 645645, 1009, IllegalArgumentException.class
			},
			// El professional record no existe -> false
			{
				"candidate1", 987, 663663, IllegalArgumentException.class
			},
			// El professional record no pertenece a la curricula -> false
			{
				"candidate1", 987, 1013, IllegalArgumentException.class
			},
			// Borrar professional record -> true
			{
				"candidate1", 987, 1009, null
			}

		};
		for (int i = 0; i < testingData.length; i++)
			this.delete((String) testingData[i][0], (int) testingData[i][1], (int) testingData[i][2], (Class<?>) testingData[i][3]);
	}

	@Test
	public void listProfessionalRecordByCurricula() {

		this.authenticate("candidate1");
		final Candidate candidate = this.candidateService.findByPrincipal();
		final List<Curricula> curriculas = (List<Curricula>) candidate.getCurriculas();
		final Object testingData[][] = {
			{
				"candidate1", curriculas.get(0).getId(), null
			}, {
				"candidate1", null, NullPointerException.class
			}, {
				"candidate1", 0, IllegalArgumentException.class
			}
		};

		for (int i = 0; i < testingData.length; i++)
			this.template3((String) testingData[i][0], (Integer) testingData[i][1], (Class<?>) testingData[i][2]);
	}

	@Test
	public void editCurriculaDriver() {

		this.authenticate("candidate1");
		final Candidate candidate = this.candidateService.findByPrincipal();
		final List<Curricula> curriculas = (List<Curricula>) candidate.getCurriculas();

		final Object testingData[][] = {
			{
				"candidate1", curriculas.get(0).getId(), null
			}, {
				"candidate1", null, NullPointerException.class
			}, {
				"candidate1", 0, IllegalArgumentException.class
			}
		};

		for (int i = 0; i < testingData.length; i++)
			this.template2((String) testingData[i][0], (Integer) testingData[i][1], (Class<?>) testingData[i][2]);
	}

}
